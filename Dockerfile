FROM mcr.microsoft.com/dotnet/aspnet:7.0 AS base
ENV TZ Asia/Ho_Chi_Minh
RUN apt-get update
RUN apt-get install -y software-properties-common
RUN apt-get install -y fonts-crosextra-carlito
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
WORKDIR /src
COPY ["Course/Course.csproj", "Course/"]
RUN dotnet restore "Course/Course.csproj"
COPY . .
WORKDIR "/src/Course"
RUN dotnet build "Course.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Course.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Course.dll"]
