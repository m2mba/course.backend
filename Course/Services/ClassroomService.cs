using Course.Common;
using Course.Common.Dto;
using Course.Common.Email;
using Course.Common.Email.Template;
using Course.DbModels;
using Course.Extensions;
using Course.Repositories.Models;
using Course.Services.Models;

namespace Course.Services;

public class ClassroomService : BaseService
{
    private static readonly Dictionary<string, List<string>> ClassroomStatusFlows = new()
    {
        {
            Constants.CLASSROOM_ACTIVE_STATUS,
            new List<string> { Constants.CLASSROOM_LEARNING_STATUS, Constants.CLASSROOM_STOP_STATUS }
        },
        { Constants.CLASSROOM_LEARNING_STATUS, new List<string> { Constants.CLASSROOM_FINISH_STATUS } }
    };

    public ClassroomService(CourseContext dbContext) : base(dbContext)
    {
    }

    public Classroom CreateClassroom(Classroom classroom)
    {
        var course = CourseRepo.GetById(classroom.CourseId);
        if (course != null)
        {
            classroom.Fee = course.Fee;
        }

        ClassroomRepo.Add(classroom);
        SaveDbChanges();
        return classroom;
    }

    public PageDto<ClassroomDto> ListClassrooms(ListClassroomParam param, Option? option = null)
    {
        var classrooms = ClassroomRepo.List(param, option);
        var total = ClassroomRepo.Count(param);
        var list = new List<ClassroomDto>();
        var courseIds = classrooms.Select(x => x.CourseId).ToList();
        var courses = CourseRepo.List(new ListCourseParam { Ids = courseIds });
        foreach (var classroom in classrooms)
        {
            var item = Utils.CastObject<ClassroomDto>(classroom);
            var course = courses.FirstOrDefault(x => x.Id == classroom.CourseId);
            if (course != null)
            {
                item.Course = Utils.CastObject<CourseDto>(course);
            }

            list.Add(item);
        }

        return new PageDto<ClassroomDto>
        {
            List = list,
            Total = total
        };
    }

    public void UpdateClassroomStatus(int classroomId, string status)
    {
        var classroom = ClassroomRepo.GetById(classroomId);
        if (classroom == null)
        {
            throw new BusinessException("Lớp học không tồn tại");
        }

        if (!ClassroomStatusFlows.TryGetValue(classroom.Status, out var allowStatuses) ||
            !allowStatuses.Contains(status))
        {
            throw new BusinessException("Trạng thái không hợp lệ");
        }

        classroom.Status = status;
        classroom.UpdatedAt = DateTime.Now;

        if (status == Constants.CLASSROOM_LEARNING_STATUS)
        {
            var registers = RegisterRepo.List(new ListRegisterParam
            {
                ClassroomId = classroomId,
                Statuses = new List<string> { Constants.REGISTER_SUCCESS_STATUS }
            });
            foreach (var register in registers)
            {
                register.Status = Constants.REGISTER_LEARNING_STATUS;
            }

            var students = StudentRepo.List(new ListStudentParam
            {
                ClassroomId = classroomId,
                Status = Constants.STUDENT_WAIT_JOIN_STATUS
            });
            foreach (var student in students)
            {
                student.Status = Constants.STUDENT_LEARNING_STATUS;
            }
        }

        if (status == Constants.CLASSROOM_FINISH_STATUS)
        {
            var registers = RegisterRepo.List(new ListRegisterParam
            {
                ClassroomId = classroomId,
                Statuses = new List<string> { Constants.REGISTER_LEARNING_STATUS }
            });
            foreach (var register in registers)
            {
                register.Status = Constants.REGISTER_FINISH_STATUS;
            }

            var students = StudentRepo.List(new ListStudentParam
            {
                ClassroomId = classroomId,
                Status = Constants.STUDENT_LEARNING_STATUS
            });
            foreach (var student in students)
            {
                student.Status = Constants.STUDENT_FINISH_STATUS;
            }
        }

        SaveDbChanges();
    }

    public void UpdateClassroom(int classroomId, Classroom data)
    {
        var classroom = ClassroomRepo.GetById(classroomId);
        if (classroom == null)
        {
            throw new BusinessException("Lớp học không tồn tại");
        }

        classroom.Code = data.Code;
        classroom.Name = data.Name;
        classroom.CourseId = data.CourseId;
        classroom.StartDate = data.StartDate;
        classroom.EndDate = data.EndDate;
        classroom.StartEnrollmentDate = data.StartEnrollmentDate;
        classroom.TotalSlot = data.TotalSlot;
        classroom.ZaloLink = data.ZaloLink;
        classroom.ZaloLinkFreeChat = data.ZaloLinkFreeChat;
        classroom.GoogleMeetLink = data.GoogleMeetLink;
        classroom.UpdatedAt = DateTime.Now;

        SaveDbChanges();
    }

    public void UpdateClassroomGoogleMeet(int classroomId, string? googleMeetLink)
    {
        var classroom = ClassroomRepo.GetById(classroomId);
        if (classroom == null)
        {
            throw new BusinessException("Lớp học không tồn tại");
        }

        classroom.GoogleMeetLink = googleMeetLink;
        classroom.UpdatedAt = DateTime.Now;

        SaveDbChanges();
    }

    public void UpdateClassroomZalo(int classroomId, string? link)
    {
        var classroom = ClassroomRepo.GetById(classroomId);
        if (classroom == null)
        {
            throw new BusinessException("Lớp học không tồn tại");
        }

        classroom.ZaloLink = link;
        classroom.UpdatedAt = DateTime.Now;

        SaveDbChanges();
    }

    public void UpdateClassroomZaloFreeChat(int classroomId, string? link)
    {
        var classroom = ClassroomRepo.GetById(classroomId);
        if (classroom == null)
        {
            throw new BusinessException("Lớp học không tồn tại");
        }

        classroom.ZaloLinkFreeChat = link;
        classroom.UpdatedAt = DateTime.Now;

        SaveDbChanges();
    }

    public void NotifyEmails(int classroomId)
    {
        var classroom = GetClassroomById(classroomId);
        if (classroom == null)
        {
            throw new BusinessException("Lớp học không tồn tại");
        }

        if (!classroom.AllowEmailNotify)
        {
            throw new BusinessException("Lớp học chưa được phép gửi thông báo");
        }

        if (classroom.StartDate == null || classroom.StartEnrollmentDate == null) return;
        var course = CourseRepo.GetById(classroom.CourseId);
        if (course == null)
        {
            throw new BusinessException("Khóa học không tồn tại");
        }

        var notifyEmails = NotificationEmailRepo.List(new ListEmailParam());
        var emails = notifyEmails.Select(x => x.Email).Distinct().ToList();
        var sentEmails = EmailLogRepo.List(new ListEmailLogParam
        {
            ClassroomId = classroomId,
            Emails = emails
        });

        emails.RemoveAll(x => sentEmails.Any(e => e.Email == x));
        if (!emails.HasValue()) return;
        var baseDomain = ConfigureService.GetString("StudentWebUrl");
        var mailData = new ContentTemplateClassroomNotify(course.Name, course.Method)
        {
            CourseName = course.Name,
            Fee = classroom.Fee.GetValueOrDefault(),
            Method = course.Method,
            StartDate = classroom.StartDate.Value,
            StartEnrollmentDate = classroom.StartEnrollmentDate.Value,
            TimeInfo = course.TimeInHour + " giờ / " + course.TimeInLesson + " buổi",
            RefLink = $"{baseDomain}/courses",
            Address = course.Method.ToUpper() == "OFFLINE" && !string.IsNullOrWhiteSpace(classroom.Address)
                ? classroom.Address
                : ""
        };
        var mailContent = mailData.FormatContent();
        foreach (var email in emails)
        {
            EmailHelper.Send(email, mailContent, mailData.GetTitle(), false);
            EmailLogRepo.Add(new EmailLog
            {
                Content = mailContent,
                Email = email,
                CourseId = course.Id,
                ClassroomId = classroomId,
                CreatedAt = DateTime.Now
            });
            var notifyEmail = notifyEmails.FirstOrDefault(x => x.Email == email);
            if (notifyEmail != null)
            {
                notifyEmail.Times = (notifyEmail.Times ?? 0) + 1;
            }
        }

        SaveDbChanges();
    }
}