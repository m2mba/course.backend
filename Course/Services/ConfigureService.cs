﻿namespace Course.Services;

public class ConfigureService
{
    private static ConfigureService? instance;

    private ConfigureService()
    {
        var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", true, true);
        Configuration = builder.Build();
    }

    private IConfiguration Configuration { get; }

    private static ConfigureService GetInstance()
    {
        return instance ??= new ConfigureService();
    }


    private string GetConnectionStringInternal(string key)
    {
        return Configuration.GetConnectionString(key)!;
    }

    public static string GetConnectionString(string key)
    {
        return GetInstance().GetConnectionStringInternal(key);
    }

    private string? GetStringInternal(string key)
    {
        return Configuration.GetValue<string>(key);
    }

    public static string GetString(string key)
    {
        return GetInstance().GetStringInternal(key) ?? "";
    }

    public static T? GetObject<T>(string key)
    {
        return GetInstance().Configuration.GetSection(key).Get<T>();
    }
}