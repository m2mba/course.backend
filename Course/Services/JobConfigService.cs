using Course.DbModels;

namespace Course.Services;

public class JobConfigService : BaseService
{
    public JobConfigService(CourseContext dbContext) : base(dbContext)
    {
    }

    public JobConfig GetConfig()
    {
        return JobConfigRepo.List().FirstOrDefault() ?? new JobConfig
        {
            AllowCancelRegister = 1,
            WaitPaymentHour = 24,
            AllowConfirmByTransaction = 1
        };
    }
}