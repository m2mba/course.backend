using Course.Common;
using Course.Common.Dto;
using Course.DbModels;
using Course.Extensions;
using Course.Repositories.Models;
using Course.Services.Models;

namespace Course.Services;

public class DocService : BaseService
{
    public DocService(CourseContext dbContext) : base(dbContext)
    {
    }

    public CatDoc CreateDoc(int? courseId, int? classroomId, CatDoc doc)
    {
        if (courseId != null)
        {
            var course = CourseRepo.GetById(courseId);
            if (course == null)
            {
                throw new BusinessException("Khóa học không tồn tại");
            }
        }

        if (classroomId != null)
        {
            var classroom = ClassroomRepo.GetById(classroomId);
            if (classroom == null)
            {
                throw new BusinessException("Đợt tuyển sinh không tồn tại");
            }

            courseId = classroom.CourseId;
        }

        if (courseId == null)
        {
            throw new BusinessException("Khóa học không tồn tại");
        }

        var oldDoc = CatDocRepo.List(new ListCatDocParam { Url = doc.Url }).FirstOrDefault();
        int docId;
        if (oldDoc == null)
        {
            CatDocRepo.Add(doc);
            SaveDbChanges();
            docId = doc.Id;
        }
        else
        {
            docId = oldDoc.Id;
        }

        var countMapDoc = CourseClassroomDocRepo.Count(new ListCourseClassroomDocParam
        {
            CourseId = courseId,
            ClassroomId = classroomId,
            DocId = docId
        });

        if (countMapDoc == 0)
        {
            var mapDoc = new CourseClassroomDoc
            {
                CourseId = courseId.Value,
                ClassroomId = classroomId,
                DocId = docId,
                CreatedAt = DateTime.Now
            };
            CourseClassroomDocRepo.Add(mapDoc);
            SaveDbChanges();
        }

        return doc;
    }

    public void UpdateDoc(int id, CatDoc newDoc)
    {
        var doc = CatDocRepo.GetById(id);
        if (doc == null)
        {
            throw new BusinessException("Tài liệu không tồn tại");
        }

        doc.Name = newDoc.Name;
        doc.Method = newDoc.Method;
        doc.DocType = newDoc.DocType;
        doc.Url = newDoc.Url;
        doc.HashUrl = newDoc.HashUrl;
        CatDocRepo.Update(doc);
        SaveDbChanges();
    }

    public void DeleteClassroomDoc(int id)
    {
        var doc = CourseClassroomDocRepo.GetById(id);
        if (doc == null)
        {
            throw new BusinessException("Tài liệu không tồn tại");
        }
        CourseClassroomDocRepo.Delete(doc);
        SaveDbChanges();
    }
    public PageDto<CourseClassroomDocDto> ListSharableDocs(ListCourseClassroomDocParam param, Option? option = null)
    {
        var mapDocs = CourseClassroomDocRepo.List(param, option);
        var total = option?.Paging ?? false ? CourseClassroomDocRepo.Count(param) : 0;
        if (param.ClassroomId != null && param.CourseId != null)
        {
            var courseParam = Utils.CastObject<ListCourseClassroomDocParam>(param);
            courseParam.ClassroomId = null;
            courseParam.OnlyCourse = true;
            mapDocs.AddRange(CourseClassroomDocRepo.List(courseParam, option));
            total += option?.Paging ?? false ? CourseClassroomDocRepo.Count(courseParam) : 0;
            if (option != null && option.Paging && option.Size > 0)
            {
                mapDocs = mapDocs.Distinct().Take(option.Size).ToList();
            }
            else
            {
                mapDocs = mapDocs.Distinct().ToList();
            }
        }

        var docIds = mapDocs.Select(x => x.DocId).ToList();
        var courseIds = mapDocs.Select(x => x.CourseId).ToList();
        var classroomIds = mapDocs.Where(x => x.ClassroomId.HasValue).Select(x => x.ClassroomId!.Value).ToList();
        var docs = CatDocRepo.List(new ListCatDocParam { Ids = docIds });
        var courses = CourseRepo.List(new ListCourseParam { Ids = courseIds });
        var classrooms = ClassroomRepo.List(new ListClassroomParam { Ids = classroomIds });
        var list = new List<CourseClassroomDocDto>();
        foreach (var doc in mapDocs)
        {
            var item = Utils.CastObject<CourseClassroomDocDto>(doc);
            var catDoc = docs.FirstOrDefault(x => x.Id == doc.DocId);
            if (catDoc != null)
            {
                item.Doc = Utils.CastObject<DocDto>(catDoc);
            }

            var course = courses.FirstOrDefault(x => x.Id == doc.CourseId);
            if (course != null)
            {
                item.Course = Utils.CastObject<CourseDto>(course);
            }

            var classroom = classrooms.FirstOrDefault(x => x.Id == doc.ClassroomId);
            if (classroom != null)
            {
                item.Classroom = Utils.CastObject<ClassroomDto>(classroom);
            }

            list.Add(item);
        }

        return new PageDto<CourseClassroomDocDto>
        {
            List = list,
            Total = total
        };
    }

    public void ShareDocs(ShareDocActionDto input)
    {
        var docIds = input.DocIds.Distinct().ToList();
        var countDocs = CatDocRepo.Count(new ListCatDocParam { Ids = docIds });
        if (docIds.Count != countDocs)
        {
            throw new BusinessException("Không tồn tại người chia sẻ file");
        }

        ValidateExist<User>(input.ShareUserId, "Không tồn tại người chia sẻ file");
        ValidateExist<DbModels.Course>(input.CourseId, "Không tồn tại khóa học");
        ValidateExist<Classroom>(input.ClassroomId, "Không tồn tại lớp học");
        var emails = input.Emails.Split(',').Select(x => x.Trim().ToLower()).Distinct().ToList();
        var receiveUsers = UserRepo.List(new ListUserParam
        {
            Emails = emails
        });

        var mapUsers = receiveUsers.GroupBy(x => x.Email).ToDictionary(x => x.Key, _ => true);
        foreach (var email in emails.Where(email => !mapUsers.ContainsKey(email)))
        {
            throw new BusinessException($"Không tồn tại người dùng có email {email}");
        }

        var mapRegisters = RegisterRepo.List(new ListRegisterParam
        {
            CourseId = input.CourseId,
            ClassroomId = input.ClassroomId,
            ExcStatuses = new List<string> { Constants.REGISTER_CANCEL_STATUS }
        }).GroupBy(x => x.UserId).ToDictionary(x => x.Key, x => x.ToList());

        foreach (var receiveUser in receiveUsers)
        {
            if (!mapRegisters.ContainsKey(receiveUser.Id))
            {
                throw new BusinessException($"Email ${receiveUser.Email} chưa đăng ký thành công lớp học này");
            }
        }

        var allowDocIds = CourseClassroomDocRepo.List(new ListCourseClassroomDocParam
        {
            CourseId = input.CourseId,
            OnlyCourse = true
        }).Select(x => x.DocId).ToList();

        allowDocIds.AddRange(CourseClassroomDocRepo.List(new ListCourseClassroomDocParam
        {
            CourseId = input.CourseId,
            ClassroomId = input.ClassroomId
        }).Select(x => x.DocId).ToList());

        foreach (var docId in input.DocIds)
        {
            if (allowDocIds.Contains(docId)) continue;
            var doc = CatDocRepo.GetById(docId);
            if (doc == null)
            {
                throw new BusinessException("Tài liệu không tồn tại");
            }
            throw new BusinessException($"Tài liệu ${doc.Name} (${doc.Url}) không thuộc lớp, khóa học được chọn");
        }

        foreach (var (_, registers) in mapRegisters)
        {
            var userRegisters = registers.Where(x => receiveUsers.Any(u => u.Id == x.UserId)).ToList();
            foreach (var register in userRegisters)
            {
                register.SharedVideo = 1;
                register.SharedDoc = 1;
            }
        }

        var group = Guid.NewGuid().ToString();
        foreach (var docId in docIds)
        {
            ShareDocRepo.Add(new ShareDoc
            {
                CourseId = input.CourseId,
                ClassroomId = input.ClassroomId,
                DocId = docId,
                Emails = string.Join(',', emails),
                ShareUserId = input.ShareUserId,
                Group = group,
                CreatedAt = DateTime.Now
            });
        }

        SaveDbChanges();
    }

    public PageDto<ShareDocDto> ListSharedDocs(ListShareDocParam param, Option? option = null)
    {
        var mapDocs = ShareDocRepo.List(param, option);
        var docIds = mapDocs.Select(x => x.DocId).ToList();
        var courseIds = mapDocs.Select(x => x.CourseId).ToList();
        var classroomIds = mapDocs.Select(x => x.ClassroomId).ToList();
        var shareUserIds = mapDocs.Select(x => x.ShareUserId).ToList();
        var docs = CatDocRepo.List(new ListCatDocParam { Ids = docIds });
        var courses = CourseRepo.List(new ListCourseParam { Ids = courseIds });
        var classrooms = ClassroomRepo.List(new ListClassroomParam { Ids = classroomIds });
        var shareUsers = UserRepo.List(new ListUserParam { Ids = shareUserIds });
        var list = new List<ShareDocDto>();
        foreach (var doc in mapDocs)
        {
            var item = Utils.CastObject<ShareDocDto>(doc);
            var catDoc = docs.FirstOrDefault(x => x.Id == doc.DocId);
            if (catDoc != null)
            {
                item.Doc = Utils.CastObject<DocDto>(catDoc);
            }

            var course = courses.FirstOrDefault(x => x.Id == doc.CourseId);
            if (course != null)
            {
                item.Course = Utils.CastObject<CourseDto>(course);
            }

            var classroom = classrooms.FirstOrDefault(x => x.Id == doc.ClassroomId);
            if (classroom != null)
            {
                item.Classroom = Utils.CastObject<ClassroomDto>(classroom);
            }

            var user = shareUsers.FirstOrDefault(x => x.Id == doc.ShareUserId);
            if (user != null)
            {
                item.ShareUser = Utils.CastObject<UserDto>(user);
            }

            list.Add(item);
        }

        return new PageDto<ShareDocDto>
        {
            List = list,
            Total = option?.Paging ?? false ? ShareDocRepo.Count(param) : 0
        };
    }
}