using Course.Common;
using Course.DbModels;
using Course.Repositories.Models;
using Course.Services.Models;

namespace Course.Services;

public class CourseService : BaseService
{
    public CourseService(CourseContext dbContext) : base(dbContext)
    {
    }

    public List<CourseDto> ListCourses(int active)
    {
        var param = new ListCourseParam();
        if (active >= 0)
        {
            param.IsActive = (sbyte)(active == 0 ? 0 : 1);
        }

        var courses = CourseRepo.List(param);
        var ids = courses.Select(x => x.Id).ToList();
        var countOpenClassroom = ClassroomRepo.CountByCourse(new ListClassroomParam
        {
            CourseIds = ids,
            IsActive = 1,
            FromStartDate = DateTime.Today,
            ToStartEnrollmentDate = DateTime.Now,
            Status = Constants.CLASSROOM_ACTIVE_STATUS
        });
        var res = new List<CourseDto>();
        foreach (var course in courses)
        {
            var item = Utils.CastObject<CourseDto>(course);
            if (countOpenClassroom.TryGetValue(course.Id, out var totalOpen)) item.TotalOpenClassroom = totalOpen;
            item.StartEnrollmentDate = ClassroomRepo.GetNearestStartEnrollmentDate(item.Id);

            res.Add(item);
        }

        return res;
    }
}