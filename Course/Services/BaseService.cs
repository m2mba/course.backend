﻿using Course.Common;
using Course.DbModels;
using Course.Extensions;
using Course.Repositories;
using Course.Repositories.Models;
using Course.Services.Models;

namespace Course.Services;

public class BaseService
{
    private readonly CourseContext _dbContext;
    private CatJobRepository? _catJobRepo;
    private ClassroomRepository? _classroomRepo;
    private CourseRepository? _courseRepo;
    private RegisterRepository? _registerRepo;
    private StudentRepository? _studentRepo;
    private UserRepository? _userRepo;
    private CatBankRepository? _catBankRepo;
    private NotificationEmailRepository? _emailRepo;
    private CatDocRepository? _catDocRepo;
    private CourseClassroomDocRepository? _courseClassroomDocRepo;
    private ShareDocRepository? _shareDocRepo;
    private FileRepository? _fileRepo;
    private ReserveRepository? _reserveRepo;
    private JobConfigRepository? _jobConfigRepo;
    private BankStatementRepository? _bankStatementRepo;
    private CatEvaluationLevelRepository? _catEvaluationLevelRepo;
    private SurveyRepository? _surveyRepo;
    private PromotionRepository? _promotionRepo;
    private PromotionUserRepository? _promotionUserRepo;
    private EmailLogRepository? _emailLogRepo;

    protected BaseService(CourseContext dbContext)
    {
        _dbContext = dbContext;
    }

    protected UserRepository UserRepo => _userRepo ??= new UserRepository(_dbContext);
    protected CatJobRepository CatJobRepo => _catJobRepo ??= new CatJobRepository(_dbContext);
    protected CourseRepository CourseRepo => _courseRepo ??= new CourseRepository(_dbContext);
    protected ClassroomRepository ClassroomRepo => _classroomRepo ??= new ClassroomRepository(_dbContext);
    protected RegisterRepository RegisterRepo => _registerRepo ??= new RegisterRepository(_dbContext);
    protected StudentRepository StudentRepo => _studentRepo ??= new StudentRepository(_dbContext);
    protected CatBankRepository CatBankRepo => _catBankRepo ??= new CatBankRepository(_dbContext);

    protected NotificationEmailRepository NotificationEmailRepo =>
        _emailRepo ??= new NotificationEmailRepository(_dbContext);

    protected CatDocRepository CatDocRepo => _catDocRepo ??= new CatDocRepository(_dbContext);

    protected CourseClassroomDocRepository CourseClassroomDocRepo =>
        _courseClassroomDocRepo ??= new CourseClassroomDocRepository(_dbContext);

    protected ShareDocRepository ShareDocRepo => _shareDocRepo ??= new ShareDocRepository(_dbContext);
    protected FileRepository FileRepo => _fileRepo ??= new FileRepository(_dbContext);
    protected ReserveRepository ReserveRepo => _reserveRepo ??= new ReserveRepository(_dbContext);
    protected JobConfigRepository JobConfigRepo => _jobConfigRepo ??= new JobConfigRepository(_dbContext);

    protected BankStatementRepository BankStatementRepo =>
        _bankStatementRepo ??= new BankStatementRepository(_dbContext);

    protected CatEvaluationLevelRepository CatEvaluationLevelRepo =>
        _catEvaluationLevelRepo ??= new CatEvaluationLevelRepository(_dbContext);

    protected SurveyRepository SurveyRepo => _surveyRepo ??= new SurveyRepository(_dbContext);
    protected PromotionRepository PromotionRepo => _promotionRepo ??= new PromotionRepository(_dbContext);
    protected PromotionUserRepository PromotionUserRepo => _promotionUserRepo ??= new PromotionUserRepository(_dbContext);
    protected EmailLogRepository EmailLogRepo => _emailLogRepo ??= new EmailLogRepository(_dbContext);

    protected void SaveDbChanges()
    {
        _dbContext.SaveChanges();
    }

    protected void ValidateExist<T>(int id, string message = "Dữ liệu không hợp lệ") where T : class
    {
        if (_dbContext.Find<T>(id) == null)
        {
            throw new BusinessException(message);
        }
    }

    public CourseDto? GetCourseById(int id)
    {
        var course = CourseRepo.GetById(id);
        if (course == null) return null;

        var res = Utils.CastObject<CourseDto>(course);
        res.TotalOpenClassroom = ClassroomRepo.Count(new ListClassroomParam
        {
            CourseId = id,
            IsActive = 1,
            FromStartDate = DateTime.Today,
            ToStartEnrollmentDate = DateTime.Now,
            Status = Constants.CLASSROOM_ACTIVE_STATUS
        });
        res.StartEnrollmentDate = ClassroomRepo.GetNearestStartEnrollmentDate(id);

        return res;
    }

    public ClassroomDto? GetClassroomById(int id)
    {
        var classroom = ClassroomRepo.GetById(id);
        if (classroom == null)
        {
            return null;
        }

        var res = Utils.CastObject<ClassroomDto>(classroom);
        var course = CourseRepo.GetById(classroom.CourseId);
        if (course != null)
        {
            res.Course = Utils.CastObject<CourseDto>(course);
        }

        res.TotalStudent = StudentRepo.Count(new ListStudentParam
        {
            ClassroomId = id
        });

        return res;
    }

    public StudentDto? GetStudentById(int id)
    {
        var student = StudentRepo.GetById(id);
        if (student == null)
        {
            return null;
        }

        var res = Utils.CastObject<StudentDto>(student);
        var course = CourseRepo.GetById(student.CourseId);
        var user = UserRepo.GetById(student.UserId);
        if (course != null)
        {
            res.Course = Utils.CastObject<CourseDto>(course);
        }

        if (student.ClassroomId != null)
        {
            var classroom = ClassroomRepo.GetById(student.ClassroomId);
            if (classroom != null)
            {
                res.Classroom = Utils.CastObject<ClassroomDto>(classroom);
            }
        }

        if (user != null)
        {
            res.User = Utils.CastObject<UserDto>(user);
        }

        return res;
    }

    public RegisterDto? GetRegisterById(int id, bool getStudent = true)
    {
        var register = RegisterRepo.GetById(id);
        if (register == null)
        {
            return null;
        }

        var res = Utils.CastObject<RegisterDto>(register);
        var course = CourseRepo.GetById(register.CourseId);
        var classroom = ClassroomRepo.GetById(register.ClassroomId);
        var user = UserRepo.GetById(register.UserId);
        if (course != null)
        {
            res.Course = Utils.CastObject<CourseDto>(course);
        }

        if (classroom != null)
        {
            res.Classroom = Utils.CastObject<ClassroomDto>(classroom);
        }

        if (user != null)
        {
            res.User = Utils.CastObject<UserDto>(user);
        }

        if (register.ApproveBy != null && register.ApproveBy > 0)
        {
            var approveUser = UserRepo.GetById(register.ApproveBy);
            if (approveUser != null)
            {
                res.ApproveUser = Utils.CastObject<UserDto>(approveUser);
            }
        }

        if (!getStudent) return res;
        var student = StudentRepo.List(new ListStudentParam
        {
            RegisterId = id
        }).FirstOrDefault();
        if (student == null) return res;
        res.StudentId = student.Id;
        res.Student = student;
        if (student.ClassroomId != null)
        {
            var currentClassroom = ClassroomRepo.GetById(student.ClassroomId);
            if (currentClassroom != null)
            {
                res.CurrentClassroom = Utils.CastObject<ClassroomDto>(currentClassroom);
            }
        }

        return res;
    }

    public T ValidateId<T>(object? id, string message) where T : class
    {
        var entity = _dbContext.Set<T>().Find(id);
        if (entity == null) throw new BusinessException(message);
        return entity;
    }
}