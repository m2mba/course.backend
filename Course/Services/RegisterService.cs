using System.Text;
using Course.Common;
using Course.Common.Dto;
using Course.Common.Email;
using Course.Common.Email.Template;
using Course.DbModels;
using Course.Extensions;
using Course.Repositories.Models;
using Course.Services.Models;

namespace Course.Services;

public class RegisterService : BaseService
{
    public RegisterService(CourseContext dbContext) : base(dbContext)
    {
    }

    public Register RegisterClassroom(int userId, int classroomId)
    {
        var classroom = ClassroomRepo.GetById(classroomId);
        if (classroom == null)
        {
            throw new BusinessException("Lớp học không tồn tại");
        }

        var now = DateTime.Today;
        if (classroom.StartEnrollmentDate == null || classroom.StartEnrollmentDate > DateTime.Now)
        {
            throw new BusinessException("Lớp học chưa tới thời gian đăng ký");
        }

        if (classroom.StartDate == null || classroom.StartDate < now)
        {
            throw new BusinessException("Lớp học đã hết thời gian đăng ký");
        }

        if (classroom.Status != Constants.CLASSROOM_ACTIVE_STATUS)
        {
            throw new BusinessException("Lớp học đã hết thời gian đăng ký");
        }

        if (classroom.TotalRegistered.GetValueOrDefault() >= classroom.TotalSlot.GetValueOrDefault())
        {
            throw new BusinessException("Lớp học đã đủ số lượng đăng ký");
        }

        var oldRegister = RegisterRepo.Count(new ListRegisterParam
        {
            UserId = userId,
            ClassroomId = classroomId,
            Statuses = new List<string>
            {
                Constants.REGISTER_WAIT_PAYMENT_STATUS,
                Constants.REGISTER_SUCCESS_STATUS,
                Constants.REGISTER_LEARNING_STATUS,
                Constants.REGISTER_RESERVE_STATUS
            }
        });
        if (oldRegister > 0)
        {
            throw new BusinessException("Bạn đã đăng ký đợt tuyển sinh này trước đó");
        }

        classroom.TotalRegistered = classroom.TotalRegistered.GetValueOrDefault() + 1;
        var course = CourseRepo.GetById(classroom.CourseId);
        if (course != null)
        {
            course.TotalRegisteredStudent += 1;
        }

        var config = JobConfigRepo.List().FirstOrDefault();

        var (promotion, promotionUserId, discountAmount) = GetPromotion(userId, classroom);
        var actualFee = promotion == null ? classroom.Fee : classroom.Fee - discountAmount;
        if (actualFee < 0)
        {
            actualFee = 0;
        }

        if (promotionUserId > 0)
        {
            var userPromotion = PromotionUserRepo.GetById(promotionUserId);
            if (userPromotion != null)
            {
                userPromotion.UsedTimes = userPromotion.UsedTimes.GetValueOrDefault() + 1;
                PromotionUserRepo.Update(userPromotion);
            }
        }

        var catBank = CatBankRepo.List().FirstOrDefault();
        var expHour = 24;
        if (config != null)
        {
            expHour = config.WaitPaymentHour;
        }
        var register = new Register
        {
            Code = GenerateRegisterCode(classroom),
            ClassroomId = classroomId,
            CourseId = classroom.CourseId,
            UserId = userId,
            OrgFee = classroom.Fee,
            Fee = actualFee,
            RegisterAt = DateTime.Now,
            ExpirePaymentDate = DateTime.Now.AddHours(expHour),
            Status = Constants.REGISTER_WAIT_PAYMENT_STATUS,
            SharedDoc = 0,
            SharedVideo = 0,
            BankCode = catBank?.BankCode,
            BankBranchName = catBank?.BankBranchName,
            AccountNumber = catBank?.AccountNumber,
            AccountHolder = catBank?.AccountHolder,
            DiscountAmount = discountAmount,
            PromotionUserId = promotionUserId,
            PromotionId = promotion?.Id
        };
        RegisterRepo.Add(register);
        SaveDbChanges();
        var user = UserRepo.GetById(userId);
        if (user == null) return register;
        var baseDomain = ConfigureService.GetString("StudentWebUrl");
        var apiDomain = ConfigureService.GetString("BaseDomain");
        var mail = new ContentTemplateRegisterClassroom
        {
            CourseName = course?.Name ?? "",
            RefLink = $"{baseDomain}/courses",
            GuideLink = $"{apiDomain}/api/v1/files/public/guide_new_student.png",
            FullName = user.FullName,
            Phone = user.Phone,
            Email = user.Email,
            RegisterAt = register.RegisterAt,
            ExpAt = register.ExpirePaymentDate ?? DateTime.Now.AddHours(24),
            Fee = register.Fee.GetValueOrDefault(),
            CourseInfo = (classroom?.Code ?? "") + " - " + (course?.Name ?? "") + " - " + "Khai giảng " +
                         (classroom?.StartDate ?? DateTime.Now).ToString("dd/MM/yyyy")
        };
        EmailHelper.Send(user.Email, mail.FormatContent(), mail.GetTitle());
        return register;
    }

    private string GenerateRegisterCode(Classroom classroom)
    {
        var count = RegisterRepo.Count(new ListRegisterParam
        {
            CourseId = classroom.CourseId,
            ClassroomId = classroom.Id,
            FromRegisterAt = DateTime.Today,
            ToRegisterAt = DateTime.Now.FormatToDate()
        }) + 1;
        return (string.IsNullOrWhiteSpace(classroom.Code) ? "" : classroom.Code.Replace("-", "").Replace("_", "")) +
               DateTime.Now.ToString("yyMMdd") + (count < 10 ? "0" + count : count.ToString());
    }

    public void RegisterEmail(int courseId, string email)
    {
        if (NotificationEmailRepo.Count(new ListEmailParam { Email = email, CourseId = courseId }) > 0)
        {
            return;
        }

        var notificationEmail = new NotificationEmail
        {
            CourseId = courseId,
            Email = email,
            CreatedAt = DateTime.Now
        };
        NotificationEmailRepo.Add(notificationEmail);
        SaveDbChanges();
    }

    public string ListRegisterEmails(ListRegisterParam param)
    {
        var userIds = RegisterRepo.GetQuery(param).Select(x => x.UserId).ToList();
        if (!userIds.Any()) return "";
        var emails = UserRepo.GetQuery(new ListUserParam { Ids = userIds }).Select(x => x.Email).Distinct().ToList();
        return string.Join(',', emails);
    }

    public PageDto<RegisterDto> ListRegisters(ListRegisterParam param, Option? option = null)
    {
        var registers = RegisterRepo.List(param, option);
        var courseIds = registers.Select(x => x.CourseId).ToList();
        var userIds = registers.Select(x => x.UserId).ToList();
        var approveUserIds = registers.Where(x => x.ApproveBy != null && x.ApproveBy > 0)
            .Select(x => x.ApproveBy!.Value).ToList();
        var classroomIds = registers.Select(x => x.ClassroomId).ToList();
        var courses = CourseRepo.List(new ListCourseParam { Ids = courseIds });
        var classrooms = ClassroomRepo.List(new ListClassroomParam { Ids = classroomIds });
        var users = UserRepo.List(new ListUserParam { Ids = userIds });
        var approveUsers = approveUserIds.HasValue()
            ? UserRepo.List(new ListUserParam { Ids = approveUserIds })
            : new List<User>();
        var students = StudentRepo.List(new ListStudentParam { RegisterIds = registers.Select(x => x.Id).ToList() });
        var currentClassroomIds = students.Where(x => x.ClassroomId > 0).Select(x => x.ClassroomId.GetValueOrDefault())
            .ToList();
        var currentClassrooms = ClassroomRepo.List(new ListClassroomParam { Ids = currentClassroomIds });
        var list = new List<RegisterDto>();
        foreach (var register in registers)
        {
            var item = Utils.CastObject<RegisterDto>(register);
            var course = courses.FirstOrDefault(x => x.Id == register.CourseId);
            if (course != null)
            {
                item.Course = Utils.CastObject<CourseDto>(course);
            }

            var classroom = classrooms.FirstOrDefault(x => x.Id == register.ClassroomId);
            if (classroom != null)
            {
                item.Classroom = Utils.CastObject<ClassroomDto>(classroom);
            }

            var user = users.FirstOrDefault(x => x.Id == register.UserId);
            if (user != null)
            {
                item.User = Utils.CastObject<UserDto>(user);
            }

            if (register.ApproveBy != null && register.ApproveBy > 0)
            {
                var approveUser = approveUsers.FirstOrDefault(x => x.Id == register.ApproveBy);
                if (approveUser != null)
                {
                    item.ApproveUser = Utils.CastObject<UserDto>(approveUser);
                }
            }

            var student = students.FirstOrDefault(x => x.RegisterId == register.Id);
            if (student != null)
            {
                item.StudentId = student.Id;
                item.Student = student;

                if (student.ClassroomId != null)
                {
                    var currentClassroom = currentClassrooms.FirstOrDefault(x => x.Id == student.ClassroomId);
                    if (currentClassroom != null)
                    {
                        item.CurrentClassroom = Utils.CastObject<ClassroomDto>(currentClassroom);
                    }
                }
            }

            list.Add(item);
        }

        return new PageDto<RegisterDto>
        {
            List = list,
            Total = option?.Paging ?? false ? RegisterRepo.Count(param) : 0
        };
    }

    public int CountRegisters(ListRegisterParam param)
    {
        return RegisterRepo.Count(param);
    }

    public void UpdateRegisterStatus(int id, string status, int userId, string? note)
    {
        var register = RegisterRepo.GetById(id);
        if (register == null)
        {
            throw new BusinessException("Không tồn tại đăng ký");
        }

        register.Status = status;
        register.ApproveNote = note;
        register.ApprovedAt = DateTime.Now;
        var user = UserRepo.GetById(register.UserId);
        var classroom = ClassroomRepo.GetById(register.ClassroomId);
        var course = CourseRepo.GetById(register.CourseId);
        var baseDomain = ConfigureService.GetString("StudentWebUrl");
        var apiDomain = ConfigureService.GetString("BaseDomain");

        if (status == Constants.REGISTER_CANCEL_STATUS)
        {
            register.ApproveBy = userId;
            register.ApproveNote = note ?? "Admin hủy đăng ký";
            if (classroom != null && classroom.TotalRegistered.HasValue && classroom.TotalRegistered > 0)
            {
                classroom.TotalRegistered -= 1;
            }

            if (course != null && course.TotalRegisteredStudent > 0)
            {
                course.TotalRegisteredStudent -= 1;
            }

            if (register.PromotionUserId > 0)
            {
                var userPromotion = PromotionUserRepo.GetById(register.PromotionUserId);
                if (userPromotion != null && userPromotion.UsedTimes > 0)
                {
                    userPromotion.UsedTimes = userPromotion.UsedTimes.GetValueOrDefault() - 1;
                    PromotionUserRepo.Update(userPromotion);
                }
            }

            if (user != null)
            {
                var mail = new ContentTemplateRegisterCancel
                {
                    RefLink = $"{baseDomain}/courses",
                    FullName = user.FullName,
                    Phone = user.Phone,
                    Email = user.Email,
                    Reason = register.ApproveNote,
                    RegisterAt = register.RegisterAt,
                    Fee = register.Fee.GetValueOrDefault(),
                    CourseInfo = (classroom?.Code ?? "") + " - " + (course?.Name ?? "") + " - " + "Khai giảng " +
                                 (classroom?.StartDate ?? DateTime.Now).ToString("dd/MM/yyyy")
                };
                EmailHelper.Send(user.Email, mail.FormatContent(), mail.GetTitle());
            }
        }

        if (status == Constants.REGISTER_SUCCESS_STATUS)
        {
            var student = new Student
            {
                RegisterId = id,
                ClassroomId = register.ClassroomId,
                OrgClassroomId = register.ClassroomId,
                CourseId = register.CourseId,
                UserId = register.UserId,
                RegisteredAt = register.RegisterAt,
                Status = Constants.STUDENT_WAIT_JOIN_STATUS,
                CreatedAt = DateTime.Now
            };
            StudentRepo.Add(student);

            // Update payment
            var bank = CatBankRepo.List().FirstOrDefault();
            if (bank != null)
            {
                register.BankCode = bank.BankCode;
                register.BankName = bank.BankName;
                register.BankBranchName = bank.BankBranchName;
                register.AccountHolder = bank.AccountHolder;
                register.AccountNumber = bank.AccountNumber;
            }

            if (user != null)
            {
                var mail = new ContentTemplateRegisterApprove
                {
                    CourseName = course?.Name ?? "",
                    RefLink = $"{baseDomain}/courses",
                    GuideLink = $"{apiDomain}/api/v1/files/public/guide_new_student.png",
                    FullName = user.FullName,
                    Phone = user.Phone,
                    Email = user.Email,
                    RegisterAt = register.RegisterAt,
                    ApproveAt = register.ApprovedAt ?? DateTime.Now,
                    Fee = register.Fee.GetValueOrDefault(),
                    CourseInfo = (classroom?.Code ?? "") + " - " + (course?.Name ?? "") + " - " + "Khai giảng " +
                                 (classroom?.StartDate ?? DateTime.Now).ToString("dd/MM/yyyy")
                };
                EmailHelper.Send(user.Email, mail.FormatContent(), mail.GetTitle());
            }
        }

        SaveDbChanges();
    }

    public void Reserve(int userId, int registerId, int? classroomId)
    {
        var register = RegisterRepo.GetById(registerId);
        if (register == null || register.UserId != userId)
        {
            throw new BusinessException("Không tồn tại đăng ký");
        }

        var allowStatuses = new List<string>
        {
            Constants.REGISTER_SUCCESS_STATUS, Constants.REGISTER_RESERVE_STATUS, Constants.REGISTER_LEARNING_STATUS
        };

        if (!allowStatuses.Contains(register.Status))
        {
            throw new BusinessException("Bạn không được phép bảo lưu ở trạng thái này");
        }

        if (register.RegisterAt.AddYears(1) < DateTime.Today)
        {
            throw new BusinessException("Bạn đã hết thời gian bảo lưu khóa học");
        }

        Classroom? newClass = null;
        if (classroomId != null)
        {
            if (classroomId == register.ClassroomId)
            {
                throw new BusinessException("Bạn phải chọn lớp học với lớp hiện tại");
            }

            newClass = ClassroomRepo.GetById(classroomId.Value);
            if (newClass == null)
            {
                throw new BusinessException("Không tồn tại lớp học");
            }

            if (register.CourseId != newClass.CourseId)
            {
                throw new BusinessException("Lớp học không cùng khóa học được đăng kí");
            }
        }

        var totalReserve = ReserveRepo.List(new ListReserveParam
        {
            RegisterId = registerId,
            UserId = userId
        }).Count(x => x.ToClassroomId.HasValue);
        if (totalReserve >= Constants.MAX_RESERVE)
        {
            throw new BusinessException($"Bạn đã bảo lưu {totalReserve} trước đó");
        }

        var student = StudentRepo.List(new ListStudentParam
        {
            RegisterId = registerId,
            CourseId = register.CourseId,
            UserId = register.UserId
        }).MaxBy(x => x.Id);

        if (student == null)
        {
            throw new BusinessException("Bạn phải đăng ký khóa học thành công trước đó");
        }

        var oldClassroomId = student.ClassroomId;

        if (oldClassroomId == null && register.Status != Constants.REGISTER_RESERVE_STATUS)
        {
            throw new BusinessException("Bạn phải đăng ký khóa học thành công trước đó");
        }

        if (classroomId == null && student.ClassroomId == null)
        {
            throw new BusinessException("Bạn phải chọn lớp cần chuyển tới");
        }

        if (oldClassroomId != null)
        {
            var config = JobConfigRepo.List().FirstOrDefault();
            if (config != null && config.ReserveInDay > 0)
            {
                var oldClassroom = ClassroomRepo.GetById(oldClassroomId);
                if (oldClassroom != null && oldClassroom.StartDate != null &&
                    oldClassroom.StartDate.Value.Date.AddDays(config.ReserveInDay.Value) < DateTime.Today)
                {
                    throw new BusinessException(
                        $"Bạn chỉ được phép bảo lưu trong vòng {config.ReserveInDay.Value} ngày kể từ khi lớp học khai giảng");
                }
            }
        }

        student.ClassroomId = classroomId;
        student.ZaloAdded = null;

        var reserve = new Reserve
        {
            RegisterId = registerId,
            CourseId = register.CourseId,
            UserId = register.UserId,
            FromClassroomId = oldClassroomId,
            ToClassroomId = classroomId,
            Status = Constants.REGISTER_RESERVE_STATUS,
            CreatedAt = DateTime.Now
        };
        ReserveRepo.Add(reserve);

        if (oldClassroomId != null)
        {
            var oldClass = ClassroomRepo.GetById(oldClassroomId);
            if (oldClass != null)
            {
                oldClass.TotalReserveOut = oldClass.TotalReserveOut.GetValueOrDefault() + 1;
            }
        }

        if (newClass != null)
        {
            newClass.TotalReserveIn = newClass.TotalReserveIn.GetValueOrDefault() + 1;
        }

        register.Status = Constants.REGISTER_RESERVE_STATUS;
        SaveDbChanges();
    }

    public PageDto<ReserveDto> ListReserves(ListReserveParam param, Option? option = null)
    {
        var reserves = ReserveRepo.List(param, option);
        var toClassroomIds = reserves.Where(x => x.ToClassroomId != null && x.ToClassroomId > 0)
            .Select(x => x.ToClassroomId!.Value).ToList();
        var toClassrooms = ClassroomRepo.List(new ListClassroomParam { Ids = toClassroomIds });
        var list = new List<ReserveDto>();
        foreach (var reserve in reserves)
        {
            var item = Utils.CastObject<ReserveDto>(reserve);
            if (item.ToClassroomId != null && item.ToClassroomId > 0)
            {
                var classroom = toClassrooms.FirstOrDefault(x => x.Id == reserve.ToClassroomId);
                if (classroom != null)
                {
                    item.ToClassroom = Utils.CastObject<ClassroomDto>(classroom);
                }
            }

            list.Add(item);
        }

        return new PageDto<ReserveDto>
        {
            List = list,
            Total = option?.Paging ?? false ? ReserveRepo.Count(param) : 0
        };
    }

    public (Promotion?, int?, long?) GetPromotion(int userId, Classroom classroom)
    {
        var applyTypes = new List<string> { Constants.PROMOTION_APPLY_TYPE_ALL };
        if (RegisterRepo.Count(new ListRegisterParam
            {
                UserId = userId,
                ExcStatuses = new List<string> { Constants.REGISTER_CANCEL_STATUS }
            }) > 0)
        {
            applyTypes.Add(Constants.PROMOTION_APPLY_TYPE_OLD_STUDENT);
        }

        var promotions = PromotionRepo.List(new ListPromotionParam
        {
            Available = true,
            ApplyTypes = applyTypes,
            CourseId = classroom.CourseId,
            UserId = userId
        });

        var userPromotions = PromotionUserRepo.List(new ListPromotionUserParam
        {
            Available = true,
            UserId = userId
        });
        var fee = classroom.Fee.GetValueOrDefault();
        var discountPromotions = promotions.Select(x => new
        {
            PromotionUserId = -1,
            PromotionId = x.Id,
            PromotionApplyType = x.ApplyType,
            PromotionTimes = x.Times,
            Valid = false,
            x.Amount,
            x.Percent,
            DiscountAmount = x.Amount.GetValueOrDefault() + (long)x.Percent.GetValueOrDefault() * fee / 100
        }).ToList();

        discountPromotions.AddRange(userPromotions.Select(x => new
        {
            PromotionUserId = x.Id,
            x.PromotionId,
            PromotionApplyType = x.ApplyType,
            PromotionTimes = x.Times - x.UsedTimes.GetValueOrDefault(),
            Valid = true,
            x.Amount,
            x.Percent,
            DiscountAmount = x.Amount.GetValueOrDefault() + (long)x.Percent.GetValueOrDefault() * fee / 100
        }).ToList());

        discountPromotions = discountPromotions.OrderByDescending(x => x.DiscountAmount).ToList();
        var promotionIds = promotions.Select(x => (int?)x.Id).ToList();
        var promotionRegisters = RegisterRepo.List(new ListRegisterParam
        {
            UserId = userId,
            ExcStatuses = new List<string> { Constants.REGISTER_CANCEL_STATUS },
            PromotionIds = promotionIds
        });
        foreach (var item in discountPromotions)
        {
            if (item.Valid)
            {
                return (new Promotion
                {
                    Id = item.PromotionId,
                    ApplyType = item.PromotionApplyType,
                    Amount = item.DiscountAmount,
                    Percent = item.Percent
                }, item.PromotionUserId, item.DiscountAmount);
            }

            var totalAppliedPromotion = promotionRegisters.Count(x => x.PromotionId == item.PromotionId);
            if (item.PromotionTimes > totalAppliedPromotion)
            {
                return (new Promotion
                {
                    Id = item.PromotionId,
                    ApplyType = item.PromotionApplyType,
                    Amount = item.DiscountAmount,
                    Percent = item.Percent
                }, null, item.DiscountAmount);
            }
        }

        return (null, null, null);
    }

    public void AddZalo(int registerId)
    {
        var register = RegisterRepo.GetById(registerId);
        if (register == null)
        {
            throw new BusinessException("Không tồn tại đăng ký");
        }

        if (register.Status == Constants.REGISTER_CANCEL_STATUS ||
            register.Status == Constants.REGISTER_WAIT_PAYMENT_STATUS)
        {
            throw new BusinessException("Trạng thái đăng ký không hợp lệ");
        }

        register.ZaloAdded = 1;
        var students = StudentRepo.List(new ListStudentParam
        {
            RegisterId = registerId
        });
        foreach (var student in students)
        {
            student.ZaloAdded = 1;
        }

        SaveDbChanges();
    }

    public (long, string) GetQr(int id)
    {
        var register = RegisterRepo.GetById(id);
        if (register == null)
        {
            throw new BusinessException("Không tồn tại đăng ký");
        }

        var fee = register.Fee.GetValueOrDefault();
        var qrFee = $"54{FormatLength(fee.ToString().Length)}{fee}";
        var qrRegisterCode = $"08{FormatLength(register.Code.Length)}{register.Code}";
        qrRegisterCode = $"62{FormatLength(qrRegisterCode.Length)}{qrRegisterCode}";
        var qr =
            $"3T02227022600008001I235016000A1R01177I0AR02600F1214108F6100022F2003017030Q010203{qrFee}5802VN{qrRegisterCode}6304";
        var data = Encoding.ASCII.GetBytes(qr);
        var crc = CalculateCrc16(data);
        return (fee, qr + crc.ToString("X4"));
    }

    private static string FormatLength(int length)
    {
        if (length < 10) return "0" + length;
        return length.ToString();
    }

    private static ushort CalculateCrc16(byte[] data)
    {
        const ushort crc = 0xFFFF; // initial value
        return crc;
    }
}