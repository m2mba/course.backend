using Course.Common;

namespace Course.Services.Models;

public class CourseDto : DbModels.Course
{
    public int TotalOpenClassroom { get; set; }
    
    public DateTime? StartEnrollmentDate { get; set; }

    public List<CourseDetail>? Details
    {
        get
        {
            try
            {
                return Utils.NormalDeserializeObject<List<CourseDetail>>(Content);
            }
            catch
            {
                // Ignored
                return new List<CourseDetail>();
            }
        }
    }
}

public class CourseDetail
{
    public string? Title { get; set; }
    public List<string>? Lines { get; set; }
}