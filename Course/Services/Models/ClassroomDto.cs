using Course.Common;
using Course.DbModels;

namespace Course.Services.Models;

public class ClassroomDto : Classroom
{
    public virtual CourseDto? Course { get; set; }
    public int TotalStudent { get; set; }

    public bool AllowRegister
    {
        get
        {
            if (TotalRegistered.GetValueOrDefault() >= TotalSlot.GetValueOrDefault())
            {
                return false;
            }

            if (StartEnrollmentDate == null || StartEnrollmentDate.Value > DateTime.Now)
            {
                return false;
            }

            return StartDate >= DateTime.Today;
        }
    }

    public bool AllowEmailNotify =>
        IsActive == 1 && Status != Constants.CLASSROOM_STOP_STATUS && StartDate > DateTime.Today;
}