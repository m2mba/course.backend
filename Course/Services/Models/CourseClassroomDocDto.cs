using Course.DbModels;

namespace Course.Services.Models;

public class CourseClassroomDocDto: CourseClassroomDoc
{
    public virtual DocDto? Doc { get; set; }
    public virtual CourseDto? Course { get; set; }
    public virtual ClassroomDto? Classroom { get; set; }
}