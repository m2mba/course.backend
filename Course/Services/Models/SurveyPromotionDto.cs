namespace Course.Services.Models;

public class SurveyPromotionDto
{
    public bool HasPromotion { get; set; }
    public string? PromotionInfo { get; set; }
    public DateTime? PromotionExpireDate { get; set; }
}