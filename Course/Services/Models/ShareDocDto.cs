using Course.DbModels;

namespace Course.Services.Models;

public class ShareDocDto: ShareDoc
{
    public virtual DocDto? Doc { get; set; }
    public virtual CourseDto? Course { get; set; }
    public virtual ClassroomDto? Classroom { get; set; }
    public virtual UserDto? ShareUser { get; set; }
}