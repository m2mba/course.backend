using Course.DbModels;

namespace Course.Services.Models;

public class ReserveDto: Reserve
{
    public ClassroomDto? ToClassroom { get; set; }
}