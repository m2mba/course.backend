namespace Course.Services.Models;

public class EvaluateStudentDto
{
    public int StudentId { get; set; }
    public int EvaluatedBy { get; set; }
    public string TestLink { get; set; } = null!;

    public int PassLevelId { get; set; }

    public string? Detail { get; set; }

    public string CertificateFileHash { get; set; } = null!;

    public sbyte IsPublic { get; set; }
}