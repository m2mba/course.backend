using Course.DbModels;

namespace Course.Services.Models;

public class ImportPaymentDto
{
    public Dictionary<int, string> Errors { get; set; } = new();
    public List<BankStatement> Statements { get; set; } = new();
    public int TotalSuccess { get; set; }
    public int TotalFailed { get; set; }
}