using Course.Common;
using Course.DbModels;

namespace Course.Services.Models;

public class RegisterDto: Register
{
    public int? StudentId { get; set; }

    public bool AllowSurvey
    {
        get
        {
            if (Student == null) return false;
            if (CurrentClassroom?.StartDate == null) return false;
            var allowClassroomStatuses = new List<string> { Constants.CLASSROOM_LEARNING_STATUS, Constants.CLASSROOM_FINISH_STATUS };
            if (!allowClassroomStatuses.Contains(CurrentClassroom.Status)) return false;
            var allowStudentStatuses = new List<string> { Constants.REGISTER_LEARNING_STATUS, Constants.REGISTER_FINISH_STATUS };
            return allowStudentStatuses.Contains(Status);
        }
    }

    public Student? Student { get; set; }
    public virtual CourseDto? Course { get; set; }
    public virtual ClassroomDto? Classroom { get; set; }
    public virtual ClassroomDto? CurrentClassroom { get; set; }
    public virtual UserDto? User { get; set; }
    public virtual UserDto? ApproveUser { get; set; }
}