using Course.DbModels;

namespace Course.Services.Models;

public class SurveyDto: Survey
{
    public Student? Student { get; set; }
    public virtual CourseDto? Course { get; set; }
    public virtual ClassroomDto? Classroom { get; set; }
    public virtual ClassroomDto? CurrentClassroom { get; set; }
    public virtual UserDto? User { get; set; }
    public virtual UserDto? ApproveUser { get; set; }
}