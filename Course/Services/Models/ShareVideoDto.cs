namespace Course.Services.Models;

public class ShareDocActionDto
{
    public int CourseId { get; set; }
    public int ClassroomId { get; set; }
    public List<int> DocIds { get; set; } = new();
    public int ShareUserId { get; set; }
    public string Emails { get; set; } = null!;
}