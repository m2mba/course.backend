using Course.DbModels;

namespace Course.Services.Models;

public class EvaluationDto: Evaluation
{
    public virtual Student? Student { get; set; }
}