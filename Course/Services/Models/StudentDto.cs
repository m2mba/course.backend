using Course.DbModels;

namespace Course.Services.Models;

public class StudentDto: Student
{
    public CourseDto? Course { get; set; }
    public ClassroomDto? Classroom { get; set; }
    public ClassroomDto? OrgClassroom { get; set; }
    public UserDto? User { get; set; }
    public CatEvaluationLevel? PassLevel { get; set; }
}