using Course.DbModels;

namespace Course.Services;

public class CatBankService : BaseService
{
    public CatBankService(CourseContext dbContext) : base(dbContext)
    {
    }

    public CatBank? GetBankById(int id)
    {
        return CatBankRepo.GetById(id);
    }

    public List<CatBank> ListBanks()
    {
        return CatBankRepo.List();
    }
}