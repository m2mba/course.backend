using Course.DbModels;
using Course.Extensions;
using Course.Repositories.Models;

namespace Course.Services;

public class UserService : BaseService
{
    public UserService(CourseContext dbContext) : base(dbContext)
    {
    }

    public User? GetUserById(int id)
    {
        return UserRepo.GetById(id);
    }

    public User? GetUserByPhone(string? phone)
    {
        return UserRepo.List(new ListUserParam { Phone = phone, IsActive = 1 }).FirstOrDefault();
    }

    public User? GetUserByEmail(string? email)
    {
        return UserRepo.List(new ListUserParam { Email = email, IsActive = 1 }).FirstOrDefault();
    }

    public void ValidateEmail(string email, int userId = 0)
    {
        var user = GetUserByEmail(email);
        if ((userId <= 0 && user != null) || (userId > 0 && user != null && user.Id != userId))
            throw new BusinessException("Email đã tồn tại trong hệ thống");
    }

    public void ValidatePhone(string phone, int userId = 0)
    {
        var user = GetUserByPhone(phone);
        if ((userId <= 0 && user != null) || (userId > 0 && user != null && user.Id != userId))
            throw new BusinessException("Số điện thoại đã tồn tại trong hệ thống");
    }

    public void UpdateUser(User user)
    {
        UserRepo.Update(user);
        SaveDbChanges();
    }

    public void CreateUser(User user)
    {
        UserRepo.Add(user);
        SaveDbChanges();
    }
}