﻿using System;
using System.Collections.Generic;

namespace Course.DbModels;

public partial class CatBank
{
    public int Id { get; set; }

    public string BankCode { get; set; } = null!;

    public string BankName { get; set; } = null!;

    public string? BankBranchName { get; set; }

    public string AccountNumber { get; set; } = null!;

    public string AccountHolder { get; set; } = null!;
}
