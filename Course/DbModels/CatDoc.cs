﻿using System;
using System.Collections.Generic;

namespace Course.DbModels;

public partial class CatDoc
{
    public int Id { get; set; }

    public string DocType { get; set; } = null!;

    public string Name { get; set; } = null!;

    public string Url { get; set; } = null!;

    public string HashUrl { get; set; } = null!;

    public string Method { get; set; } = null!;

    public sbyte IsActive { get; set; }
}
