﻿using System;
using System.Collections.Generic;

namespace Course.DbModels;

public partial class Promotion
{
    public int Id { get; set; }

    public int? CourseId { get; set; }

    public decimal? Percent { get; set; }

    public long? Amount { get; set; }

    public int Times { get; set; }

    public int? SurveyTimes { get; set; }

    public DateTime? FromDate { get; set; }

    public DateTime? ToDate { get; set; }

    /// <summary>
    /// - ALL: Tat ca
    /// - OLD_STUDENT: Hoc vien cu
    /// - SURVEY: Hoc vien tham gia survey
    /// - SHARE: Chia se kien thuc
    /// </summary>
    public string ApplyType { get; set; } = null!;

    public int? PeriodDay { get; set; }

    public int? IsActive { get; set; }
}
