﻿using System;
using System.Collections.Generic;

namespace Course.DbModels;

public partial class FeeType
{
    public int Id { get; set; }

    public string? FeeName { get; set; }

    public string? FeeDes { get; set; }
}
