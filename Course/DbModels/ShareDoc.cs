﻿using System;
using System.Collections.Generic;

namespace Course.DbModels;

public partial class ShareDoc
{
    public int Id { get; set; }

    public int DocId { get; set; }

    public int ShareUserId { get; set; }

    public string Emails { get; set; } = null!;

    public int CourseId { get; set; }

    public int ClassroomId { get; set; }

    public string? Group { get; set; }

    public DateTime CreatedAt { get; set; }
}
