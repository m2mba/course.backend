﻿using System;
using System.Collections.Generic;

namespace Course.DbModels;

public partial class Survey
{
    public int Id { get; set; }

    public int StudentId { get; set; }

    public int Rate { get; set; }

    public string? Note { get; set; }

    public DateTime CreatedAt { get; set; }

    public int CourseId { get; set; }

    public int ClassroomId { get; set; }

    public int UserId { get; set; }

    public DateTime UpdatedAt { get; set; }

    public int UpdatedBy { get; set; }

    public DateTime? ApprovedAt { get; set; }

    public int? ApprovedBy { get; set; }

    public string? ApprovedNote { get; set; }

    public string Status { get; set; } = null!;
}
