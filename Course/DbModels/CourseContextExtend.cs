﻿using Course.Services;
using Microsoft.EntityFrameworkCore;

namespace Course.DbModels;

public class CourseContextExtend : CourseContext
{
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseMySQL(ConfigureService.GetConnectionString("Course"));
    }
}