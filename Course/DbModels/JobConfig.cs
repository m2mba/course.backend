﻿using System;
using System.Collections.Generic;

namespace Course.DbModels;

public partial class JobConfig
{
    public int Id { get; set; }

    public int WaitPaymentHour { get; set; }

    public int AllowCancelRegister { get; set; }

    public int AllowConfirmByTransaction { get; set; }

    public int? ReserveInDay { get; set; }
}
