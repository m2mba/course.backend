﻿using System;
using System.Collections.Generic;

namespace Course.DbModels;

public partial class PromotionUser
{
    public int Id { get; set; }

    public int UserId { get; set; }

    public int PromotionId { get; set; }

    public decimal? Percent { get; set; }

    public long? Amount { get; set; }

    public int Times { get; set; }

    public int? UsedTimes { get; set; }

    public DateTime? ExpireDate { get; set; }

    /// <summary>
    /// - ALL: Tat ca
    /// - OLD_STUDENT: Hoc vien cu
    /// - SURVEY: Hoc vien tham gia survey
    /// - SHARE: Chia se kien thuc
    /// </summary>
    public string ApplyType { get; set; } = null!;

    public int? PeriodDay { get; set; }

    public DateTime CreatedDate { get; set; }
}
