﻿using System;
using System.Collections.Generic;

namespace Course.DbModels;

public partial class User
{
    public int Id { get; set; }

    public string Password { get; set; } = null!;

    public string FullName { get; set; } = null!;

    public string FirstName { get; set; } = null!;

    public string Email { get; set; } = null!;

    public string Phone { get; set; } = null!;

    public DateTime CreatedAt { get; set; }

    public DateTime UpdatedAt { get; set; }

    public string Status { get; set; } = null!;

    public string Roles { get; set; } = null!;

    public string? RefreshToken { get; set; }

    public sbyte IsActive { get; set; }

    public int? CatJobId { get; set; }
}
