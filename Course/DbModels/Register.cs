﻿using System;
using System.Collections.Generic;

namespace Course.DbModels;

public partial class Register
{
    public int Id { get; set; }

    public int UserId { get; set; }

    public int CourseId { get; set; }

    public int ClassroomId { get; set; }

    public string Code { get; set; } = null!;

    public long? Fee { get; set; }

    public DateTime RegisterAt { get; set; }

    public DateTime? ExpirePaymentDate { get; set; }

    public string Status { get; set; } = null!;

    public string? BankCode { get; set; }

    public string? BankName { get; set; }

    public string? BankBranchName { get; set; }

    public string? AccountNumber { get; set; }

    public string? AccountHolder { get; set; }

    public DateTime? ApprovedAt { get; set; }

    public string? ApproveNote { get; set; }

    public int? ApproveBy { get; set; }

    public sbyte SharedVideo { get; set; }

    public sbyte SharedDoc { get; set; }

    public string? EntryNumber { get; set; }

    public int? PromotionId { get; set; }

    public int? PromotionUserId { get; set; }

    public long? DiscountAmount { get; set; }

    public long? OrgFee { get; set; }

    public int? ZaloAdded { get; set; }
}
