﻿using System;
using System.Collections.Generic;

namespace Course.DbModels;

public partial class Classroom
{
    public int Id { get; set; }

    public string? Code { get; set; }

    public string Name { get; set; } = null!;

    public int CourseId { get; set; }

    public DateTime? StartDate { get; set; }

    public DateTime? EndDate { get; set; }

    public DateTime? StartEnrollmentDate { get; set; }

    public long? Fee { get; set; }

    public int? TotalSlot { get; set; }

    public sbyte IsActive { get; set; }

    public int? TotalRegistered { get; set; }

    public int? TotalReserveIn { get; set; }

    public int? TotalReserveOut { get; set; }

    public string? ZaloLink { get; set; }

    public string Status { get; set; } = null!;

    public DateTime? UpdatedAt { get; set; }

    public string? StudyTime { get; set; }

    public string? Schedule { get; set; }

    public string? Address { get; set; }

    public string? ZaloLinkFreeChat { get; set; }

    public string? GoogleMeetLink { get; set; }
}
