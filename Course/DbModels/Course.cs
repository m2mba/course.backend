﻿using System;
using System.Collections.Generic;

namespace Course.DbModels;

public partial class Course
{
    public int Id { get; set; }

    public string? Code { get; set; }

    public string Name { get; set; } = null!;

    public string Method { get; set; } = null!;

    public long Fee { get; set; }

    public string? SuitableObject { get; set; }

    public string? Content { get; set; }

    public string? Lecturer { get; set; }

    public string Status { get; set; } = null!;

    public int TimeInHour { get; set; }

    public int? TimeInLesson { get; set; }

    public int TotalRegisteredStudent { get; set; }

    public string? ImageUrl { get; set; }

    public sbyte IsActive { get; set; }
}
