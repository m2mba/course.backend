﻿using System;
using System.Collections.Generic;

namespace Course.DbModels;

public partial class NotificationEmail
{
    public int Id { get; set; }

    public string Email { get; set; } = null!;

    public int CourseId { get; set; }

    public string? Status { get; set; }

    public int? Times { get; set; }

    public DateTime CreatedAt { get; set; }
}
