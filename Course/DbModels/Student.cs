﻿using System;
using System.Collections.Generic;

namespace Course.DbModels;

public partial class Student
{
    public int Id { get; set; }

    public int RegisterId { get; set; }

    public int CourseId { get; set; }

    public int? ClassroomId { get; set; }

    public int UserId { get; set; }

    public DateTime RegisteredAt { get; set; }

    public string Status { get; set; } = null!;

    public int OrgClassroomId { get; set; }

    public string? TestLink { get; set; }

    public int? PassLevelId { get; set; }

    public string? Detail { get; set; }

    public int? CertificateFileId { get; set; }

    public string? CertificateFileHash { get; set; }

    public sbyte? IsPublic { get; set; }

    public int? EvaluatedBy { get; set; }

    public DateTime? EvaluatedAt { get; set; }

    public DateTime? CreatedAt { get; set; }

    public int? ZaloAdded { get; set; }
}
