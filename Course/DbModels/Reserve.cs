﻿using System;
using System.Collections.Generic;

namespace Course.DbModels;

public partial class Reserve
{
    public int Id { get; set; }

    public int RegisterId { get; set; }

    public int CourseId { get; set; }

    public int UserId { get; set; }

    public int? FromClassroomId { get; set; }

    public int? ToClassroomId { get; set; }

    public DateTime CreatedAt { get; set; }

    public string Status { get; set; } = null!;
}
