﻿using System;
using System.Collections.Generic;

namespace Course.DbModels;

public partial class CatJob
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public sbyte IsActive { get; set; }
}
