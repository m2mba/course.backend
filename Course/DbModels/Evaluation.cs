﻿namespace Course.DbModels;

public class Evaluation
{
    public int Id { get; set; }

    public int StudentId { get; set; }

    public int UserId { get; set; }

    public string? TestLink { get; set; }

    public string PassLevel { get; set; } = null!;

    public string? Detail { get; set; }

    public string? CertificateLink { get; set; }

    public sbyte IsPublic { get; set; }

    public DateTime CreatedAt { get; set; }

    public DateTime UpdatedAt { get; set; }

    public int EvaluatedBy { get; set; }
}
