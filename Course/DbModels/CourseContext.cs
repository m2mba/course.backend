﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Course.DbModels;

public partial class CourseContext : DbContext
{
    public CourseContext()
    {
    }

    public CourseContext(DbContextOptions<CourseContext> options)
        : base(options)
    {
    }

    public virtual DbSet<BankStatement> BankStatements { get; set; }

    public virtual DbSet<CatBank> CatBanks { get; set; }

    public virtual DbSet<CatDoc> CatDocs { get; set; }

    public virtual DbSet<CatEvaluationLevel> CatEvaluationLevels { get; set; }

    public virtual DbSet<CatJob> CatJobs { get; set; }

    public virtual DbSet<Classroom> Classrooms { get; set; }

    public virtual DbSet<Course> Courses { get; set; }

    public virtual DbSet<CourseClassroomDoc> CourseClassroomDocs { get; set; }

    public virtual DbSet<EmailLog> EmailLogs { get; set; }

    public virtual DbSet<File> Files { get; set; }

    public virtual DbSet<JobConfig> JobConfigs { get; set; }

    public virtual DbSet<NotificationEmail> NotificationEmails { get; set; }

    public virtual DbSet<Promotion> Promotions { get; set; }

    public virtual DbSet<PromotionUser> PromotionUsers { get; set; }

    public virtual DbSet<Register> Registers { get; set; }

    public virtual DbSet<Reserve> Reserves { get; set; }

    public virtual DbSet<ShareDoc> ShareDocs { get; set; }

    public virtual DbSet<Student> Students { get; set; }

    public virtual DbSet<Survey> Surveys { get; set; }

    public virtual DbSet<User> Users { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        => optionsBuilder.UseMySQL("Name=ConnectionStrings:Course");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<BankStatement>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("bank_statement");

            entity.HasIndex(e => e.EntryNumber, "bank_statement_entry_number_index");

            entity.HasIndex(e => e.Id, "id").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.CreatedAt)
                .HasColumnType("datetime")
                .HasColumnName("created_at");
            entity.Property(e => e.CreatedBy).HasColumnName("created_by");
            entity.Property(e => e.EntryContent)
                .HasColumnType("text")
                .HasColumnName("entry_content");
            entity.Property(e => e.EntryNumber).HasColumnName("entry_number");
            entity.Property(e => e.FromAccountNumber)
                .HasColumnType("text")
                .HasColumnName("from_account_number");
            entity.Property(e => e.ImportFileId).HasColumnName("import_file_id");
            entity.Property(e => e.Note)
                .HasColumnType("text")
                .HasColumnName("note");
            entity.Property(e => e.PartnerBank)
                .HasMaxLength(255)
                .HasColumnName("partner_bank");
            entity.Property(e => e.RegisterId).HasColumnName("register_id");
            entity.Property(e => e.ResultFileId).HasColumnName("result_file_id");
            entity.Property(e => e.Status)
                .HasMaxLength(255)
                .HasColumnName("status");
            entity.Property(e => e.TransactionAmount).HasColumnName("transaction_amount");
            entity.Property(e => e.TransactionDate)
                .HasColumnType("datetime")
                .HasColumnName("transaction_date");
        });

        modelBuilder.Entity<CatBank>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("cat_bank");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.AccountHolder)
                .HasColumnType("text")
                .HasColumnName("account_holder");
            entity.Property(e => e.AccountNumber)
                .HasMaxLength(255)
                .HasColumnName("account_number");
            entity.Property(e => e.BankBranchName)
                .HasColumnType("text")
                .HasColumnName("bank_branch_name");
            entity.Property(e => e.BankCode)
                .HasMaxLength(255)
                .HasColumnName("bank_code");
            entity.Property(e => e.BankName)
                .HasColumnType("text")
                .HasColumnName("bank_name");
        });

        modelBuilder.Entity<CatDoc>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("cat_docs");

            entity.HasIndex(e => e.HashUrl, "hash_url").IsUnique();

            entity.HasIndex(e => e.Id, "id1").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.DocType)
                .HasMaxLength(255)
                .HasColumnName("doc_type");
            entity.Property(e => e.HashUrl).HasColumnName("hash_url");
            entity.Property(e => e.IsActive)
                .HasDefaultValueSql("'1'")
                .HasColumnName("is_active");
            entity.Property(e => e.Method)
                .HasMaxLength(255)
                .HasColumnName("method");
            entity.Property(e => e.Name)
                .HasColumnType("text")
                .HasColumnName("name");
            entity.Property(e => e.Url)
                .HasColumnType("text")
                .HasColumnName("url");
        });

        modelBuilder.Entity<CatEvaluationLevel>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("cat_evaluation_levels");

            entity.HasIndex(e => e.Id, "id").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.IsActive)
                .HasDefaultValueSql("'1'")
                .HasColumnName("is_active");
            entity.Property(e => e.Name)
                .HasColumnType("text")
                .HasColumnName("name");
        });

        modelBuilder.Entity<CatJob>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("cat_jobs");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.IsActive).HasColumnName("is_active");
            entity.Property(e => e.Name)
                .HasColumnType("text")
                .HasColumnName("name");
        });

        modelBuilder.Entity<Classroom>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("classrooms");

            entity.HasIndex(e => e.CourseId, "classrooms_course_id_fk");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Address)
                .HasColumnType("text")
                .HasColumnName("address");
            entity.Property(e => e.Code)
                .HasMaxLength(255)
                .HasColumnName("code");
            entity.Property(e => e.CourseId).HasColumnName("course_id");
            entity.Property(e => e.EndDate)
                .HasColumnType("datetime")
                .HasColumnName("end_date");
            entity.Property(e => e.Fee).HasColumnName("fee");
            entity.Property(e => e.GoogleMeetLink)
                .HasColumnType("text")
                .HasColumnName("google_meet_link");
            entity.Property(e => e.IsActive).HasColumnName("is_active");
            entity.Property(e => e.Name)
                .HasColumnType("text")
                .HasColumnName("name");
            entity.Property(e => e.Schedule)
                .HasColumnType("text")
                .HasColumnName("schedule");
            entity.Property(e => e.StartDate)
                .HasColumnType("datetime")
                .HasColumnName("start_date");
            entity.Property(e => e.StartEnrollmentDate)
                .HasColumnType("datetime")
                .HasColumnName("start_enrollment_date");
            entity.Property(e => e.Status)
                .HasMaxLength(255)
                .HasColumnName("status");
            entity.Property(e => e.StudyTime)
                .HasColumnType("text")
                .HasColumnName("study_time");
            entity.Property(e => e.TotalRegistered).HasColumnName("total_registered");
            entity.Property(e => e.TotalReserveIn).HasColumnName("total_reserve_in");
            entity.Property(e => e.TotalReserveOut).HasColumnName("total_reserve_out");
            entity.Property(e => e.TotalSlot).HasColumnName("total_slot");
            entity.Property(e => e.UpdatedAt)
                .HasColumnType("datetime")
                .HasColumnName("updated_at");
            entity.Property(e => e.ZaloLink)
                .HasColumnType("text")
                .HasColumnName("zalo_link");
            entity.Property(e => e.ZaloLinkFreeChat)
                .HasColumnType("text")
                .HasColumnName("zalo_link_free_chat");
        });

        modelBuilder.Entity<Course>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("courses");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Code)
                .HasMaxLength(255)
                .HasColumnName("code");
            entity.Property(e => e.Content)
                .HasColumnType("text")
                .HasColumnName("content");
            entity.Property(e => e.Fee).HasColumnName("fee");
            entity.Property(e => e.ImageUrl)
                .HasColumnType("text")
                .HasColumnName("image_url");
            entity.Property(e => e.IsActive).HasColumnName("is_active");
            entity.Property(e => e.Lecturer)
                .HasColumnType("text")
                .HasColumnName("lecturer");
            entity.Property(e => e.Method)
                .HasMaxLength(255)
                .HasColumnName("method");
            entity.Property(e => e.Name)
                .HasColumnType("text")
                .HasColumnName("name");
            entity.Property(e => e.Status)
                .HasMaxLength(255)
                .HasColumnName("status");
            entity.Property(e => e.SuitableObject)
                .HasColumnType("text")
                .HasColumnName("suitable_object");
            entity.Property(e => e.TimeInHour).HasColumnName("time_in_hour");
            entity.Property(e => e.TimeInLesson).HasColumnName("time_in_lesson");
            entity.Property(e => e.TotalRegisteredStudent).HasColumnName("total_registered_student");
        });

        modelBuilder.Entity<CourseClassroomDoc>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("course_classroom_docs");

            entity.HasIndex(e => e.Id, "id11").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.ClassroomId).HasColumnName("classroom_id");
            entity.Property(e => e.CourseId).HasColumnName("course_id");
            entity.Property(e => e.CreatedAt)
                .HasColumnType("datetime")
                .HasColumnName("created_at");
            entity.Property(e => e.DocId).HasColumnName("doc_id");
        });

        modelBuilder.Entity<EmailLog>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("email_logs");

            entity.HasIndex(e => e.ClassroomId, "email_logs_classroom_id_index");

            entity.HasIndex(e => e.CourseId, "email_logs_courses_id_fk");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.ClassroomId).HasColumnName("classroom_id");
            entity.Property(e => e.Content)
                .HasColumnType("text")
                .HasColumnName("content");
            entity.Property(e => e.CourseId).HasColumnName("course_id");
            entity.Property(e => e.CreatedAt)
                .HasColumnType("datetime")
                .HasColumnName("created_at");
            entity.Property(e => e.Email)
                .HasMaxLength(255)
                .HasColumnName("email");
        });

        modelBuilder.Entity<File>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("files");

            entity.HasIndex(e => e.HashId, "hash_id").IsUnique();

            entity.HasIndex(e => e.Id, "id2").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.CreatedAt)
                .HasColumnType("datetime")
                .HasColumnName("created_at");
            entity.Property(e => e.Data).HasColumnName("data");
            entity.Property(e => e.FileName)
                .HasColumnType("text")
                .HasColumnName("file_name");
            entity.Property(e => e.FileSize).HasColumnName("file_size");
            entity.Property(e => e.FileType)
                .HasMaxLength(255)
                .HasColumnName("file_type");
            entity.Property(e => e.HashId).HasColumnName("hash_id");
        });

        modelBuilder.Entity<JobConfig>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("job_configs");

            entity.HasIndex(e => e.Id, "id3").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.AllowCancelRegister)
                .HasDefaultValueSql("'1'")
                .HasColumnName("allow_cancel_register");
            entity.Property(e => e.AllowConfirmByTransaction)
                .HasDefaultValueSql("'1'")
                .HasColumnName("allow_confirm_by_transaction");
            entity.Property(e => e.ReserveInDay)
                .HasDefaultValueSql("'30'")
                .HasColumnName("reserve_in_day");
            entity.Property(e => e.WaitPaymentHour)
                .HasDefaultValueSql("'24'")
                .HasColumnName("wait_payment_hour");
        });

        modelBuilder.Entity<NotificationEmail>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("notification_emails");

            entity.HasIndex(e => e.CourseId, "emails_courses_id_fk");

            entity.HasIndex(e => e.Email, "emails_email_index");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.CourseId).HasColumnName("course_id");
            entity.Property(e => e.CreatedAt)
                .HasColumnType("datetime")
                .HasColumnName("created_at");
            entity.Property(e => e.Email).HasColumnName("email");
            entity.Property(e => e.Status)
                .HasMaxLength(255)
                .HasColumnName("status");
            entity.Property(e => e.Times).HasColumnName("times");
        });

        modelBuilder.Entity<Promotion>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("promotions");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Amount).HasColumnName("amount");
            entity.Property(e => e.ApplyType)
                .HasMaxLength(255)
                .HasComment("- ALL: Tat ca\n- OLD_STUDENT: Hoc vien cu\n- SURVEY: Hoc vien tham gia survey\n- SHARE: Chia se kien thuc")
                .HasColumnName("apply_type");
            entity.Property(e => e.CourseId).HasColumnName("course_id");
            entity.Property(e => e.FromDate)
                .HasColumnType("datetime")
                .HasColumnName("from_date");
            entity.Property(e => e.IsActive)
                .HasDefaultValueSql("'1'")
                .HasColumnName("is_active");
            entity.Property(e => e.Percent)
                .HasPrecision(10)
                .HasColumnName("percent");
            entity.Property(e => e.PeriodDay).HasColumnName("period_day");
            entity.Property(e => e.SurveyTimes).HasColumnName("survey_times");
            entity.Property(e => e.Times).HasColumnName("times");
            entity.Property(e => e.ToDate)
                .HasColumnType("datetime")
                .HasColumnName("to_date");
        });

        modelBuilder.Entity<PromotionUser>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("promotion_users");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Amount).HasColumnName("amount");
            entity.Property(e => e.ApplyType)
                .HasMaxLength(255)
                .HasComment("- ALL: Tat ca\n- OLD_STUDENT: Hoc vien cu\n- SURVEY: Hoc vien tham gia survey\n- SHARE: Chia se kien thuc")
                .HasColumnName("apply_type");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");
            entity.Property(e => e.ExpireDate)
                .HasColumnType("datetime")
                .HasColumnName("expire_date");
            entity.Property(e => e.Percent)
                .HasPrecision(10)
                .HasColumnName("percent");
            entity.Property(e => e.PeriodDay).HasColumnName("period_day");
            entity.Property(e => e.PromotionId).HasColumnName("promotion_id");
            entity.Property(e => e.Times).HasColumnName("times");
            entity.Property(e => e.UsedTimes).HasColumnName("used_times");
            entity.Property(e => e.UserId).HasColumnName("user_id");
        });

        modelBuilder.Entity<Register>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("registers");

            entity.HasIndex(e => e.ClassroomId, "registers_classroom_id_index");

            entity.HasIndex(e => e.Code, "registers_code_index");

            entity.HasIndex(e => e.CourseId, "registers_course_id_index");

            entity.HasIndex(e => e.EntryNumber, "registers_entry_number_index");

            entity.HasIndex(e => e.RegisterAt, "registers_register_at_index");

            entity.HasIndex(e => e.UserId, "registers_user_id_index");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.AccountHolder)
                .HasColumnType("text")
                .HasColumnName("account_holder");
            entity.Property(e => e.AccountNumber)
                .HasMaxLength(255)
                .HasColumnName("account_number");
            entity.Property(e => e.ApproveBy).HasColumnName("approve_by");
            entity.Property(e => e.ApproveNote)
                .HasColumnType("text")
                .HasColumnName("approve_note");
            entity.Property(e => e.ApprovedAt)
                .HasColumnType("datetime")
                .HasColumnName("approved_at");
            entity.Property(e => e.BankBranchName)
                .HasColumnType("text")
                .HasColumnName("bank_branch_name");
            entity.Property(e => e.BankCode)
                .HasMaxLength(255)
                .HasColumnName("bank_code");
            entity.Property(e => e.BankName)
                .HasColumnType("text")
                .HasColumnName("bank_name");
            entity.Property(e => e.ClassroomId).HasColumnName("classroom_id");
            entity.Property(e => e.Code).HasColumnName("code");
            entity.Property(e => e.CourseId).HasColumnName("course_id");
            entity.Property(e => e.DiscountAmount).HasColumnName("discount_amount");
            entity.Property(e => e.EntryNumber).HasColumnName("entry_number");
            entity.Property(e => e.ExpirePaymentDate)
                .HasColumnType("datetime")
                .HasColumnName("expire_payment_date");
            entity.Property(e => e.Fee).HasColumnName("fee");
            entity.Property(e => e.OrgFee).HasColumnName("org_fee");
            entity.Property(e => e.PromotionId).HasColumnName("promotion_id");
            entity.Property(e => e.PromotionUserId).HasColumnName("promotion_user_id");
            entity.Property(e => e.RegisterAt)
                .HasColumnType("datetime")
                .HasColumnName("register_at");
            entity.Property(e => e.SharedDoc).HasColumnName("shared_doc");
            entity.Property(e => e.SharedVideo).HasColumnName("shared_video");
            entity.Property(e => e.Status)
                .HasMaxLength(255)
                .HasColumnName("status");
            entity.Property(e => e.UserId).HasColumnName("user_id");
            entity.Property(e => e.ZaloAdded).HasColumnName("zalo_added");
        });

        modelBuilder.Entity<Reserve>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("reserve");

            entity.HasIndex(e => e.Id, "id31").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.CourseId).HasColumnName("course_id");
            entity.Property(e => e.CreatedAt)
                .HasColumnType("datetime")
                .HasColumnName("created_at");
            entity.Property(e => e.FromClassroomId).HasColumnName("from_classroom_id");
            entity.Property(e => e.RegisterId).HasColumnName("register_id");
            entity.Property(e => e.Status)
                .HasMaxLength(255)
                .HasColumnName("status");
            entity.Property(e => e.ToClassroomId).HasColumnName("to_classroom_id");
            entity.Property(e => e.UserId).HasColumnName("user_id");
        });

        modelBuilder.Entity<ShareDoc>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("share_docs");

            entity.HasIndex(e => e.Id, "id4").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.ClassroomId).HasColumnName("classroom_id");
            entity.Property(e => e.CourseId).HasColumnName("course_id");
            entity.Property(e => e.CreatedAt)
                .HasColumnType("datetime")
                .HasColumnName("created_at");
            entity.Property(e => e.DocId).HasColumnName("doc_id");
            entity.Property(e => e.Emails)
                .HasColumnType("text")
                .HasColumnName("emails");
            entity.Property(e => e.Group)
                .HasMaxLength(255)
                .HasColumnName("group");
            entity.Property(e => e.ShareUserId).HasColumnName("share_user_id");
        });

        modelBuilder.Entity<Student>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("students");

            entity.HasIndex(e => e.ClassroomId, "students_classroom_id_index");

            entity.HasIndex(e => e.CourseId, "students_course_id_index");

            entity.HasIndex(e => e.RegisteredAt, "students_registered_at_index");

            entity.HasIndex(e => e.UserId, "students_user_id_index");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.CertificateFileHash)
                .HasMaxLength(255)
                .HasColumnName("certificate_file_hash");
            entity.Property(e => e.CertificateFileId).HasColumnName("certificate_file_id");
            entity.Property(e => e.ClassroomId).HasColumnName("classroom_id");
            entity.Property(e => e.CourseId).HasColumnName("course_id");
            entity.Property(e => e.CreatedAt)
                .HasColumnType("datetime")
                .HasColumnName("created_at");
            entity.Property(e => e.Detail)
                .HasColumnType("text")
                .HasColumnName("detail");
            entity.Property(e => e.EvaluatedAt)
                .HasColumnType("datetime")
                .HasColumnName("evaluated_at");
            entity.Property(e => e.EvaluatedBy).HasColumnName("evaluated_by");
            entity.Property(e => e.IsPublic).HasColumnName("is_public");
            entity.Property(e => e.OrgClassroomId).HasColumnName("org_classroom_id");
            entity.Property(e => e.PassLevelId).HasColumnName("pass_level_id");
            entity.Property(e => e.RegisterId).HasColumnName("register_id");
            entity.Property(e => e.RegisteredAt)
                .HasColumnType("datetime")
                .HasColumnName("registered_at");
            entity.Property(e => e.Status)
                .HasMaxLength(255)
                .HasColumnName("status");
            entity.Property(e => e.TestLink)
                .HasColumnType("text")
                .HasColumnName("test_link");
            entity.Property(e => e.UserId).HasColumnName("user_id");
            entity.Property(e => e.ZaloAdded).HasColumnName("zalo_added");
        });

        modelBuilder.Entity<Survey>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("surveys");

            entity.HasIndex(e => e.StudentId, "ssurveys_student_id__index");

            entity.HasIndex(e => e.ClassroomId, "surveys_classroom_id_index");

            entity.HasIndex(e => new { e.CourseId, e.Id }, "surveys_course_id_id_index");

            entity.HasIndex(e => e.CreatedAt, "surveys_created_at_index");

            entity.HasIndex(e => e.Id, "surveys_id_uindex").IsUnique();

            entity.HasIndex(e => e.StudentId, "surveys_student_id_index");

            entity.HasIndex(e => e.UpdatedAt, "surveys_updated_at_index");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.ApprovedAt)
                .HasColumnType("datetime")
                .HasColumnName("approved_at");
            entity.Property(e => e.ApprovedBy).HasColumnName("approved_by");
            entity.Property(e => e.ApprovedNote)
                .HasColumnType("text")
                .HasColumnName("approved_note");
            entity.Property(e => e.ClassroomId).HasColumnName("classroom_id");
            entity.Property(e => e.CourseId).HasColumnName("course_id");
            entity.Property(e => e.CreatedAt)
                .HasColumnType("datetime")
                .HasColumnName("created_at");
            entity.Property(e => e.Note)
                .HasColumnType("text")
                .HasColumnName("note");
            entity.Property(e => e.Rate).HasColumnName("rate");
            entity.Property(e => e.Status)
                .HasMaxLength(255)
                .HasColumnName("status");
            entity.Property(e => e.StudentId).HasColumnName("student_id");
            entity.Property(e => e.UpdatedAt)
                .HasColumnType("datetime")
                .HasColumnName("updated_at");
            entity.Property(e => e.UpdatedBy).HasColumnName("updated_by");
            entity.Property(e => e.UserId).HasColumnName("user_id");
        });

        modelBuilder.Entity<User>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("users");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.CatJobId).HasColumnName("cat_job_id");
            entity.Property(e => e.CreatedAt)
                .HasColumnType("timestamp")
                .HasColumnName("created_at");
            entity.Property(e => e.Email)
                .HasMaxLength(255)
                .HasColumnName("email");
            entity.Property(e => e.FirstName)
                .HasMaxLength(255)
                .HasColumnName("first_name");
            entity.Property(e => e.FullName)
                .HasColumnType("text")
                .HasColumnName("full_name");
            entity.Property(e => e.IsActive).HasColumnName("is_active");
            entity.Property(e => e.Password)
                .HasMaxLength(255)
                .HasColumnName("password");
            entity.Property(e => e.Phone)
                .HasMaxLength(255)
                .HasColumnName("phone");
            entity.Property(e => e.RefreshToken)
                .HasMaxLength(255)
                .HasColumnName("refresh_token");
            entity.Property(e => e.Roles)
                .HasColumnType("text")
                .HasColumnName("roles");
            entity.Property(e => e.Status)
                .HasMaxLength(255)
                .HasColumnName("status");
            entity.Property(e => e.UpdatedAt)
                .HasColumnType("timestamp")
                .HasColumnName("updated_at");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
