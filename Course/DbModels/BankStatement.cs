﻿using System;
using System.Collections.Generic;

namespace Course.DbModels;

public partial class BankStatement
{
    public int Id { get; set; }

    public string EntryNumber { get; set; } = null!;

    public string? EntryContent { get; set; }

    public long TransactionAmount { get; set; }

    public string PartnerBank { get; set; } = null!;

    public string? FromAccountNumber { get; set; }

    public DateTime? TransactionDate { get; set; }

    public int ImportFileId { get; set; }

    public int? ResultFileId { get; set; }

    public string Status { get; set; } = null!;

    public DateTime? CreatedAt { get; set; }

    public string? Note { get; set; }

    public int CreatedBy { get; set; }

    public int? RegisterId { get; set; }
}
