﻿using System;
using System.Collections.Generic;

namespace Course.DbModels;

public partial class File
{
    public int Id { get; set; }

    public string FileName { get; set; } = null!;

    public string FileType { get; set; } = null!;

    public long FileSize { get; set; }

    public string HashId { get; set; } = null!;

    public string Data { get; set; } = null!;

    public DateTime CreatedAt { get; set; }
}
