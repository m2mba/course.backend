﻿using System;
using System.Collections.Generic;

namespace Course.DbModels;

public partial class CourseClassroomDoc
{
    public int Id { get; set; }

    public int CourseId { get; set; }

    public int? ClassroomId { get; set; }

    public int DocId { get; set; }

    public DateTime CreatedAt { get; set; }
}
