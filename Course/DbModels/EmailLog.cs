﻿using System;
using System.Collections.Generic;

namespace Course.DbModels;

public partial class EmailLog
{
    public int Id { get; set; }

    public string Email { get; set; } = null!;

    public int? CourseId { get; set; }

    public int? ClassroomId { get; set; }

    public string Content { get; set; } = null!;

    public DateTime CreatedAt { get; set; }
}
