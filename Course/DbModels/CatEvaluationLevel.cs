﻿using System;
using System.Collections.Generic;

namespace Course.DbModels;

public partial class CatEvaluationLevel
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public int IsActive { get; set; }
}
