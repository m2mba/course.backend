using Course.Common;
using Course.DbModels;
using Course.Repositories.Models;

namespace Course.Repositories;

public class ClassroomRepository : Repository<Classroom>
{
    public ClassroomRepository(CourseContext context) : base(context)
    {
    }

    public List<Classroom> List(ListClassroomParam param, Option? option = null)
    {
        var query = GetQuery(param);
        if (option?.Orders == null || !option.Orders.Any())
        {
            var now = DateTime.Today;
            query = query.ToList().OrderBy(x => x.TotalSlot > x.TotalRegistered && x.StartEnrollmentDate >= now ? 0 : 1)
                .ThenBy(
                    x => x.TotalSlot > x.TotalRegistered && x.StartEnrollmentDate >= now
                        ? (x.StartEnrollmentDate.Value - now).Days
                        : (now - (x.StartEnrollmentDate ?? now)).Days).AsQueryable();
            option ??= new Option();
            option.KeepQueryOrder = true;
        }

        return Load(query, option);
    }

    public int Count(ListClassroomParam param)
    {
        var query = GetQuery(param);
        return query.Count();
    }

    public DateTime? GetNearestStartEnrollmentDate(int courseId)
    {
        var query = GetQuery(new ListClassroomParam
        {
            CourseId = courseId,
            FromStartDate = DateTime.Today
        });
        return query.OrderBy(x => x.StartEnrollmentDate).Select(x => x.StartEnrollmentDate).FirstOrDefault();
    }
    
    public Dictionary<int, int> CountByCourse(ListClassroomParam param)
    {
        var query = GetQuery(param);
        return query.GroupBy(x => x.CourseId).Select(x => new
        {
            x.Key,
            Total = x.Count()
        }).ToList().ToDictionary(x => x.Key, x => x.Total);
    }

    private IQueryable<Classroom> GetQuery(ListClassroomParam param)
    {
        var query = GetAll();

        if (param.Id != null) query = query.Where(x => x.Id == param.Id);

        if (param.Ids.HasValue()) query = query.Where(x => param.Ids!.Contains(x.Id));

        if (param.CourseId != null) query = query.Where(x => x.CourseId == param.CourseId);

        if (param.CourseIds.HasValue()) query = query.Where(x => param.CourseIds!.Contains(x.CourseId));

        if (param.FromStartDate != null) query = query.Where(x => x.StartDate >= param.FromStartDate);
        if (param.ToStartDate != null) query = query.Where(x => x.StartDate <= param.ToStartDate);

        if (param.FromStartEnrollmentDate != null)
            query = query.Where(x => x.StartEnrollmentDate >= param.FromStartEnrollmentDate);

        if (param.ToStartEnrollmentDate != null)
            query = query.Where(x => x.StartEnrollmentDate <= param.ToStartEnrollmentDate);

        if (!string.IsNullOrWhiteSpace(param.Name))
        {
            var name = param.Name.Trim().ToLower();
            query = query.Where(x => x.Name.ToLower().Contains(name));
        }

        if (param.IsActive != null) query = query.Where(x => x.IsActive == param.IsActive);
        if (param.Status != null) query = query.Where(x => x.Status == param.Status);

        if (param.Available != null)
            query = param.Available.Value
                ? query.Where(x => x.TotalSlot > x.TotalRegistered)
                : query.Where(x => x.TotalSlot <= x.TotalRegistered);

        return query;
    }
}