using Course.Common;
using Course.DbModels;
using Course.Repositories.Models;

namespace Course.Repositories;

public class PromotionUserRepository : Repository<PromotionUser>
{
    public PromotionUserRepository(CourseContext context) : base(context)
    {
    }


    public List<PromotionUser> List(ListPromotionUserParam param, Option? option = null)
    {
        var query = GetQuery(param);
        return Load(query, option);
    }

    public int Count(ListPromotionUserParam param)
    {
        var query = GetQuery(param);
        return query.Count();
    }

    private IQueryable<PromotionUser> GetQuery(ListPromotionUserParam param)
    {
        var query = GetAll();

        if (param.Id != null) query = query.Where(x => x.Id == param.Id);

        if (param.UserId != null) query = query.Where(x => x.UserId == param.UserId);

        if (param.PromotionId != null) query = query.Where(x => x.PromotionId == param.PromotionId);

        if (param.Ids.HasValue()) query = query.Where(x => param.Ids!.Contains(x.Id));

        if (param.ApplyType != null) query = query.Where(x => x.ApplyType == param.ApplyType);

        if (param.Available != null && param.Available.Value)
        {
            var now = DateTime.Now;
            query = query.Where(x => x.ExpireDate == null || x.ExpireDate >= now);
            query = query.Where(x => x.UsedTimes == null || x.UsedTimes < x.Times);
        }

        return query;
    }
}