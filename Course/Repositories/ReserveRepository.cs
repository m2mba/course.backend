using Course.DbModels;
using Course.Repositories.Models;

namespace Course.Repositories;

public class ReserveRepository : Repository<Reserve>
{
    public ReserveRepository(CourseContext context) : base(context)
    {
    }

    public List<Reserve> List(ListReserveParam param, Option? option = null)
    {
        var query = GetQuery(param);
        return Load(query, option);
    }

    public int Count(ListReserveParam param)
    {
        var query = GetQuery(param);
        return query.Count();
    }

    private IQueryable<Reserve> GetQuery(ListReserveParam param)
    {
        var query = GetAll();

        if (param.UserId != null) query = query.Where(x => x.UserId == param.UserId);
        if (param.RegisterId != null) query = query.Where(x => x.RegisterId == param.RegisterId);

        return query;
    }
}