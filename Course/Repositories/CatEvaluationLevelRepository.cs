using Course.Common;
using Course.DbModels;
using Course.Repositories.Models;

namespace Course.Repositories;

public class CatEvaluationLevelRepository : Repository<CatEvaluationLevel>
{
    public CatEvaluationLevelRepository(CourseContext context) : base(context)
    {
    }

    public List<CatEvaluationLevel> List(BaseParam param, Option? option = null)
    {
        var query = GetAll().Where(x => x.IsActive == 1);
        if (param.Ids.HasValue())
        {
            query = query.Where(x => param.Ids!.Contains(x.Id));
        }
        return Load(query, option);
    }
}