using Course.Common;
using Course.DbModels;
using Course.Repositories.Models;

namespace Course.Repositories;

public class RegisterRepository : Repository<Register>
{
    public RegisterRepository(CourseContext context) : base(context)
    {
    }


    public List<Register> List(ListRegisterParam param, Option? option = null)
    {
        var query = GetQuery(param);
        return Load(query, option);
    }

    public int Count(ListRegisterParam param)
    {
        var query = GetQuery(param);
        return query.Count();
    }

    public IQueryable<Register> GetQuery(ListRegisterParam param)
    {
        var query = GetAll();

        if (param.CourseId != null) query = query.Where(x => x.CourseId == param.CourseId);

        if (param.UserId != null) query = query.Where(x => x.UserId == param.UserId);

        if (param.ClassroomId != null) query = query.Where(x => x.ClassroomId == param.ClassroomId);

        if (param.PromotionIds.HasValue()) query = query.Where(x => param.PromotionIds!.Contains(x.PromotionId));

        if (param.Statuses.HasValue()) query = query.Where(x => param.Statuses!.Contains(x.Status));

        if (param.ExcStatuses.HasValue()) query = query.Where(x => !param.ExcStatuses!.Contains(x.Status));

        if (!string.IsNullOrWhiteSpace(param.Status))
        {
            var status = param.Status.Trim().ToUpper();
            query = query.Where(x => x.Status == status);
        }

        if (!string.IsNullOrWhiteSpace(param.Code))
        {
            var code = param.Code.Trim().ToLower();
            query = query.Where(x => x.Code.ToLower().Contains(code));
        }

        if (!string.IsNullOrWhiteSpace(param.UserFullName))
        {
            var fullName = param.UserFullName.Trim().ToLower();
            query = query.Where(x =>
                DbContext.Users.Any(u => u.Id == x.UserId && u.FullName.ToLower().Contains(fullName)));
        }

        if (!string.IsNullOrWhiteSpace(param.UserPhone))
        {
            var phone = param.UserPhone.Trim().ToLower();
            query = query.Where(x =>
                DbContext.Users.Any(u => u.Id == x.UserId && u.Phone.ToLower().Contains(phone)));
        }

        if (!string.IsNullOrWhiteSpace(param.UserEmail))
        {
            var email = param.UserEmail.Trim().ToLower();
            query = query.Where(x =>
                DbContext.Users.Any(u => u.Id == x.UserId && u.Email.ToLower().Contains(email)));
        }

        if (!string.IsNullOrWhiteSpace(param.EntryNumber))
        {
            var num = param.EntryNumber.Trim();
            query = query.Where(x => x.EntryNumber == num);
        }

        if (param.SharedDoc != null) query = query.Where(x => x.SharedDoc == param.SharedDoc);

        if (param.SharedVideo != null) query = query.Where(x => x.SharedVideo == param.SharedVideo);

        if (param.ZaloAdded != null)
        {
            query = param.ZaloAdded == 1
                ? query.Where(x => x.ZaloAdded == param.ZaloAdded)
                : query.Where(x => x.ZaloAdded == null || x.ZaloAdded == 0);
        }

        if (param.FromRegisterAt != null) query = query.Where(x => x.RegisterAt >= param.FromRegisterAt);
        if (param.ToRegisterAt != null) query = query.Where(x => x.RegisterAt <= param.ToRegisterAt);

        return query;
    }
}