namespace Course.Repositories.Models;

public class ListBankStatementParam
{
    public string? EntryNumber { get; set; }
    public int? ImportFileId { get; set; }
    public int? ResultFileId { get; set; }
    public string? Status { get; set; }
}