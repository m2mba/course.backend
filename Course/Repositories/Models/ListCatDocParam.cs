namespace Course.Repositories.Models;

public class ListCatDocParam: IdParam
{
    public sbyte? IsActive { get; set; } = 1;
    public string? Url { get; set; }
}