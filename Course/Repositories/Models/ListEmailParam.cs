namespace Course.Repositories.Models;

public class ListEmailParam
{
    public int? CourseId { get; set; }
    public string? Email { get; set; }
}