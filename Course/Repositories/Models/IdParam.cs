namespace Course.Repositories.Models;

public class IdParam
{
    public int? Id { get; set; }
    public List<int>? Ids { get; set; }
}