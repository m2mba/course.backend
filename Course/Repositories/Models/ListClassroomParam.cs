namespace Course.Repositories.Models;

public class ListClassroomParam: IdParam
{
    public int? CourseId { get; set; }
    public List<int>? CourseIds { get; set; }
    public string? Name { get; set; }
    public DateTime? FromStartEnrollmentDate { get; set; }
    public DateTime? ToStartEnrollmentDate { get; set; }
    public DateTime? FromStartDate { get; set; }
    public DateTime? ToStartDate { get; set; }
    public sbyte? IsActive { get; set; } = 1;
    public bool? Available { get; set; }
    public string? Status { get; set; }
}