namespace Course.Repositories.Models;

public class Option
{
    public bool Paging { get; set; }
    public bool NoTracking { get; set; } = true;
    public int Page { get; set; }
    public int Size { get; set; }
    public List<Order>? Orders { get; set; }
    public bool KeepQueryOrder { get; set; } = false;
}

public class Order
{
    public bool Descending { get; set; }

    /// <summary>
    ///     It converts to lower before comparing
    /// </summary>
    public string? FieldName { get; set; }
}

public class BaseParam
{
    public int? Id { get; set; }
    public int? TenantId { get; set; }
    public List<int>? Ids { get; set; }
    public bool? Active { get; set; } = true;
}