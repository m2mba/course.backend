namespace Course.Repositories.Models;

public class ListEvaluationParam: IdParam
{
    public int? UserId { get; set; }
    public int? CourseId { get; set; }
    public int? ClassroomId { get; set; }
    public string? PassLevel { get; set; }
}