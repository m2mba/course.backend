namespace Course.Repositories.Models;

public class ListSurveyParam: IdParam
{
    public int? UserId { get; set; }
    public string? UserEmail { get; set; }
    public string? UserPhone { get; set; }
    public int? CourseId { get; set; }
    public int? ClassroomId { get; set; }
    public int? StudentId { get; set; }
    public string? Status { get; set; }
    public List<string>? Statuses { get; set; }
    public List<string>? ExcStatuses { get; set; }
    public DateTime? FromCreatedAt { get; set; }
    public DateTime? ToCreatedAt { get; set; }
}