namespace Course.Repositories.Models;

public class ListCourseParam: IdParam
{
    public sbyte? IsActive { get; set; } = 1;
}