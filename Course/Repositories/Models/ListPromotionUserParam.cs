namespace Course.Repositories.Models;

public class ListPromotionUserParam: IdParam
{
    public int? UserId { get; set; }
    public int? PromotionId { get; set; }
    public string? ApplyType { get; set; }
    public bool? Available { get; set; }
}