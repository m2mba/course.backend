namespace Course.Repositories.Models;

public class ListPromotionParam: IdParam
{
    public sbyte? IsActive { get; set; } = 1;   
    public bool? Available { get; set; }
    public int? UserId { get; set; }
    public int? CourseId { get; set; }
    public DateTime? FromToDate { get; set; }
    public DateTime? ToToDate { get; set; }
    public DateTime? FromFromDate { get; set; }
    public DateTime? ToFromDate { get; set; }
    public List<string>? ApplyTypes { get; set; }
}