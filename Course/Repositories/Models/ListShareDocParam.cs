namespace Course.Repositories.Models;

public class ListShareDocParam: IdParam
{
    public int? CourseId { get; set; }
    public int? ClassroomId { get; set; }
    public List<int>? DocIds { get; set; }
    public string? Email { get; set; } 
    public DateTime? FromCreatedAt { get; set; } 
    public DateTime? ToCreatedAt { get; set; } 
}