namespace Course.Repositories.Models;

public class ListEmailLogParam
{
    public int? ClassroomId { get; set; }
    public List<string>? Emails { get; set; }
}