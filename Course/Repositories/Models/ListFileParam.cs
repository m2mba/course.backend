namespace Course.Repositories.Models;

public class ListFileParam: IdParam
{
    public string? HashId { get; set; }
}