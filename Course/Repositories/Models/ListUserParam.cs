namespace Course.Repositories.Models;

public class ListUserParam: IdParam
{
    public string? FullName { get; set; }
    public string? Phone { get; set; }
    public string? Email { get; set; }
    public List<string>? Emails { get; set; }
    public sbyte? IsActive { get; set; } = 1;
}