namespace Course.Repositories.Models;

public class ListCourseClassroomDocParam: IdParam
{
    public int? CourseId { get; set; }
    public int? ClassroomId { get; set; }
    public int? DocId { get; set; }
    public bool? OnlyCourse { get; set; }
    public string? Method { get; set; }
}