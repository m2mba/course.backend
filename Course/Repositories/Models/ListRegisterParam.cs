namespace Course.Repositories.Models;

public class ListRegisterParam
{
    public int? UserId { get; set; }
    public string? Code { get; set; }
    public string? UserFullName { get; set; }
    public string? UserEmail { get; set; }
    public string? UserPhone { get; set; }
    public int? CourseId { get; set; }
    public int? ClassroomId { get; set; }
    public List<int?>? PromotionIds { get; set; }
    public string? Status { get; set; }
    public string? EntryNumber { get; set; }
    public List<string>? Statuses { get; set; }
    public List<string>? ExcStatuses { get; set; }
    public sbyte? SharedDoc { get; set; }
    public sbyte? SharedVideo { get; set; }
    public int? ZaloAdded { get; set; }
    public DateTime? FromRegisterAt { get; set; }
    public DateTime? ToRegisterAt { get; set; }
}