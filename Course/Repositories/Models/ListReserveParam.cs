namespace Course.Repositories.Models;

public class ListReserveParam
{
    public int? UserId { get; set; }
    public int? RegisterId { get; set; }
}