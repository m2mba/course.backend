namespace Course.Repositories.Models;

public class ListStudentParam: IdParam
{
    public List<int>? RegisterIds { get; set; }
    public int? RegisterId { get; set; }
    public int? UserId { get; set; }
    public int? CourseId { get; set; }
    public int? ClassroomId { get; set; }
    public string? Phone { get; set; }
    public string? Email { get; set; }
    public string? PhoneOrEmailEqual { get; set; }
    public int? PassLevelId { get; set; }
    public string? Status { get; set; }
    public sbyte? IsPublic { get; set; }
    public int? ZaloAdded { get; set; }
}