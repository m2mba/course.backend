using Course.Common;
using Course.DbModels;
using Course.Repositories.Models;
using File = Course.DbModels.File;

namespace Course.Repositories;

public class FileRepository : Repository<File>
{
    public FileRepository(CourseContext context) : base(context)
    {
    }

    public File? GetByHashId(string hashId, bool getData = true)
    {
        var query = GetAll().Where(x => x.HashId == hashId);
        if (getData)
        {
            return query.FirstOrDefault();
        }

        return query.Select(x => new File
        {
            Id = x.Id,
            FileName = x.FileName
        }).FirstOrDefault();
    }

    public List<File> List(ListFileParam param, Option? option = null)
    {
        var query = GetQuery(param);
        return Load(query, option);
    }

    public int Count(ListFileParam param)
    {
        var query = GetQuery(param);
        return query.Count();
    }

    private IQueryable<File> GetQuery(ListFileParam param)
    {
        var query = GetAll();

        if (param.Id != null) query = query.Where(x => x.Id == param.Id);

        if (param.Ids.HasValue()) query = query.Where(x => param.Ids!.Contains(x.Id));

        if (!string.IsNullOrWhiteSpace(param.HashId))
        {
            query = query.Where(x => x.HashId == param.HashId);
        }

        return query;
    }
}