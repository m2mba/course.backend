using Course.DbModels;
using Course.Repositories.Models;

namespace Course.Repositories;

public class CatBankRepository : Repository<CatBank>
{
    public CatBankRepository(CourseContext context) : base(context)
    {
    }

    public List<CatBank> List(Option? option = null)
    {
        var query = GetAll();
        return Load(query, option);
    }
}