using Course.DbModels;
using Course.Repositories.Models;

namespace Course.Repositories;

public class NotificationEmailRepository : Repository<NotificationEmail>
{
    public NotificationEmailRepository(CourseContext context) : base(context)
    {
    }

    public List<NotificationEmail> List(ListEmailParam param, Option? option = null)
    {
        var query = GetQuery(param);
        return Load(query, option);
    }

    public int Count(ListEmailParam param)
    {
        var query = GetQuery(param);
        return query.Count();
    }

    private IQueryable<NotificationEmail> GetQuery(ListEmailParam param)
    {
        var query = GetAll();
        if (param.CourseId != null) query = query.Where(x => x.CourseId == param.CourseId);
        if (!string.IsNullOrWhiteSpace(param.Email))
        {
            var email = param.Email.Trim().ToLower();
            query = query.Where(x => x.Email == email);
        }

        return query;
    }
}