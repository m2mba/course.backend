using Course.Common;
using Course.DbModels;
using Course.Repositories.Models;

namespace Course.Repositories;

public class StudentRepository : Repository<Student>
{
    public StudentRepository(CourseContext context) : base(context)
    {
    }

    public List<Student> List(ListStudentParam param, Option? option = null)
    {
        var query = GetQuery(param);
        return Load(query, option);
    }

    public int Count(ListStudentParam param)
    {
        var query = GetQuery(param);
        return query.Count();
    }

    public IQueryable<Student> GetQuery(ListStudentParam param)
    {
        var query = GetAll();
        if (param.Id != null) query = query.Where(x => x.Id == param.Id);

        if (param.Ids.HasValue()) query = query.Where(x => param.Ids!.Contains(x.Id));
        if (param.CourseId != null) query = query.Where(x => x.CourseId == param.CourseId);

        if (param.RegisterIds.HasValue()) query = query.Where(x => param.RegisterIds!.Contains(x.RegisterId));
        if (param.RegisterId != null) query = query.Where(x => x.RegisterId == param.RegisterId);
        if (param.UserId != null) query = query.Where(x => x.UserId == param.UserId);

        if (param.ClassroomId != null) query = query.Where(x => x.ClassroomId == param.ClassroomId);

        if (param.IsPublic != null) query = query.Where(x => x.IsPublic == param.IsPublic);

        if (!string.IsNullOrWhiteSpace(param.Status)) query = query.Where(x => x.Status == param.Status);

        if (param.PassLevelId != null)
        {
            query = query.Where(x => x.PassLevelId == param.PassLevelId);
        }

        if (param.ZaloAdded != null)
        {
            query = param.ZaloAdded == 1
                ? query.Where(x => x.ZaloAdded == param.ZaloAdded)
                : query.Where(x => x.ZaloAdded == null || x.ZaloAdded == 0);
        }

        if (!string.IsNullOrWhiteSpace(param.Phone))
        {
            query = query.Where(x =>
                DbContext.Users.Any(u => u.Id == x.UserId && u.Phone == param.Phone));
        }

        if (!string.IsNullOrWhiteSpace(param.Email))
        {
            var email = param.Email.Trim().ToLower();
            query = query.Where(x =>
                DbContext.Users.Any(u => u.Id == x.UserId && u.Email.ToLower().Contains(email)));
        }

        if (!string.IsNullOrWhiteSpace(param.PhoneOrEmailEqual))
        {
            var phoneOrEmail = param.PhoneOrEmailEqual.Trim().ToLower();
            query = query.Where(x => DbContext.Users.Any(u => u.Id == x.UserId && (u.Phone.ToLower().Equals(phoneOrEmail) || u.Email.ToLower().Equals(phoneOrEmail))));
        }

        return query;
    }
}