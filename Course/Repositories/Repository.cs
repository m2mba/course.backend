﻿using Course.Common;
using Course.DbModels;
using Course.Repositories.Models;
using Microsoft.EntityFrameworkCore;

namespace Course.Repositories;

public class Repository<T> : IRepository<T> where T : class
{
    protected readonly CourseContext DbContext;

    protected Repository(CourseContext dbContext)
    {
        DbContext = dbContext;
    }

    public T? GetById(object id)
    {
        return DbContext.Set<T>().Find(id);
    }

    public IQueryable<T> GetAll()
    {
        return DbContext.Set<T>().AsQueryable();
    }

    public void Add(T entity)
    {
        DbContext.Set<T>().Add(entity);
    }

    public void Update(T entity)
    {
        DbContext.Entry(entity).State = EntityState.Modified;
    }

    public void Delete(T entity)
    {
        DbContext.Set<T>().Remove(entity);
    }

    protected List<T> Load(IQueryable<T> query, Option? option)
    {
        if (option == null) return BuildQueryOrder(query).ToList();

        if (!option.KeepQueryOrder)
        {
            var orders = option.Orders;
            query = orders != null && orders.HasValue()
                ? orders.Aggregate(query, BuildQueryOrder)
                : OrderByDescendingId(query);
        }

        if (option.Paging && option.Page > 0 && option.Size > 0)
            query = query.Skip((option.Page - 1) * option.Size).Take(option.Size);

        if (option.NoTracking) query = query.AsNoTracking();

        return query.ToList();
    }

    private static IQueryable<T> BuildQueryOrder(IQueryable<T> query, Order? order = null)
    {
        var sortColumn = order?.FieldName;
        if (string.IsNullOrWhiteSpace(sortColumn)) return OrderByDescendingId(query);

        var properties = typeof(T).GetProperties().Select(x => x.Name).ToList();
        var column = sortColumn.Trim().ToLower();
        var sortProperty = properties.FirstOrDefault(x => x.ToLower().Equals(column));
        if (string.IsNullOrWhiteSpace(sortProperty)) return query;

        return order != null && order.Descending
            ? query.OrderByDescending(x => EF.Property<object>(x, sortProperty))
            : query.OrderBy(x => EF.Property<object>(x, sortProperty));
    }

    private static IQueryable<T> OrderByDescendingId(IQueryable<T> query)
    {
        return query.OrderByDescending(x => EF.Property<object>(x, "Id"));
    }
}