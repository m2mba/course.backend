using Course.Common;
using Course.DbModels;
using Course.Repositories.Models;

namespace Course.Repositories;

public class CourseClassroomDocRepository : Repository<CourseClassroomDoc>
{
    public CourseClassroomDocRepository(CourseContext context) : base(context)
    {
    }


    public List<CourseClassroomDoc> List(ListCourseClassroomDocParam param, Option? option = null)
    {
        var query = GetQuery(param);
        return Load(query, option);
    }

    public int Count(ListCourseClassroomDocParam param)
    {
        var query = GetQuery(param);
        return query.Count();
    }

    private IQueryable<CourseClassroomDoc> GetQuery(ListCourseClassroomDocParam param)
    {
        var query = GetAll();

        if (param.Id != null) query = query.Where(x => x.Id == param.Id);

        if (param.Ids.HasValue()) query = query.Where(x => param.Ids!.Contains(x.Id));

        if (param.CourseId != null) query = query.Where(x => x.CourseId == param.CourseId);

        if (param.ClassroomId != null) query = query.Where(x => x.ClassroomId == param.ClassroomId);

        if (param.DocId != null) query = query.Where(x => x.DocId == param.DocId);

        if (!string.IsNullOrWhiteSpace(param.Method))
        {
            query = query.Where(x => DbContext.CatDocs.Any(d => d.Id == x.DocId && d.Method == param.Method));
        }

        if (param.OnlyCourse != null && param.OnlyCourse.Value) query = query.Where(x => x.ClassroomId == null);

        return query;
    }
}