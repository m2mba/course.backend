using Course.Common;
using Course.DbModels;
using Course.Repositories.Models;

namespace Course.Repositories;

public class SurveyRepository : Repository<Survey>
{
    public SurveyRepository(CourseContext context) : base(context)
    {
    }


    public List<Survey> List(ListSurveyParam param, Option? option = null)
    {
        var query = GetQuery(param);
        return Load(query, option);
    }

    public int Count(ListSurveyParam param)
    {
        var query = GetQuery(param);
        return query.Count();
    }

    public IQueryable<Survey> GetQuery(ListSurveyParam param)
    {
        var query = GetAll();

        if (param.CourseId != null) query = query.Where(x => x.CourseId == param.CourseId);

        if (param.UserId != null) query = query.Where(x => x.UserId == param.UserId);

        if (param.ClassroomId != null) query = query.Where(x => x.ClassroomId == param.ClassroomId);

        if (param.StudentId != null) query = query.Where(x => x.StudentId == param.StudentId);

        if (param.Statuses.HasValue()) query = query.Where(x => param.Statuses!.Contains(x.Status));

        if (param.ExcStatuses.HasValue()) query = query.Where(x => !param.ExcStatuses!.Contains(x.Status));

        if (!string.IsNullOrWhiteSpace(param.Status))
        {
            var status = param.Status.Trim().ToUpper();
            query = query.Where(x => x.Status == status);
        }

        if (!string.IsNullOrWhiteSpace(param.UserPhone))
        {
            var phone = param.UserPhone.Trim().ToLower();
            query = query.Where(x =>
                DbContext.Users.Any(u => u.Id == x.UserId && u.Phone.ToLower().Contains(phone)));
        }

        if (!string.IsNullOrWhiteSpace(param.UserEmail))
        {
            var email = param.UserEmail.Trim().ToLower();
            query = query.Where(x =>
                DbContext.Users.Any(u => u.Id == x.UserId && u.Email.ToLower().Contains(email)));
        }

        if (param.FromCreatedAt != null) query = query.Where(x => x.CreatedAt >= param.FromCreatedAt);
        if (param.ToCreatedAt != null) query = query.Where(x => x.CreatedAt <= param.ToCreatedAt);

        return query;
    }
}