using Course.Common;
using Course.DbModels;
using Course.Repositories.Models;

namespace Course.Repositories;

public class EmailLogRepository : Repository<EmailLog>
{
    public EmailLogRepository(CourseContext context) : base(context)
    {
    }

    public List<EmailLog> List(ListEmailLogParam param, Option? option = null)
    {
        var query = GetQuery(param);
        return Load(query, option);
    }

    public int Count(ListEmailLogParam param)
    {
        var query = GetQuery(param);
        return query.Count();
    }

    private IQueryable<EmailLog> GetQuery(ListEmailLogParam param)
    {
        var query = GetAll();
        if (param.ClassroomId != null) query = query.Where(x => x.ClassroomId == param.ClassroomId);
        if (param.Emails.HasValue())
        {
            query = query.Where(x => param.Emails!.Contains(x.Email));
        }

        return query;
    }
}