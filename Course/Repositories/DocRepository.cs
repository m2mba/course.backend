using Course.Common;
using Course.DbModels;
using Course.Repositories.Models;

namespace Course.Repositories;

public class CatDocRepository : Repository<CatDoc>
{
    public CatDocRepository(CourseContext context) : base(context)
    {
    }


    public List<CatDoc> List(ListCatDocParam param, Option? option = null)
    {
        var query = GetQuery(param);
        return Load(query, option);
    }

    public int Count(ListCatDocParam param)
    {
        var query = GetQuery(param);
        return query.Count();
    }

    private IQueryable<CatDoc> GetQuery(ListCatDocParam param)
    {
        var query = GetAll();

        if (param.IsActive != null) query = query.Where(x => x.IsActive == param.IsActive);

        if (param.Id != null) query = query.Where(x => x.Id == param.Id);

        if (param.Ids.HasValue()) query = query.Where(x => param.Ids!.Contains(x.Id));

        if (!string.IsNullOrWhiteSpace(param.Url))
        {
            var hash = Utils.Sha256Hash(param.Url);
            query = query.Where(x => x.HashUrl == hash);
        }

        return query;
    }
}