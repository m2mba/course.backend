using Course.DbModels;
using Course.Repositories.Models;

namespace Course.Repositories;

public class BankStatementRepository : Repository<BankStatement>
{
    public BankStatementRepository(CourseContext context) : base(context)
    {
    }

    public List<BankStatement> List(ListBankStatementParam param, Option? option = null)
    {
        var query = GetQuery(param);
        if (option?.Orders == null || !option.Orders.Any())
        {
            query = query.OrderByDescending(x => x.CreatedAt).ThenBy(x => x.EntryNumber);
            option ??= new Option();
            option.KeepQueryOrder = true;
        }

        return Load(query, option);
    }

    private IQueryable<BankStatement> GetQuery(ListBankStatementParam param)
    {
        var query = GetAll();
        if (param.ImportFileId != null) query = query.Where(x => x.ImportFileId == param.ImportFileId);
        if (param.ResultFileId != null) query = query.Where(x => x.ResultFileId == param.ResultFileId);

        if (!string.IsNullOrWhiteSpace(param.Status))
        {
            query = query.Where(x => x.Status == param.Status);
        }

        if (!string.IsNullOrWhiteSpace(param.EntryNumber))
        {
            query = query.Where(x => x.EntryNumber == param.EntryNumber);
        }

        return query;
    }
}