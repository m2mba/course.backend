using Course.Common;
using Course.DbModels;
using Course.Repositories.Models;

namespace Course.Repositories;

public class UserRepository : Repository<User>
{
    public UserRepository(CourseContext context) : base(context)
    {
    }

    public List<User> List(ListUserParam param, Option? option = null)
    {
        var query = GetQuery(param);
        return Load(query, option);
    }

    public int Count(ListUserParam param)
    {
        var query = GetQuery(param);
        return query.Count();
    }

    public IQueryable<User> GetQuery(ListUserParam param)
    {
        var query = GetAll();

        if (param.Id != null) query = query.Where(x => x.Id == param.Id);

        if (param.Ids.HasValue()) query = query.Where(x => param.Ids!.Contains(x.Id));

        if (!string.IsNullOrWhiteSpace(param.FullName)) query = query.Where(x => x.FullName.ToLower().Contains(param.FullName.ToLower()));

        if (!string.IsNullOrWhiteSpace(param.Phone)) query = query.Where(x => x.Phone == param.Phone);

        if (param.Emails.HasValue()) query = query.Where(x => param.Emails!.Contains(x.Email));

        if (!string.IsNullOrWhiteSpace(param.Email)) query = query.Where(x => x.Email == param.Email);

        if (param.IsActive != null) query = query.Where(x => x.IsActive == param.IsActive);

        return query;
    }
}