using Course.DbModels;
using Course.Repositories.Models;

namespace Course.Repositories;

public class CatJobRepository : Repository<CatJob>
{
    public CatJobRepository(CourseContext context) : base(context)
    {
    }

    public List<CatJob> List(Option? option = null)
    {
        var query = GetAll().Where(x => x.IsActive == 1);
        return Load(query, option);
    }
}