using Course.Common;
using Course.DbModels;
using Course.Repositories.Models;

namespace Course.Repositories;

public class CourseRepository : Repository<DbModels.Course>
{
    public CourseRepository(CourseContext context) : base(context)
    {
    }

    public List<DbModels.Course> List(ListCourseParam param, Option? option = null)
    {
        var query = GetQuery(param);
        if (option?.Orders == null || !option.Orders.Any())
        {
            query = query.OrderByDescending(x => x.IsActive).ThenBy(x => x.Name);
            option ??= new Option();
            option.KeepQueryOrder = true;
        }
        return Load(query, option);
    }

    private IQueryable<DbModels.Course> GetQuery(ListCourseParam param)
    {
        var query = GetAll();
        if (param.Id != null) query = query.Where(x => x.Id == param.Id);

        if (param.Ids.HasValue()) query = query.Where(x => param.Ids!.Contains(x.Id));

        if (param.IsActive != null) query = query.Where(x => x.IsActive == param.IsActive);

        return query;
    }
}