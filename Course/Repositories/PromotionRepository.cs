using Course.Common;
using Course.DbModels;
using Course.Repositories.Models;

namespace Course.Repositories;

public class PromotionRepository : Repository<Promotion>
{
    public PromotionRepository(CourseContext context) : base(context)
    {
    }


    public List<Promotion> List(ListPromotionParam param, Option? option = null)
    {
        var query = GetQuery(param);
        return Load(query, option);
    }

    public int Count(ListPromotionParam param)
    {
        var query = GetQuery(param);
        return query.Count();
    }

    private IQueryable<Promotion> GetQuery(ListPromotionParam param)
    {
        var query = GetAll();

        if (param.IsActive != null) query = query.Where(x => x.IsActive == param.IsActive);

        if (param.Id != null) query = query.Where(x => x.Id == param.Id);

        if (param.Ids.HasValue()) query = query.Where(x => param.Ids!.Contains(x.Id));
        if (param.ApplyTypes.HasValue()) query = query.Where(x => param.ApplyTypes!.Contains(x.ApplyType));

        if (param.FromFromDate != null) query = query.Where(x => x.FromDate >= param.FromFromDate);
        if (param.ToFromDate != null) query = query.Where(x => x.FromDate <= param.ToFromDate);
        if (param.FromToDate != null) query = query.Where(x => x.ToDate >= param.FromToDate);
        if (param.ToToDate != null) query = query.Where(x => x.ToDate <= param.ToToDate);

        if (param.Available != null && param.Available.Value)
        {
            var now = DateTime.Today;
            query = query.Where(x => x.IsActive == param.IsActive);
            query = query.Where(x => x.FromDate == null || x.FromDate <= now);
            query = query.Where(x => x.ToDate == null || x.ToDate >= now);
            if (param.CourseId != null) query = query.Where(x => x.CourseId == null || x.CourseId == param.CourseId);
        }
        else
        {
            if (param.CourseId != null) query = query.Where(x => x.CourseId == param.CourseId);
        }

        return query;
    }
}