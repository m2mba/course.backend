using Course.DbModels;
using Course.Repositories.Models;

namespace Course.Repositories;

public class JobConfigRepository : Repository<JobConfig>
{
    public JobConfigRepository(CourseContext context) : base(context)
    {
    }

    public List<JobConfig> List(Option? option = null)
    {
        var query = GetAll();
        return Load(query, option);
    }
}