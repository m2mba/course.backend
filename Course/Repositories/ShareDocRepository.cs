using Course.Common;
using Course.DbModels;
using Course.Repositories.Models;

namespace Course.Repositories;

public class ShareDocRepository : Repository<ShareDoc>
{
    public ShareDocRepository(CourseContext context) : base(context)
    {
    }


    public List<ShareDoc> List(ListShareDocParam param, Option? option = null)
    {
        var query = GetQuery(param);
        return Load(query, option);
    }

    public int Count(ListShareDocParam param)
    {
        var query = GetQuery(param);
        return query.Count();
    }

    private IQueryable<ShareDoc> GetQuery(ListShareDocParam param)
    {
        var query = GetAll();

        if (param.Id != null) query = query.Where(x => x.Id == param.Id);

        if (param.Ids.HasValue()) query = query.Where(x => param.Ids!.Contains(x.Id));

        if (param.DocIds.HasValue()) query = query.Where(x => param.DocIds!.Contains(x.DocId));

        if (param.CourseId != null) query = query.Where(x => x.CourseId == param.CourseId);

        if (param.ClassroomId != null) query = query.Where(x => x.ClassroomId == param.ClassroomId);

        if (!string.IsNullOrWhiteSpace(param.Email))
        {
            var email = param.Email.Trim().ToLower();
            query = query.Where(x => x.Emails.Contains(email));
        }

        if (param.FromCreatedAt != null) query = query.Where(x => x.CreatedAt >= param.FromCreatedAt);
        if (param.ToCreatedAt != null) query = query.Where(x => x.CreatedAt <= param.ToCreatedAt);


        return query;
    }
}