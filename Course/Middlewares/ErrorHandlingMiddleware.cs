﻿using System.Net;
using Course.Common.Response;
using Course.Extensions;
using FluentValidation;
using Newtonsoft.Json;

namespace Course.Middlewares;

public class ErrorHandlingMiddleware
{
    private readonly RequestDelegate _next;

    public ErrorHandlingMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    public async Task Invoke(HttpContext context)
    {
        try
        {
            await _next(context);
        }
        catch (ValidationException ex)
        {
            context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            context.Response.ContentType = "application/json";
            var errorResponse = new ErrorResponse
            {
                Message = ex.Errors.Any() ? ex.Errors.First().ErrorMessage : "Dữ liệu không hợp lệ",
                Result = ex.Errors.Select(x => new ErrorDetailResponse
                {
                    ErrorMessage = x.ErrorMessage,
                    ErrorCode = x.ErrorCode,
                    PropertyName = x.PropertyName
                }).ToList()
            };
            await context.Response.WriteAsync(JsonConvert.SerializeObject(errorResponse));
        }
        catch (BusinessException ex)
        {
            context.Response.StatusCode = ex.StatusCode;
            context.Response.ContentType = "application/json";
            var errorResponse = new ErrorResponse
            {
                Message = ex.Message,
                Result = ex.Result
            };
            await context.Response.WriteAsync(JsonConvert.SerializeObject(errorResponse));
        }
        catch (Exception ex)
        {
            // Generate a custom error response
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            context.Response.ContentType = "application/json";
            var errorResponse = new ErrorResponse
            {
                Message = $"An error occurred while processing your request: {ex.Message} - {ex.StackTrace} " +
                          $"- {ex.InnerException?.Message} - {ex.InnerException?.StackTrace}"
            };
            await context.Response.WriteAsync(JsonConvert.SerializeObject(errorResponse));
        }
    }
}