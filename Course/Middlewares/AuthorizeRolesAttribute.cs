﻿using Microsoft.AspNetCore.Authorization;

namespace Course.Middlewares;

/// <summary>
///     Customize authorization by roles
/// </summary>
public class AuthorizeRolesAttribute : AuthorizeAttribute
{
    /// <summary>
    ///     Join roles list of function
    /// </summary>
    /// <param name="roles">string[]</param>
    public AuthorizeRolesAttribute(params string[] roles)
    {
        Roles = string.Join(",", roles);
    }
}