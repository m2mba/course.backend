using Course.Common.Dto;
using Course.DbModels;
using Course.Services.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace Course.Controllers.Course;

[SwaggerTag("api-course-v1")]
[ApiController]
[Route("api/v1/courses")]
public class CourseController : GenericController
{
    public CourseController(IHttpContextAccessor httpContextAccessor, CourseContext dbContext) : base(
        httpContextAccessor,
        dbContext)
    {
    }

    [HttpGet("")]
    [EnableCors("AllowOrigin")]
    [AllowAnonymous]
    public PageDto<CourseDto> ListCourses(int active = 1)
    {
        var list = CourseService.ListCourses(active);
        return new PageDto<CourseDto> { List = list, Total = list.Count };
    }

    [HttpGet("{id:int}")]
    [EnableCors("AllowOrigin")]
    [AllowAnonymous]
    public CourseDto? GetCourse(int id)
    {
        return CourseService.GetCourseById(id);
    }
}