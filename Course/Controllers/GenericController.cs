using Course.Common;
using Course.Common.Dto;
using Course.DbModels;
using Course.Services;
using Microsoft.AspNetCore.Mvc;

namespace Course.Controllers;

public class GenericController : ControllerBase
{
    private readonly CourseContext _dbContext;
    private readonly IHttpContextAccessor _httpContextAccessor;
    private ClassroomService? _classroomService;
    private CourseService? _courseService;
    private RegisterService? _registerService;
    private UserService? _userService;
    private DocService? _docService;
    private JobConfigService? _jobConfigService;

    public GenericController(IHttpContextAccessor httpContextAccessor, CourseContext dbContext)
    {
        _httpContextAccessor = httpContextAccessor;
        _dbContext = dbContext;
    }

    protected UserService UserService => _userService ??= new UserService(_dbContext);
    protected CourseService CourseService => _courseService ??= new CourseService(_dbContext);
    protected ClassroomService ClassroomService => _classroomService ??= new ClassroomService(_dbContext);
    protected RegisterService RegisterService => _registerService ??= new RegisterService(_dbContext);
    protected DocService DocService => _docService ??= new DocService(_dbContext);
    protected JobConfigService JobConfigService => _jobConfigService ??= new JobConfigService(_dbContext);
    protected AuthUserDto GetAuthUser()
    {
        var claimsUser = _httpContextAccessor.HttpContext?.User;
        return new AuthUserDto
        {
            Id = int.Parse(claimsUser?.FindFirst("id")?.Value ?? "0")
        };
    }

    protected bool IsAdmin()
    {
        var user = _httpContextAccessor.HttpContext?.User;
        return user != null && user.Identity != null && user.Identity.IsAuthenticated && user.IsInRole(Constants.ROLE_ADMIN);
    }

    protected string GetToken()
    {
        var headers = _httpContextAccessor.HttpContext?.Request.Headers;
        if (headers == null)
        {
            return "";
        }
        return headers["Authorization"]!;
    }
}