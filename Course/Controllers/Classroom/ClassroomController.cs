using Course.Common;
using Course.Common.Dto;
using Course.Controllers.Classroom.Models;
using Course.Controllers.Models;
using Course.DbModels;
using Course.Repositories.Models;
using Course.Services.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace Course.Controllers.Classroom;

[SwaggerTag("api-course-v1")]
[ApiController]
[Route("api/v1/classrooms")]
public class ClassroomController : GenericController
{
    public ClassroomController(IHttpContextAccessor httpContextAccessor, CourseContext dbContext) : base(
        httpContextAccessor,
        dbContext)
    {
    }

    [HttpGet("")]
    [EnableCors("AllowOrigin")]
    [AllowAnonymous]
    public PageDto<PublicClassroomResponse> ListClassrooms(int courseId = 0, string name = "", int available = 1, int page = 1,
        int size = 10)
    {
        var param = new ListClassroomParam
        {
            CourseId = courseId,
            Name = name
        };
        if (available > 0)
        {
            param.FromStartDate = DateTime.Today;
            param.Status = Constants.CLASSROOM_ACTIVE_STATUS;
        }

        Option? option = null;
        if (size > 0)
        {
            option = new Option
            {
                Paging = true,
                Page = page,
                Size = size
            };
        }

        var pageClassrooms = ClassroomService.ListClassrooms(param, option);
        var data = Utils.CastObject<List<PublicClassroomResponse>>(pageClassrooms.List);
        return new PageDto<PublicClassroomResponse>
        {
            List = data,
            Total = pageClassrooms.Total
        };
    }

    [HttpPost("list")]
    [EnableCors("AllowOrigin")]
    [Authorize(Roles = Constants.ROLE_ADMIN)]
    public PageDto<ClassroomDto> ListClassroom([FromBody] ListClassroomRequest request)
    {
        var trimRequest = request.TrimObject();
        var param = trimRequest.ToParam();
        return ClassroomService.ListClassrooms(param, new Option
        {
            Paging = true,
            Page = trimRequest.Page,
            Size = trimRequest.Size
        });
    }

    [HttpPost("")]
    [EnableCors("AllowOrigin")]
    [Authorize(Roles = Constants.ROLE_ADMIN)]
    public IdResponse CreateClassroom([FromBody] CreateClassroomRequest request)
    {
        var trimRequest = request.TrimObject();
        trimRequest.Validate();
        var classroom = ClassroomService.CreateClassroom(trimRequest.ToModel());
        return new IdResponse { Id = classroom.Id };
    }

    [HttpPut("{id:int}")]
    [EnableCors("AllowOrigin")]
    [Authorize(Roles = Constants.ROLE_ADMIN)]
    public SuccessResponse UpdateClassroom(int id, [FromBody] CreateClassroomRequest request)
    {
        var trimRequest = request.TrimObject();
        trimRequest.Validate();
        ClassroomService.UpdateClassroom(id, trimRequest.ToModel());
        return new SuccessResponse();
    }

    [HttpGet("me/current")]
    [EnableCors("AllowOrigin")]
    public ClassroomDto? GetCurrentClassroom()
    {
        var authUser = GetAuthUser();
        var lastRegister = RegisterService.ListRegisters(new ListRegisterParam
        {
            UserId = authUser.Id,
            ExcStatuses = new List<string> { Constants.REGISTER_CANCEL_STATUS }
        }).List.MaxBy(x => x.Id);
        return lastRegister != null ? ClassroomService.GetClassroomById(lastRegister.ClassroomId) : null;
    }

    [HttpGet("{id:int}")]
    [EnableCors("AllowOrigin")]
    [Authorize(Roles = Constants.ROLE_ADMIN)]
    public ClassroomDto? GetClassroom(int id)
    {
        return ClassroomService.GetClassroomById(id);
    }

    [HttpPut("{id:int}/start")]
    [EnableCors("AllowOrigin")]
    [Authorize(Roles = Constants.ROLE_ADMIN)]
    public SuccessResponse StartClassroom(int id)
    {
        ClassroomService.UpdateClassroomStatus(id, Constants.CLASSROOM_LEARNING_STATUS);
        return new SuccessResponse();
    }

    [HttpPost("{id:int}/notify")]
    [EnableCors("AllowOrigin")]
    [Authorize(Roles = Constants.ROLE_ADMIN)]
    public SuccessResponse NotifyClassroomInfoByEmails(int id)
    {
        ClassroomService.NotifyEmails(id);
        return new SuccessResponse();
    }

    [HttpGet("{id:int}/docs")]
    [EnableCors("AllowOrigin")]
    public PageDto<CourseClassroomDocDto> ListClassroomDocs(int id)
    {
        var classroom = ClassroomService.GetClassroomById(id);
        if (classroom == null)
        {
            return new PageDto<CourseClassroomDocDto>();
        }

        if (IsAdmin())
        {
            return DocService.ListSharableDocs(new ListCourseClassroomDocParam
            {
                CourseId = classroom.CourseId,
                ClassroomId = id
            });
        }

        return DocService.ListSharableDocs(new ListCourseClassroomDocParam
        {
            CourseId = classroom.CourseId,
            ClassroomId = id
        });
    }

    [HttpPost("{classroomId:int}/docs")]
    [EnableCors("AllowOrigin")]
    [Authorize(Roles = Constants.ROLE_ADMIN)]
    public IdResponse CreateClassroomDoc(int classroomId, [FromBody] CreateClassroomDocRequest request)
    {
        var trimRequest = request.TrimObject();
        trimRequest.Validate();
        var doc = DocService.CreateDoc(null, classroomId, trimRequest.ToModel());
        return new IdResponse { Id = doc.Id };
    }

    [HttpPut("docs/{id:int}")]
    [EnableCors("AllowOrigin")]
    [Authorize(Roles = Constants.ROLE_ADMIN)]
    public SuccessResponse UpdateDoc(int id, [FromBody] CreateClassroomDocRequest request)
    {
        var trimRequest = request.TrimObject();
        trimRequest.Validate();
        DocService.UpdateDoc(id, trimRequest.ToModel());
        return new SuccessResponse();
    }

    [HttpDelete("docs/{id:int}")]
    [EnableCors("AllowOrigin")]
    [Authorize(Roles = Constants.ROLE_ADMIN)]
    public SuccessResponse DeleteClassroomDoc(int id)
    {
        DocService.DeleteClassroomDoc(id);
        return new SuccessResponse();
    }

    [HttpPut("{id:int}/stop")]
    [EnableCors("AllowOrigin")]
    [Authorize(Roles = Constants.ROLE_ADMIN)]
    public SuccessResponse StopClassroom(int id)
    {
        ClassroomService.UpdateClassroomStatus(id, Constants.CLASSROOM_STOP_STATUS);
        return new SuccessResponse();
    }

    [HttpPut("{id:int}/finish")]
    [EnableCors("AllowOrigin")]
    [Authorize(Roles = Constants.ROLE_ADMIN)]
    public SuccessResponse FinishClassroom(int id)
    {
        ClassroomService.UpdateClassroomStatus(id, Constants.CLASSROOM_FINISH_STATUS);
        return new SuccessResponse();
    }

    [HttpPut("{id:int}/google-meet")]
    [EnableCors("AllowOrigin")]
    [Authorize(Roles = Constants.ROLE_ADMIN)]
    public SuccessResponse UpdateGoogleMeet(int id, [FromBody] UpdateLinkRequest request)
    {
        var trimRequest = request.TrimObject();
        ClassroomService.UpdateClassroomGoogleMeet(id, trimRequest.Link);
        return new SuccessResponse();
    }

    [HttpPut("{id:int}/zalo")]
    [EnableCors("AllowOrigin")]
    [Authorize(Roles = Constants.ROLE_ADMIN)]
    public SuccessResponse UpdateZalo(int id, [FromBody] UpdateLinkRequest request)
    {
        var trimRequest = request.TrimObject();
        ClassroomService.UpdateClassroomZalo(id, trimRequest.Link);
        return new SuccessResponse();
    }

    [HttpPut("{id:int}/zalo-free-chat")]
    [EnableCors("AllowOrigin")]
    [Authorize(Roles = Constants.ROLE_ADMIN)]
    public SuccessResponse UpdateZaloFreeChat(int id, [FromBody] UpdateLinkRequest request)
    {
        var trimRequest = request.TrimObject();
        ClassroomService.UpdateClassroomZaloFreeChat(id, trimRequest.Link);
        return new SuccessResponse();
    }
}