using Course.Common;
using Course.Controllers.Models;
using Course.Repositories.Models;

namespace Course.Controllers.Classroom.Models;

public class ListClassroomRequest : PageRequest
{
    public int? CourseId { get; set; }
    public int? ClassroomId { get; set; }
    public string? Name { get; set; }
    public string? Status { get; set; }
    public DateTime? FromStartDate { get; set; }
    public DateTime? ToStartDate { get; set; }

    public ListClassroomParam ToParam()
    {
        return new ListClassroomParam
        {
            CourseId = CourseId,
            Id = ClassroomId,
            Name = Name,
            Status = Status,
            FromStartDate = FromStartDate.FormatFromDate(),
            ToStartDate = ToStartDate?.FormatToDate(),
        };
    }
}