using Course.Common;
using FluentValidation;

namespace Course.Controllers.Classroom.Models;

public class CreateClassroomRequest
{
    public string? Code { get; set; }

    public string Name { get; set; } = null!;

    public int CourseId { get; set; }

    public DateTime? StartDate { get; set; }

    public DateTime? EndDate { get; set; }

    public DateTime? StartEnrollmentDate { get; set; }
    public int? TotalSlot { get; set; }
    public string? ZaloLink { get; set; }
    public string? ZaloLinkFreeChat { get; set; }
    public string? GoogleMeetLink { get; set; }
    public string? StudyTime { get; set; }
    public string? Schedule { get; set; }
    public string? Address { get; set; }

    public DbModels.Classroom ToModel()
    {
        return new DbModels.Classroom
        {
            Code = Code,
            Name = Name,
            CourseId = CourseId,
            StartDate = StartDate,
            EndDate = EndDate,
            StartEnrollmentDate = StartEnrollmentDate,
            TotalSlot = TotalSlot,
            TotalRegistered = 0,
            TotalReserveIn = 0,
            TotalReserveOut = 0,
            IsActive = 1,
            ZaloLink = ZaloLink,
            ZaloLinkFreeChat = ZaloLinkFreeChat,
            GoogleMeetLink = GoogleMeetLink,
            StudyTime = StudyTime,
            Schedule = Schedule,
            Address = Address,
            Status = Constants.CLASSROOM_ACTIVE_STATUS,
            UpdatedAt = DateTime.Now
        };
    } public void Validate()
    {
        var validator = new CreateClassroomRequestValidator();
        var validationResult = validator.Validate(this);

        if (!validationResult.IsValid) throw new ValidationException(validationResult.Errors);
    }
}

public class CreateClassroomRequestValidator : AbstractValidator<CreateClassroomRequest>
{
    public CreateClassroomRequestValidator()
    {
        RuleFor(p => p.Code).NotEmpty().WithMessage("Mã lớp học không được để trống");
        RuleFor(p => p.Name).NotEmpty().MaximumLength(255).WithMessage("Tên lớp học không được để trống hoặc dài hơn 255 kí tự");
        RuleFor(p => p.StartDate).NotEmpty().WithMessage("Ngày bắt đầu lớp học không được để trống");
        RuleFor(p => p.EndDate).NotEmpty().WithMessage("Ngày kết thúc lớp học không được để trống");
        RuleFor(p => p).Must(x => x.EndDate >= x.StartDate).WithMessage("Ngày kết thúc lớp học không được nhỏ thua ngày bắt đầu");
        RuleFor(p => p.StartEnrollmentDate).NotEmpty().WithMessage("Thời gian bắt đầu tuyển sinh của lớp học không được để trống");
        RuleFor(p => p).Must(x => x.StartDate >= x.StartEnrollmentDate).WithMessage("Ngày khai giảng lớp học không được nhỏ thua ngày bắt đầu tuyển sinh");
        RuleFor(p => p.CourseId).NotEmpty().GreaterThan(0).WithMessage("Tổng số slot của lớp học không được để trống và phải lớn hơn 0");
        RuleFor(p => p.TotalSlot).NotEmpty().GreaterThan(0).WithMessage("Khóa học không được để trống và phải lớn hơn 0");
    }
}