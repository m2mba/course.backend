using Course.Services.Models;

namespace Course.Controllers.Classroom.Models;

public class PublicClassroomResponse : ClassroomDto
{
    public new int? TotalReserveIn => null;

    public new int? TotalReserveOut => null;

    public new string? ZaloLink => null;

    public new DateTime? UpdatedAt => null;

    public new string? ZaloLinkFreeChat => null;

    public new string? GoogleMeetLink => null;
}