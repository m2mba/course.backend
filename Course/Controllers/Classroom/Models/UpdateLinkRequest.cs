namespace Course.Controllers.Classroom.Models;

public class UpdateLinkRequest
{
    public string? Link { get; set; }
}