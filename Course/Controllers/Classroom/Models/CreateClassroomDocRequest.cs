using Course.Common;
using Course.DbModels;
using FluentValidation;

namespace Course.Controllers.Classroom.Models;

public class CreateClassroomDocRequest
{
    public string? DocType { get; set; }

    public string? Name { get; set; }

    public string? Url { get; set; }
    public string? Method { get; set; }

    public CatDoc ToModel()
    {
        var method = Method?.ToUpper();
        if (string.IsNullOrWhiteSpace(method) || (method != Constants.DOC_METHOD_BEFORE && method != Constants.DOC_METHOD_OFFICIAL))
        {
            method = Constants.DOC_METHOD_OFFICIAL;
        }

        return new CatDoc
        {
            DocType = DocType ?? "",
            Name = Name ?? "",
            Url = Url ?? "",
            IsActive = 1,
            Method = method,
            HashUrl = Utils.Sha256Hash(Url ?? "")
        };
    }

    public void Validate()
    {
        var validator = new CreateClassroomDocRequestValidator();
        var validationResult = validator.Validate(this);

        if (!validationResult.IsValid) throw new ValidationException(validationResult.Errors);
    }
}

public class CreateClassroomDocRequestValidator : AbstractValidator<CreateClassroomDocRequest>
{
    public CreateClassroomDocRequestValidator()
    {
        RuleFor(p => p.DocType).NotNull().NotEmpty().MaximumLength(255);
        RuleFor(p => p.Name).NotNull().NotEmpty();
        RuleFor(p => p.Url).NotNull().NotEmpty();
    }
}