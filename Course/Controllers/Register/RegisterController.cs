using Course.Common;
using Course.Common.Dto;
using Course.Controllers.Models;
using Course.Controllers.Register.Models;
using Course.DbModels;
using Course.Extensions;
using Course.Repositories.Models;
using Course.Services.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace Course.Controllers.Register;

[SwaggerTag("api-course-v1")]
[ApiController]
[Route("api/v1/registers")]
public class RegisterController : GenericController
{
    public RegisterController(IHttpContextAccessor httpContextAccessor, CourseContext dbContext) : base(
        httpContextAccessor,
        dbContext)
    {
    }

    [HttpPost("")]
    [EnableCors("AllowOrigin")]
    public IdResponse RegisterClassroom([FromBody] RegisterClassroomRequest request)
    {
        request.Validate();
        var authUser = GetAuthUser();
        var register = RegisterService.RegisterClassroom(authUser.Id, request.ClassroomId);
        return new IdResponse { Id = register.Id };
    }

    [HttpGet("me/{classroomId:int}/promotion")]
    [EnableCors("AllowOrigin")]
    public RegisterPromotionResponse? GetMyRegisterPromotion(int classroomId)
    {
        var classroom = ClassroomService.GetClassroomById(classroomId);
        if (classroom == null)
        {
            throw new BusinessException("Không tồn tại lớp học");
        }

        var authUser = GetAuthUser();
        var (promotion, _, discountAmount) = RegisterService.GetPromotion(authUser.Id, classroom);
        return new RegisterPromotionResponse
        {
            PromotionId = promotion?.Id,
            ApplyType = promotion?.ApplyType,
            DiscountAmount = discountAmount.GetValueOrDefault(),
            OrgFee = classroom.Fee.GetValueOrDefault(),
            Fee = classroom.Fee.GetValueOrDefault() > discountAmount.GetValueOrDefault()
                ? classroom.Fee.GetValueOrDefault() - discountAmount.GetValueOrDefault()
                : 0
        };
    }

    [HttpPost("emails")]
    [EnableCors("AllowOrigin")]
    [AllowAnonymous]
    public SuccessResponse RegisterEmail([FromBody] RegisterEmailRequest request)
    {
        request.Validate();
        var trimRequest = request.TrimObject();
        RegisterService.RegisterEmail(trimRequest.CourseId, trimRequest.Email);
        return new SuccessResponse();
    }

    [HttpPost("list")]
    [EnableCors("AllowOrigin")]
    [Authorize(Roles = Constants.ROLE_ADMIN)]
    public ListRegisterResponse ListRegisters([FromBody] ListRegisterRequest request)
    {
        var trimRequest = request.TrimObject();
        var param = trimRequest.ToParam();

        var page = RegisterService.ListRegisters(param, new Option
        {
            Paging = true,
            Page = trimRequest.Page,
            Size = trimRequest.Size
        });
        var emails = RegisterService.ListRegisterEmails(param);
        return new ListRegisterResponse
        {
            List = page.List,
            Total = page.Total,
            Emails = emails
        };
    }

    [HttpGet("{id:int}")]
    [EnableCors("AllowOrigin")]
    [Authorize(Roles = Constants.ROLE_ADMIN)]
    public RegisterDto? GetRegister(int id)
    {
        return RegisterService.GetRegisterById(id);
    }

    [HttpGet("me/{id:int}")]
    [EnableCors("AllowOrigin")]
    public RegisterDto? GetMyRegister(int id)
    {
        var register = RegisterService.GetRegisterById(id);
        return register?.UserId != GetAuthUser().Id ? null : register;
    }

    [HttpGet("me")]
    [EnableCors("AllowOrigin")]
    public PageDto<RegisterDto> ListMyRegisters(int page = 1, int size = 10)
    {
        var authUser = GetAuthUser();

        return RegisterService.ListRegisters(new ListRegisterParam { UserId = authUser.Id }, new Option
        {
            Paging = true,
            Page = page,
            Size = size
        });
    }

    [HttpPut("{id:int}/cancel")]
    [EnableCors("AllowOrigin")]
    [Authorize(Roles = Constants.ROLE_ADMIN)]
    public SuccessResponse CancelRegister(int id, [FromBody] ApproveRegisterRequest request)
    {
        var trimRequest = request.TrimObject();
        var authUser = GetAuthUser();
        RegisterService.UpdateRegisterStatus(id, Constants.REGISTER_CANCEL_STATUS, authUser.Id, trimRequest.Note);
        return new SuccessResponse();
    }

    [HttpPut("{id:int}/approve-payment")]
    [EnableCors("AllowOrigin")]
    [Authorize(Roles = Constants.ROLE_ADMIN)]
    public SuccessResponse ApproveRegisterPayment(int id, [FromBody] ApproveRegisterRequest request)
    {
        var trimRequest = request.TrimObject();
        var authUser = GetAuthUser();
        RegisterService.UpdateRegisterStatus(id, Constants.REGISTER_SUCCESS_STATUS, authUser.Id, trimRequest.Note);
        return new SuccessResponse();
    }

    [HttpPut("{id:int}/reserve")]
    [EnableCors("AllowOrigin")]
    [Authorize(Roles = Constants.ROLE_STUDENT)]
    public SuccessResponse Reserve(int id, [FromBody] ReserveRequest request)
    {
        RegisterService.Reserve(GetAuthUser().Id, id, request.ClassroomId);
        return new SuccessResponse();
    }

    [HttpGet("{id:int}/reserve-classrooms")]
    [EnableCors("AllowOrigin")]
    public PageDto<ClassroomDto> GetReserveClassrooms(int id)
    {
        var register = RegisterService.GetRegisterById(id);
        if (register == null)
        {
            return new PageDto<ClassroomDto> { List = new List<ClassroomDto>() };
        }

        var classrooms = ClassroomService.ListClassrooms(new ListClassroomParam
        {
            CourseId = register.CourseId,
            FromStartDate = DateTime.Today.AddDays(1),
            IsActive = 1
        }).List.Where(x => x.Id != register.ClassroomId).ToList();
        return new PageDto<ClassroomDto> { List = classrooms, Total = classrooms.Count };
    }

    [HttpGet("me/reserves")]
    [EnableCors("AllowOrigin")]
    public PageDto<ReserveDto> ListMyReserves(int registerId, int page = 1, int size = 10)
    {
        var authUser = GetAuthUser();
        return RegisterService.ListReserves(new ListReserveParam
            {
                UserId = authUser.Id,
                RegisterId = registerId
            },
            new Option
            {
                Paging = true,
                Page = page,
                Size = size
            });
    }

    [HttpGet("docs")]
    [EnableCors("AllowOrigin")]
    public PageDto<DocDto> GetSharableDocs(int courseId = 0, int classroomId = 0)
    {
        if (courseId == 0 || classroomId == 0)
        {
            return new PageDto<DocDto> { List = new List<DocDto>() };
        }

        var mapDocs = DocService.ListSharableDocs(new ListCourseClassroomDocParam
        {
            CourseId = courseId,
            ClassroomId = classroomId
        });
        var docs = mapDocs.List.Where(x => x.Doc != null).Select(x => x.Doc!).ToList();
        return new PageDto<DocDto> { List = docs, Total = docs.Count };
    }

    [HttpPost("docs")]
    [EnableCors("AllowOrigin")]
    [Authorize(Roles = Constants.ROLE_ADMIN)]
    public SuccessResponse ShareDocs([FromBody] ShareDocsRequest request)
    {
        var trimRequest = request.TrimObject();
        trimRequest.Validate();
        var authUser = GetAuthUser();
        DocService.ShareDocs(trimRequest.ToDto(authUser.Id));
        return new SuccessResponse();
    }

    [HttpPost("docs/list")]
    [EnableCors("AllowOrigin")]
    [Authorize(Roles = Constants.ROLE_ADMIN)]
    public PageDto<ShareDocDto> ListSharedVideos([FromBody] ListShareDocRequest request)
    {
        return DocService.ListSharedDocs(request.ToParam(), new Option
        {
            Paging = true,
            Page = request.Page,
            Size = request.Size
        });
    }

    [HttpGet("{id:int}/qr")]
    [EnableCors("AllowOrigin")]
    public QrCodeResponse GetQrCode(int id)
    {
        var (fee, qr) = RegisterService.GetQr(id);
        var register = RegisterService.GetRegisterById(id);
        var jobConfig = JobConfigService.GetConfig();
        return new QrCodeResponse
        {
            Fee = fee,
            QrCode = qr,
            Content = register?.Code,
            Deadline = (register?.RegisterAt ?? DateTime.Now).AddHours(jobConfig.WaitPaymentHour),
            Bank = new CatBank
            {
                BankName = register?.BankName ?? "",
                BankCode = register?.BankCode ?? "",
                BankBranchName = register?.BankBranchName ?? "",
                AccountHolder = register?.AccountHolder ?? "",
                AccountNumber = register?.AccountNumber ?? ""
            }
        };
    }

    [HttpPut("{id:int}/zalo")]
    [EnableCors("AllowOrigin")]
    [Authorize(Roles = Constants.ROLE_ADMIN)]
    public SuccessResponse AddZalo(int id)
    {
        RegisterService.AddZalo(id);
        return new SuccessResponse();
    }
}