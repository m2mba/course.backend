using Microsoft.AspNetCore.Mvc;

namespace Course.Controllers.Register.Models;

public class ImportRegisterPaymentRequest
{
    [FromForm(Name="file")]
    public IFormFile? File { get; set; }
}
