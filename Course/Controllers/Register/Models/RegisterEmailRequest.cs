using FluentValidation;

namespace Course.Controllers.Register.Models;

public class RegisterEmailRequest
{
    public string Email { get; set; } = "";
    public int CourseId { get; set; }

    public void Validate()
    {
        var validator = new RegisterEmailRequestValidator();
        var validationResult = validator.Validate(this);

        if (!validationResult.IsValid) throw new ValidationException(validationResult.Errors);
    }
}

public class RegisterEmailRequestValidator : AbstractValidator<RegisterEmailRequest>
{
    public RegisterEmailRequestValidator()
    {
        RuleFor(p => p.CourseId).NotNull().GreaterThan(0);
        RuleFor(p => p.Email).NotEmpty().MaximumLength(255).EmailAddress();
    }
}