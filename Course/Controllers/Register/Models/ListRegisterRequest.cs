using Course.Common;
using Course.Controllers.Models;
using Course.Repositories.Models;

namespace Course.Controllers.Register.Models;

public class ListRegisterRequest: PageRequest
{
    public string? Code { get; set; }
    public string? FullName { get; set; }
    public string? Email { get; set; }
    public string? Phone { get; set; }
    public int? CourseId { get; set; }
    public int? ClassroomId { get; set; }
    public string? Status { get; set; }
    public sbyte? SharedVideo { get; set; }
    public int? ZaloAdded { get; set; }
    public DateTime? FromRegisterAt { get; set; }
    public DateTime? ToRegisterAt { get; set; }

    public ListRegisterParam ToParam()
    {
        return new ListRegisterParam
        {
            Code = Code,
            ClassroomId = ClassroomId,
            CourseId = CourseId,
            SharedVideo = SharedVideo,
            ZaloAdded = ZaloAdded,
            FromRegisterAt = FromRegisterAt.FormatFromDate(),
            ToRegisterAt = ToRegisterAt?.FormatToDate(),
            UserFullName = FullName,
            UserPhone = Phone,
            UserEmail = Email,
            Status = Status
        };
    }
}