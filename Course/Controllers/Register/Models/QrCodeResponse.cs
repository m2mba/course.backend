using Course.DbModels;

namespace Course.Controllers.Register.Models;

public class QrCodeResponse
{
    public long Fee { get; set; }
    public string? QrCode { get; set; } 
    public string? Content { get; set; } 
    public DateTime? Deadline { get; set; } 
    public CatBank? Bank { get; set; }
}