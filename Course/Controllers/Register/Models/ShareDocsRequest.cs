using Course.Services.Models;
using FluentValidation;

namespace Course.Controllers.Register.Models;

public class ShareDocsRequest
{
    public int CourseId { get; set; }
    public int ClassroomId { get; set; }
    public List<int> DocIds { get; set; } = new();
    public string Emails { get; set; } = null!;

    public void Validate()
    {
        var validator = new ShareDocsRequestValidator();
        var validationResult = validator.Validate(this);

        if (!validationResult.IsValid) throw new ValidationException(validationResult.Errors);
    }

    public ShareDocActionDto ToDto(int shareUserId)
    {
        return new ShareDocActionDto
        {
            CourseId = CourseId,
            ClassroomId = ClassroomId,
            ShareUserId = shareUserId,
            DocIds = DocIds,
            Emails = Emails
        };
    }
}

public class ShareDocsRequestValidator : AbstractValidator<ShareDocsRequest>
{
    public ShareDocsRequestValidator()
    {
        RuleFor(p => p.CourseId).NotEmpty().GreaterThan(0);
        RuleFor(p => p.ClassroomId).NotEmpty().GreaterThan(0);
        RuleFor(p => p.Emails).NotEmpty();
        RuleFor(p => p.DocIds).NotEmpty().Must(x => x.Count > 0);
    }
}