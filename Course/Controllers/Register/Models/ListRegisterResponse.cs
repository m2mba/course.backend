using Course.Common.Dto;
using Course.Services.Models;

namespace Course.Controllers.Register.Models;

public class ListRegisterResponse: PageDto<RegisterDto>
{
    public string? Emails { get; set; } = "";
}