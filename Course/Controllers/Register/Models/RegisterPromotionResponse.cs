namespace Course.Controllers.Register.Models;

public class RegisterPromotionResponse
{
    public int? PromotionId { get; set; }
    public string? ApplyType { get; set; }
    public long DiscountAmount { get; set; }
    public long Fee { get; set; }
    public long OrgFee { get; set; }
}