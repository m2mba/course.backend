namespace Course.Controllers.Register.Models;

public class ApproveRegisterRequest
{
    public string? Note { get; set; }
}