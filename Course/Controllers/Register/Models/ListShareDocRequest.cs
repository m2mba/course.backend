using Course.Common;
using Course.Controllers.Models;
using Course.Repositories.Models;

namespace Course.Controllers.Register.Models;

public class ListShareDocRequest: PageRequest
{
    public int? CourseId { get; set; }
    public int? ClassroomId { get; set; }
    public DateTime? FromCreatedAt { get; set; } 
    public DateTime? ToCreatedAt { get; set; } 
    public string? Email { get; set; } = null!;

    public ListShareDocParam ToParam()
    {
        return new ListShareDocParam
        {
            CourseId = CourseId,
            ClassroomId = ClassroomId,
            FromCreatedAt = FromCreatedAt.FormatFromDate(),
            ToCreatedAt = ToCreatedAt?.FormatToDate(),
            Email = Email
        };
    }
}