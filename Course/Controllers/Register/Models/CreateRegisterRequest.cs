using FluentValidation;

namespace Course.Controllers.Register.Models;

public class RegisterClassroomRequest
{
    public int ClassroomId { get; set; }
    
    public void Validate()
    {
        var validator = new RegisterClassroomRequestValidator();
        var validationResult = validator.Validate(this);

        if (!validationResult.IsValid) throw new ValidationException(validationResult.Errors);
    }
}

public class RegisterClassroomRequestValidator : AbstractValidator<RegisterClassroomRequest>
{
    public RegisterClassroomRequestValidator()
    {
        RuleFor(p => p.ClassroomId).NotNull().GreaterThan(0);
    }
}