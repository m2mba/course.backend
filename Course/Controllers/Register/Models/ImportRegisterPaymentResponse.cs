namespace Course.Controllers.Register.Models;

public class ImportRegisterPaymentResponse
{
    public int Id { get; set; }

    public string FileName { get; set; } = null!;

    public string FileType { get; set; } = null!;

    public long FileSize { get; set; }

    public string HashId { get; set; } = null!;

    public DateTime CreatedAt { get; set; }
    public int TotalSuccess { get; set; }
    public int TotalFailed { get; set; }
}