using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Course.Common;
using Course.Common.Email;
using Course.Common.Email.Template;
using Course.Controllers.Models;
using Course.Controllers.User.Models;
using Course.DbModels;
using Course.Extensions;
using Course.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.Annotations;

namespace Course.Controllers.User;

[SwaggerTag("api-course-v1")]
[ApiController]
[Route("api/v1/users")]
public class UserController : GenericController
{
    public UserController(IHttpContextAccessor httpContextAccessor, CourseContext dbContext) : base(httpContextAccessor,
        dbContext)
    {
    }

    [HttpPost("register")]
    [EnableCors("AllowOrigin")]
    [AllowAnonymous]
    public SuccessResponse Register([FromBody] RegisterRequest request)
    {
        var trimRequest = request.TrimObject();
        trimRequest.Validate();
        UserService.ValidateEmail(trimRequest.Email);
        UserService.ValidatePhone(trimRequest.Phone);

        var secret = GenerateRegisterToken(trimRequest);
        var baseDomain = ConfigureService.GetString("BaseDomain");
        var mail = new ContentTemplateRegister
        {
            ConfirmLink = $"{baseDomain}/api/v1/users/register/confirm?secret={secret}",
            FullName = request.FullName,
            Phone = request.Phone,
            Email = request.Email,
        };
        var suc = EmailHelper.Send(request.Email, mail.FormatContent(), mail.GetTitle());
        if (!suc)
        {
            throw new BusinessException("Đăng ký không thành công, vui lòng kiểm tra lại địa chỉ Email đã nhập");
        }

        return new SuccessResponse();
    }

    private static string GenerateRegisterToken(RegisterRequest request)
    {
        var tokenHandler = new JwtSecurityTokenHandler();
        var key = Encoding.ASCII.GetBytes(ConfigureService.GetString("SecretKey"));

        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(new[]
            {
                new Claim("fullName", request.FullName),
                new Claim("email", request.Email),
                new Claim("phone", request.Phone),
                new Claim("password", Utils.Sha256Hash(request.Password))
            }),
            Expires = DateTime.UtcNow.AddDays(1),
            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
                SecurityAlgorithms.HmacSha256Signature)
        };
        var token = tokenHandler.CreateToken(tokenDescriptor);
        return tokenHandler.WriteToken(token);
    }

    [HttpGet("register/confirm")]
    [EnableCors("AllowOrigin")]
    [AllowAnonymous]
    public IActionResult ConfirmRegister(string secret)
    {
        try
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(ConfigureService.GetString("SecretKey") ??
                                              throw new ArgumentException("Config"));
            tokenHandler.ValidateToken(secret, new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidateIssuer = false,
                ValidateAudience = false,
                ClockSkew = TimeSpan.Zero
            }, out var validatedToken);

            var jwtToken = (JwtSecurityToken)validatedToken;
            var email = jwtToken.Claims.First(x => x.Type == "email").Value;
            var phone = jwtToken.Claims.First(x => x.Type == "phone").Value;
            var fullName = jwtToken.Claims.First(x => x.Type == "fullName").Value;
            var password = jwtToken.Claims.First(x => x.Type == "password").Value;

            if (UserService.GetUserByEmail(email) != null)
            {
                return base.Content(Utils.GetErrorByHtml("Email đã được kích hoạt trước đó"), "text/html");
            }

            if (UserService.GetUserByPhone(phone) != null)
            {
                return base.Content(Utils.GetErrorByHtml("Số điện thoại đã tồn tại trong hệ thống"), "text/html");
            }

            var user = new DbModels.User
            {
                FullName = fullName,
                Phone = phone,
                Email = email.ToLower(),
                Roles = JsonConvert.SerializeObject(new List<string> { Constants.ROLE_STUDENT }),
                Password = password,
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                IsActive = 1,
                FirstName = fullName.Split(' ').Last(),
                Status = Constants.ACTIVE
            };
            UserService.CreateUser(user);
            var url = user.Roles.Contains(Constants.ROLE_ADMIN)
                ? ConfigureService.GetString("AdminWebUrl")
                : ConfigureService.GetString("StudentWebUrl");
            return Redirect(url + "/auth/login?from=register");
        }
        catch
        {
            return base.Content(Utils.GetErrorByHtml("Đường dẫn không hợp lệ"), "text/html");
        }
    }

    [HttpGet("me")]
    [EnableCors("AllowOrigin")]
    public UserResponse GetMe()
    {
        var authUser = GetAuthUser();

        var user = UserService.GetUserById(authUser.Id);
        if (user == null) return new UserResponse();

        var userResponse = Utils.CastObject<UserResponse>(user);
        userResponse.Password = "";
        userResponse.RefreshToken = "";
        userResponse.ListRoles = JsonConvert.DeserializeObject<List<string>>(user.Roles);

        return userResponse;
    }

    [HttpPut("me")]
    [EnableCors("AllowOrigin")]
    public SuccessResponse UpdateMe([FromBody] UpdateMeRequest request)
    {
        var trimRequest = request.TrimObject();
        trimRequest.Validate();
        var authUser = GetAuthUser();
        var user = UserService.ValidateId<DbModels.User>(authUser.Id, "User không tồn tại trong hệ thống");
        UserService.ValidatePhone(trimRequest.Phone, user.Id);
        user.FullName = trimRequest.FullName;
        user.Phone = trimRequest.Phone;
        user.CatJobId = trimRequest.CatJobId;
        UserService.UpdateUser(user);
        return new SuccessResponse();
    }

    [HttpPut("change-password")]
    [EnableCors("AllowOrigin")]
    public SuccessResponse ChangePassword([FromBody] ChangePasswordRequest request)
    {
        var trimRequest = request.TrimObject();
        trimRequest.Validate();
        var authUser = GetAuthUser();
        var user = UserService.GetUserById(authUser.Id);
        if (user == null) throw new BusinessException("User không tồn tại trong hệ thống");

        var hashPass = Utils.Sha256Hash(request.OldPassword);
        if (!user.Password.Equals(hashPass))
            throw new BusinessException("Mật khẩu cũ không chính xác. Vui lòng kiểm tra lại");

        user.Password = Utils.Sha256Hash(request.NewPassword);
        UserService.UpdateUser(user);
        return new SuccessResponse();
    }
}