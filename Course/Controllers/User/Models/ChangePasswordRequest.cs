using FluentValidation;

namespace Course.Controllers.User.Models;

public class ChangePasswordRequest
{
    public string OldPassword { get; set; } = "";
    public string NewPassword { get; set; } = "";

    public void Validate()
    {
        var validator = new ChangePasswordRequestValidator();
        var validationResult = validator.Validate(this);

        if (!validationResult.IsValid) throw new ValidationException(validationResult.Errors);
    }
}

public class ChangePasswordRequestValidator : AbstractValidator<ChangePasswordRequest>
{
    public ChangePasswordRequestValidator()
    {
        RuleFor(p => p.OldPassword).NotEmpty().MinimumLength(6);
        RuleFor(p => p.NewPassword).NotEmpty().MinimumLength(6).NotEqual(x => x.OldPassword);
    }
}