using Diacritics.Extensions;
using FluentValidation;

namespace Course.Controllers.User.Models;

public class UpdateMeRequest
{
    public string FullName { get; set; } = "";
    public string Phone { get; set; } = "";
    public int? CatJobId { get; set; }


    public void Validate()
    {
        var validator = new UpdateMeRequestValidator();
        var validationResult = validator.Validate(this);

        if (!validationResult.IsValid) throw new ValidationException(validationResult.Errors);
    }
}

public class UpdateMeRequestValidator : AbstractValidator<UpdateMeRequest>
{
    public UpdateMeRequestValidator()
    {
        RuleFor(p => p.FullName.RemoveDiacritics()).NotEmpty().MinimumLength(3).MaximumLength(255).Matches("^[a-zA-ZĐđ ]+$").WithMessage("Vui lòng nhập họ tên từ 3 ký tự trở lên và không được chứa ký tự đặc biệt");
        RuleFor(p => p.Phone).NotEmpty().Matches("^(84|0[35789])[0-9]{8}$").WithMessage("Số điện thoại không đúng định dạng. Vui lòng nhập lại");
    }
}