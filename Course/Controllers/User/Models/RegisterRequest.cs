using Course.Common;
using Diacritics.Extensions;
using FluentValidation;
using Newtonsoft.Json;

namespace Course.Controllers.User.Models;

public class RegisterRequest
{
    public string FullName { get; set; } = "";
    public string Password { get; set; } = "";
    public string Phone { get; set; } = "";
    public string Email { get; set; } = "";

    public void Validate()
    {
        var validator = new RegisterRequestValidator();
        var validationResult = validator.Validate(this);

        if (!validationResult.IsValid) throw new ValidationException(validationResult.Errors);
    }
}

public class RegisterRequestValidator : AbstractValidator<RegisterRequest>
{
    public RegisterRequestValidator()
    {
        RuleFor(p => p.Password).NotEmpty().MinimumLength(6);
        RuleFor(p => p.FullName.RemoveDiacritics()).NotEmpty().MinimumLength(3).MaximumLength(255).Matches("^[a-zA-ZĐđ ]*$").WithMessage("Vui lòng nhập họ tên từ 3 ký tự trở lên và không được chứa ký tự đặc biệt");
        RuleFor(p => p.Phone).NotEmpty().Matches("^(84|0[35789])[0-9]{8}$").WithMessage("Số điện thoại không đúng định dạng. Vui lòng nhập lại");
        RuleFor(p => p.Email).NotEmpty().EmailAddress().WithMessage("Email không đúng định dạng. Vui lòng nhập lại");
    }
}