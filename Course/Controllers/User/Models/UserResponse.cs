namespace Course.Controllers.User.Models;

public class UserResponse : DbModels.User
{
    public string? JobName { get; set; }
    public List<string>? ListRoles { get; set; }
}