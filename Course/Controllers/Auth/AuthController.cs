using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using Course.Common;
using Course.Common.Email;
using Course.Common.Email.Template;
using Course.Controllers.Auth.Models;
using Course.Controllers.Models;
using Course.DbModels;
using Course.Extensions;
using Course.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.Annotations;

namespace Course.Controllers.Auth;

[SwaggerTag("api-course-v1")]
[ApiController]
[Route("api/v1/auth")]
public class AuthController : GenericController
{
    private const int TOKEN_EXP_HOUR = 10 * 365 * 24;

    public AuthController(IHttpContextAccessor httpContextAccessor, CourseContext dbContext) : base(httpContextAccessor,
        dbContext)
    {
    }

    [HttpPost("login")]
    [EnableCors("AllowOrigin")]
    [AllowAnonymous]
    public LoginResponse Login([FromBody] LoginRequest request)
    {
        var trimRequest = request.TrimObject();
        trimRequest.Validate();
        var user = request.UserName.Contains('@')
            ? UserService.GetUserByEmail(request.UserName)
            : UserService.GetUserByPhone(request.UserName);
        if (user == null) throw new AuthorizationException("Tên đăng nhập hoặc mật khẩu không đúng");

        var hashPass = Utils.Sha256Hash(request.Password);

        if (!user.Password.Equals(hashPass)) throw new AuthorizationException("Tên đăng nhập hoặc mật khẩu không đúng");

        var roles = JsonConvert.DeserializeObject<List<string>>(user.Roles) ?? new List<string>();
        if (!string.IsNullOrWhiteSpace(trimRequest.Role) && !roles.Contains(trimRequest.Role))
        {
            throw new AuthorizationException("Đăng nhập không hợp lệ, vui lòng thử lại");
        }

        var accessToken = GenerateAccessToken(user);
        var refreshToken = GenerateRefreshToken();

        user.RefreshToken = refreshToken;
        UserService.UpdateUser(user);

        return new LoginResponse(accessToken, refreshToken);
    }

    [HttpPost("forgot")]
    [EnableCors("AllowOrigin")]
    [AllowAnonymous]
    public SuccessResponse ForgotPassword([FromBody] ForgotPasswordRequest request)
    {
        var trimRequest = request.TrimObject();
        trimRequest.Validate();

        var user = UserService.GetUserByEmail(request.Email.ToLower());

        if (user == null) throw new BusinessException("Email không tồn tại trong hệ thống");

        const string validChars = "ABCDEFGHJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*?_-";
        var random = new Random();

        var chars = new char[6];
        for (var i = 0; i < 6; i++) chars[i] = validChars[random.Next(0, validChars.Length)];

        var newPass = new string(chars);
        var baseDomain = ConfigureService.GetString("BaseDomain");
        var secret = GenerateForgotPasswordToken(user, newPass);

        var mail = new ContentTemplateForgotPassword
        {
            ConfirmLink = $"{baseDomain}/api/v1/auth/forgot/confirm?secret={secret}",
            FullName = user.FullName,
            Phone = user.Phone,
            Email = user.Email,
            NewPassword = newPass
        };
        EmailHelper.Send(user.Email, mail.FormatContent(), mail.GetTitle());

        return new SuccessResponse();
    }

    [HttpGet("forgot/confirm")]
    [EnableCors("AllowOrigin")]
    [AllowAnonymous]
    public IActionResult ConfirmForgotPassword(string secret)
    {
        try
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(ConfigureService.GetString("SecretKey") ??
                                              throw new ArgumentException("Config"));
            tokenHandler.ValidateToken(secret, new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidateIssuer = false,
                ValidateAudience = false,
                ClockSkew = TimeSpan.Zero
            }, out var validatedToken);

            var jwtToken = (JwtSecurityToken)validatedToken;
            var sid = jwtToken.Claims.First(x => x.Type == "id").Value;
            var email = jwtToken.Claims.First(x => x.Type == "email").Value;
            var password = jwtToken.Claims.First(x => x.Type == "password").Value;

            var user = UserService.GetUserByEmail(email);

            if (user == null || !user.Id.ToString().Equals(sid))
            {
                return base.Content(Utils.GetErrorByHtml("Email không tồn tại"), "text/html");
            }

            user.Password = Utils.Sha256Hash(password);
            UserService.UpdateUser(user);
            var url = user.Roles.Contains(Constants.ROLE_ADMIN)
                ? ConfigureService.GetString("AdminWebUrl")
                : ConfigureService.GetString("StudentWebUrl");
            return Redirect(url + "/auth/login?from=forgot");
        }
        catch
        {
            return base.Content(Utils.GetErrorByHtml("Đường dẫn không hợp lệ"), "text/html");
        }
    }

    [HttpPost("refresh")]
    [EnableCors("AllowOrigin")]
    public LoginResponse RefreshToken([FromBody] RefreshRequest request)
    {
        try
        {
            request.Validate();
            var principal = GetPrincipalFromExpiredToken(GetToken());
            if (principal == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }

            var userId = int.Parse(principal.FindFirst("id")?.Value ?? "0");
            var user = UserService.GetUserById(userId);
            if (user == null)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }

            if (user.RefreshToken != request.RefreshToken)
            {
                throw new BusinessException("Dữ liệu không hợp lệ");
            }

            var accessToken = GenerateAccessToken(user);
            var refreshToken = GenerateRefreshToken();

            return new LoginResponse(accessToken, refreshToken);
        }
        catch
        {
            throw new BusinessException("Dữ liệu không hợp lệ");
        }
    }

    private static string GenerateForgotPasswordToken(DbModels.User user, string newPassword)
    {
        var tokenHandler = new JwtSecurityTokenHandler();
        var key = Encoding.ASCII.GetBytes(ConfigureService.GetString("SecretKey"));

        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(new[]
            {
                new Claim("id", user.Id.ToString()),
                new Claim("email", user.Email),
                new Claim("password", newPassword)
            }),
            Expires = DateTime.UtcNow.AddDays(1),
            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
                SecurityAlgorithms.HmacSha256Signature)
        };
        var token = tokenHandler.CreateToken(tokenDescriptor);
        return tokenHandler.WriteToken(token);
    }

    private static string GenerateAccessToken(DbModels.User user)
    {
        var tokenHandler = new JwtSecurityTokenHandler();
        var key = Encoding.ASCII.GetBytes(ConfigureService.GetString("SecretKey"));
        var claim = new List<Claim>
        {
            new("id", user.Id.ToString()),
            new("roles", JsonConvert.SerializeObject(user.Roles))
        };
        if (!string.IsNullOrWhiteSpace(user.Roles))
        {
            var roles = JsonConvert.DeserializeObject<List<string>>(user.Roles);
            if (roles != null && roles.Any()) claim.AddRange(roles.Select(item => new Claim(ClaimTypes.Role, item)));
        }

        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(claim),
            Expires = DateTime.UtcNow.AddHours(TOKEN_EXP_HOUR),
            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
                SecurityAlgorithms.HmacSha256Signature)
        };
        var token = tokenHandler.CreateToken(tokenDescriptor);
        return tokenHandler.WriteToken(token);
    }

    private static string GenerateRefreshToken()
    {
        var randomNumber = new byte[64];
        using var rng = RandomNumberGenerator.Create();
        rng.GetBytes(randomNumber);
        return Convert.ToBase64String(randomNumber);
    }

    private static ClaimsPrincipal? GetPrincipalFromExpiredToken(string? token)
    {
        var tokenValidationParameters = new TokenValidationParameters
        {
            ValidateAudience = false,
            ValidateIssuer = false,
            ValidateIssuerSigningKey = true,
            IssuerSigningKey =
                new SymmetricSecurityKey(Encoding.UTF8.GetBytes(ConfigureService.GetString("SecretKey"))),
            ValidateLifetime = false
        };

        var tokenHandler = new JwtSecurityTokenHandler();
        var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out SecurityToken securityToken);
        if (securityToken is not JwtSecurityToken jwtSecurityToken ||
            !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256,
                StringComparison.InvariantCultureIgnoreCase))
            throw new SecurityTokenException("Invalid token");

        return principal;
    }
}