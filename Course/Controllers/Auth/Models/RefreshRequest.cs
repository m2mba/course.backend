using FluentValidation;

namespace Course.Controllers.Auth.Models;

public class RefreshRequest
{
    public string RefreshToken { get; set; } = "";

    public void Validate()
    {
        var validator = new RefreshValidator();
        var validationResult = validator.Validate(this);

        if (!validationResult.IsValid) throw new ValidationException(validationResult.Errors);
    }
}

public class RefreshValidator : AbstractValidator<RefreshRequest>
{
    public RefreshValidator()
    {
        RuleFor(p => p.RefreshToken).NotEmpty();
    }
}