using FluentValidation;

namespace Course.Controllers.Auth.Models;

public class ForgotPasswordRequest
{
    public string Email { get; set; } = "";

    public void Validate()
    {
        var validator = new ForgotPasswordRequestValidator();
        var validationResult = validator.Validate(this);

        if (!validationResult.IsValid) throw new ValidationException(validationResult.Errors);
    }
}

public class ForgotPasswordRequestValidator : AbstractValidator<ForgotPasswordRequest>
{
    public ForgotPasswordRequestValidator()
    {
        RuleFor(p => p.Email).NotEmpty().EmailAddress();
    }
}