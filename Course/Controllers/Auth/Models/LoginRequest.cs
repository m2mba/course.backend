using FluentValidation;

namespace Course.Controllers.Auth.Models;

public class LoginRequest
{
    public string UserName { get; set; } = "";
    public string Password { get; set; } = "";
    public string? Role { get; set; } = "";

    public void Validate()
    {
        var validator = new LoginRequestValidator();
        var validationResult = validator.Validate(this);

        if (!validationResult.IsValid) throw new ValidationException(validationResult.Errors);
    }
}

public class LoginRequestValidator : AbstractValidator<LoginRequest>
{
    public LoginRequestValidator()
    {
        RuleFor(p => p.UserName).NotEmpty();
        RuleFor(p => p.Password).NotEmpty();
    }
}