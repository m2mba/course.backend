namespace Course.Controllers.Models;

public class IdResponse
{
    public int Id { get; set; }
}