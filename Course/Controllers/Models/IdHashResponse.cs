namespace Course.Controllers.Models;

public class IdHashResponse
{
    public string? HashId { get; set; }
}