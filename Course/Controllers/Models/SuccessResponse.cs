namespace Course.Controllers.Models;

public class SuccessResponse
{
    public bool Success { get; set; } = true;
}