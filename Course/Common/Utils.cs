using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Course.Common;

public static class Utils
{
    public static string Sha256Hash(string value)
    {
        var sb = new StringBuilder();
        var enc = Encoding.UTF8;
        var result = SHA256.HashData(enc.GetBytes(value));
        foreach (var b in result) sb.Append(b.ToString("x2"));

        return sb.ToString();
    }

    public static T CastObject<T>(object obj)
    {
        return NormalDeserializeObject<T>(NormalSerializeJson(obj)) ?? throw new ArgumentNullException(nameof(T));
    }

    private static string NormalSerializeJson(dynamic obj)
    {
        return JsonConvert.SerializeObject(obj);
    }

    public static T? NormalDeserializeObject<T>(string? jsonStr, ExpandoObjectConverter? convertor = null)
    {
        if (jsonStr == null) return default;

        return convertor != null
            ? JsonConvert.DeserializeObject<T>(jsonStr, convertor)
            : JsonConvert.DeserializeObject<T>(jsonStr);
    }

    public static string GetMimeType(string fileName)
    {
        if (fileName.EndsWith(".pdf", StringComparison.OrdinalIgnoreCase))
        {
            return "application/pdf";
        }

        if (fileName.EndsWith(".jpg", StringComparison.OrdinalIgnoreCase) ||
            fileName.EndsWith(".jpeg", StringComparison.OrdinalIgnoreCase))
        {
            return "image/jpeg";
        }

        return fileName.EndsWith(".png", StringComparison.OrdinalIgnoreCase) ? "image/png" : "application/octet-stream";
    }
    
    public static string GetErrorByHtml(string errorText)
    {
        using var reader = new StreamReader(Path.Combine("wwwroot", "error.html"));
        var body = reader.ReadToEnd();
        body = body.Replace("[text]", errorText);
        return body;
    }
}