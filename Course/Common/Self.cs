using System.Globalization;

namespace Course.Common;

public static class Self
{
    public static bool HasValue<T>(this IEnumerable<T>? list)
    {
        return list != null && list.Any();
    }

    public static bool HasValue<T1, T2>(this Dictionary<T1, T2>? dic) where T1 : notnull
    {
        return dic != null && dic.Any();
    }

    public static T TrimObject<T>(this T input)
    {
        if (input == null) return input;

        var stringProperties = input.GetType().GetProperties()
            .Where(p => p.PropertyType == typeof(string) && p.CanWrite);

        foreach (var stringProperty in stringProperties)
        {
            var currentValue = stringProperty.GetValue(input, null);
            var s = currentValue?.ToString();
            if (s == null) continue;

            stringProperty.SetValue(input, s.Trim(), null);
        }

        return input;
    }

    public static DateTime? FormatFromDate(this DateTime? d)
    {
        return d?.Date ?? d;
    }

    public static DateTime FormatToDate(this DateTime d)
    {
        return d.Date.AddDays(1).AddSeconds(-1);
    }

    public static string GetValueByIndex(this string[] l, int index, int len = 0)
    {
        if (len == 0)
        {
            len = l.Length;
        }

        return index > len ? "" : l[index];
    }

    public static string FormatMoney(this long val)
    {
        if (val == 0) return "0";
        var cul = CultureInfo.GetCultureInfo("vi-VN");
        return val.ToString("#,###", cul.NumberFormat);
    }

    public static string FormatRate(this decimal val)
    {
        if (val > 1 && val % 1 == 0)
        {
            return (long)val + " %";
        }
        return $"{val / 100:P2}";
    }
}