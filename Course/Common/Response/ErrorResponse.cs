using Newtonsoft.Json;

namespace Course.Common.Response;

public class ErrorResponse
{
    [JsonProperty("message")] public string? Message { get; set; }
    [JsonProperty("result")] public object? Result { get; set; }
}

public class ErrorDetailResponse
{
    [JsonProperty("errorMessage")] public string? ErrorMessage { get; set; }
    [JsonProperty("errorCode")] public string? ErrorCode { get; set; }
    [JsonProperty("propertyName")] public string? PropertyName { get; set; }
}