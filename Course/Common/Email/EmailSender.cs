﻿using Course.Services;
using Mailjet.Client;
using Mailjet.Client.TransactionalEmails;
using MailKit.Security;
using MimeKit;
using SmtpClient = MailKit.Net.Smtp.SmtpClient;

namespace Course.Common.Email;

public class EmailSender
{
    private const int MAX_RETRY_SEND_MAIL = 3;

    public static bool SendMailCommon(string email, string subject, string content)
    {
        if (string.IsNullOrWhiteSpace(email)) return false;
        var sendMailJet = SendMailByMailJet(email, subject, content);
        return sendMailJet || SendMailByAddress(email, subject, content);
    }

    private static bool SendMailByMailJet(string toEmail, string subject, string content)
    {
        var emailSettings = ConfigureService.GetObject<MailJetSettingOptions>(MailJetSettingOptions.EmailSettings);
        if (emailSettings == null) return true;
        for (var i = 0; i < MAX_RETRY_SEND_MAIL; i++)
        {
            try
            {
                var client = new MailjetClient(emailSettings.Key, emailSettings.Secret);
                var jetEmail = new TransactionalEmailBuilder()
                    .WithFrom(new SendContact(emailSettings.From, "M2mBA"))
                    .WithSubject(subject)
                    .WithHtmlPart(content)
                    .WithTo(new SendContact(toEmail))
                    .Build();

                var result = client.SendTransactionalEmailAsync(jetEmail).Result;
                if (result.Messages.Any(x => !x.Errors.HasValue()))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                Task.Delay(5 * 1000);
            }
        }

        return false;
    }

    private static bool SendMailByAddress(string toEmail, string subject, string content)
    {
        var emailSettings = ConfigureService.GetObject<EmailSettingOptions>(EmailSettingOptions.EmailSettings);
        if (emailSettings == null) return false;
        for (var i = 0; i < MAX_RETRY_SEND_MAIL; i++)
        {
            try
            {
                using var client = new SmtpClient();
                var message = new MimeMessage { Subject = subject };
                message.From.Add(MailboxAddress.Parse(emailSettings.From));
                message.To.Add(MailboxAddress.Parse(toEmail));
                var builder = new BodyBuilder { HtmlBody = content };

                message.Body = builder.ToMessageBody();
                client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                client.Connect(emailSettings.SmtpServer, emailSettings.Port, SecureSocketOptions.SslOnConnect);
                client.Authenticate(emailSettings.Username, emailSettings.Password);
                client.Send(message);
                client.Disconnect(true);
                return true;
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                Task.Delay(5 * 1000);
            }
        }

        return false;
    }
}

public class EmailSettingOptions
{
    public const string EmailSettings = "EmailSettings";
    public string From { get; set; } = null!;
    public string Username { get; set; } = null!;
    public string Password { get; set; } = null!;
    public string SmtpServer { get; set; } = null!;
    public int Port { get; set; }
}

public class MailJetSettingOptions
{
    public const string EmailSettings = "MailJetSettings";
    public string Key { get; set; } = null!;
    public string Secret { get; set; } = null!;
    public string From { get; set; } = null!;
}