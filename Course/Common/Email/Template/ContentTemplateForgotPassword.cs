namespace Course.Common.Email.Template;

public class ContentTemplateForgotPassword : ContentTemplateBase
{
    public ContentTemplateForgotPassword() : base(true)
    {
        Title = "[M2mBA] Quên mật khẩu";
        TemplatePath = "mail_forgot_password.html";
    }

    public string ConfirmLink { get; set; } = null!;
    public string FullName { get; set; } = null!;
    public string Phone { get; set; } = null!;
    public string Email { get; set; } = null!;
    public string NewPassword { get; set; } = null!;

    public new string FormatContent()
    {
        var body = base.FormatContent();
        body = body.Replace("[confirm_link]", ConfirmLink);
        body = body.Replace("[full_name]", FullName);
        body = body.Replace("[phone]", Phone);
        body = body.Replace("[email]", Email);
        body = body.Replace("[new_password]", NewPassword);

        return body;
    }
}