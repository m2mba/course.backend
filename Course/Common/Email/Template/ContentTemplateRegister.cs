namespace Course.Common.Email.Template;

public class ContentTemplateRegister : ContentTemplateBase
{
    public ContentTemplateRegister() : base(true)
    {
        Title = "[M2mBA] Kích hoạt tài khoản";
        TemplatePath = "mail_register.html";
    }

    public string ConfirmLink { get; set; } = null!;
    public string FullName { get; set; } = null!;
    public string Phone { get; set; } = null!;
    public string Email { get; set; } = null!;

    public new string FormatContent()
    {
        var body = base.FormatContent();
        body = body.Replace("[confirm_link]", ConfirmLink);
        body = body.Replace("[full_name]", FullName);
        body = body.Replace("[phone]", Phone);
        body = body.Replace("[email]", Email);

        return body;
    }
}