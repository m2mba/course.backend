namespace Course.Common.Email.Template;

/// <summary>
///     Mail contents to replace from template
/// </summary>
public class ContentTemplateBase
{
    protected ContentTemplateBase(bool privateTemplate = false)
    {
        PrivateTemplate = privateTemplate;
    }

    private bool PrivateTemplate { get; }
    protected string? TemplatePath { get; set; }
    protected string? Content { get; set; }
    protected string Title { get; set; } = null!;

    private static string GetTemplate(string path)
    {
        var filePath = Path.Combine(Path.Combine("wwwroot", "templates/email"), path);
        using var reader = new StreamReader(filePath);
        var body = reader.ReadToEnd();
        return body;
    }

    protected string FormatContent()
    {
        var templateContent = Content ?? (string.IsNullOrWhiteSpace(TemplatePath) ? "" : GetTemplate(TemplatePath!));
        if (PrivateTemplate) return templateContent;

        var commonContent = GetTemplate("mail_common.html");
        return commonContent.Replace("[content]", templateContent);
    }

    public string GetTitle()
    {
        return Title;
    }
}