namespace Course.Common.Email.Template;

public class ContentTemplateClassroomNotify : ContentTemplateBase
{
    public ContentTemplateClassroomNotify(string courseName, string method)
    {
        Title = $"[M2mBA] Thông báo tuyển sinh khóa học {courseName} - Hình thức {method}";
        TemplatePath = "mail_classroom_notify.html";
    }

    public string CourseName { get; set; } = null!;
    public string Method { get; set; } = null!;
    public long Fee { get; set; }
    public string RefLink { get; set; } = null!;
    public string TimeInfo { get; set; } = null!;
    public string Address { get; set; } = "";
    public DateTime StartEnrollmentDate { get; set; }
    public DateTime StartDate { get; set; }

    public new string FormatContent()
    {
        var body = base.FormatContent();
        body = body.Replace("[course_name]", CourseName);
        body = body.Replace("[method]", Method);
        body = body.Replace("[fee]", Fee.FormatMoney());
        var address = "";
        if (!string.IsNullOrWhiteSpace(Address))
        {
            address = $"<p>-&nbsp;Địa điểm học: {Address}</p>";
        }

        body = body.Replace("[address]", address);
        body = body.Replace("[start_enrollment_date]", StartEnrollmentDate.ToString("HH:mm:ss dd/MM/yyyy"));
        body = body.Replace("[start_date]", StartDate.ToString("dd/MM/yyyy"));
        body = body.Replace("[time_info]", TimeInfo);
        body = body.Replace("[ref_link]", RefLink);

        return body;
    }
}