namespace Course.Common.Email.Template;

public class ContentTemplateManual : ContentTemplateBase
{
    public ContentTemplateManual(string title, string content)
    {
        Title = title;
        Content = content;
    }

    public new string FormatContent()
    {
        var body = base.FormatContent();
        return body;
    }
}