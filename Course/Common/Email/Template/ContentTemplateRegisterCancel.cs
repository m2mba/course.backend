namespace Course.Common.Email.Template;

public class ContentTemplateRegisterCancel : ContentTemplateBase
{
    public ContentTemplateRegisterCancel() : base(true)
    {
        Title = "[M2mBA] Hủy đăng ký khóa học";
        TemplatePath = "mail_register_cancel.html";
    }

    public string FullName { get; set; } = null!;
    public string Phone { get; set; } = null!;
    public string Email { get; set; } = null!;
    public string CourseInfo { get; set; } = null!;
    public long Fee { get; set; }
    public string Reason { get; set; } = null!;
    public string RefLink { get; set; } = null!;
    public DateTime RegisterAt { get; set; }

    public new string FormatContent()
    {
        var body = base.FormatContent();
        body = body.Replace("[full_name]", FullName);
        body = body.Replace("[phone]", Phone);
        body = body.Replace("[email]", Email);
        body = body.Replace("[course_info]", CourseInfo);
        body = body.Replace("[fee]", Fee.FormatMoney());
        body = body.Replace("[registered_at]", RegisterAt.ToString("HH:mm dd/MM/yyyy"));
        body = body.Replace("[reason]", Reason);
        body = body.Replace("[ref_link]", RefLink);

        return body;
    }
}