namespace Course.Common.Email.Template;

public class ContentTemplateChangeEmail : ContentTemplateBase
{
    public ContentTemplateChangeEmail() : base(true)
    {
        Title = "[M2mBA] Đổi email mới";
        TemplatePath = "mail_change_email.html";
    }

    public string ConfirmLink { get; set; } = null!;
    public string FullName { get; set; } = null!;
    public string Phone { get; set; } = null!;
    public string Email { get; set; } = null!;

    public new string FormatContent()
    {
        var body = base.FormatContent();
        body = body.Replace("[confirm_link]", ConfirmLink);
        body = body.Replace("[full_name]", FullName);
        body = body.Replace("[phone]", Phone);
        body = body.Replace("[email]", Email);

        return body;
    }
}