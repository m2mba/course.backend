namespace Course.Common.Email.Template;

public class ContentTemplateRegisterClassroom : ContentTemplateBase
{
    public ContentTemplateRegisterClassroom()
    {
        Title = "[M2mBA] Đăng ký giữ chỗ khóa học thành công";
        TemplatePath = "mail_register_classroom.html";
    }

    public string FullName { get; set; } = null!;
    public string Phone { get; set; } = null!;
    public string Email { get; set; } = null!;
    public string CourseInfo { get; set; } = null!;
    public string CourseName { get; set; } = null!;
    public long Fee { get; set; }
    public string RefLink { get; set; } = null!;
    public string GuideLink { get; set; } = null!;
    public DateTime RegisterAt { get; set; }
    public DateTime ExpAt { get; set; }

    public new string FormatContent()
    {
        var body = base.FormatContent();
        body = body.Replace("[course_name]", CourseName);
        body = body.Replace("[full_name]", FullName);
        body = body.Replace("[phone]", Phone);
        body = body.Replace("[email]", Email);
        body = body.Replace("[course_info]", CourseInfo);
        body = body.Replace("[fee]", Fee.FormatMoney());
        body = body.Replace("[registered_at]", RegisterAt.ToString("HH:mm dd/MM/yyyy"));
        body = body.Replace("[exp_at]", ExpAt.ToString("HH:mm dd/MM/yyyy"));
        body = body.Replace("[guide_link]", GuideLink);
        body = body.Replace("[ref_link]", RefLink);

        return body;
    }
}