namespace Course.Common.Email.Template;

public class ContentTemplateSurveyPromotion : ContentTemplateBase
{
    public ContentTemplateSurveyPromotion()
    {
        Title = "[M2mBA] Duyệt đánh giá khóa học";
        TemplatePath = "mail_survey_promotion.html";
    }

    public string CourseName { get; set; } = null!;

    public string PromotionInfo
    {
        get
        {
            var promotionInfo = "";
            if (DiscountAmount > 0)
            {
                promotionInfo = DiscountAmount.Value.FormatMoney() + " VND";
                if (DiscountPercent > 0)
                {
                    promotionInfo += $" và {DiscountPercent.Value.FormatRate()} giá trị khóa học";
                }
            }
            else if (DiscountPercent > 0)
            {
                promotionInfo = $" {DiscountPercent.Value.FormatRate()} giá trị khóa học";
            }

            return promotionInfo;
        }
    }

    public long? DiscountAmount { get; set; }
    public decimal? DiscountPercent { get; set; }
    public DateTime ToDate { get; set; }

    public new string FormatContent()
    {
        var body = base.FormatContent();
        body = body.Replace("[course_name]", CourseName);
        body = body.Replace("[promotion]", PromotionInfo);
        body = body.Replace("[to_date]", ToDate.ToString("dd/MM/yyyy"));

        return body;
    }
}