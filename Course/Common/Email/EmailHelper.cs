using Course.DbModels;

namespace Course.Common.Email;

public static class EmailHelper
{
    public static bool Send(string toEmail, string body, string subject, bool log = true)
    {
        var status = EmailSender.SendMailCommon(toEmail, subject, body);
        if (!log) return status;
        using var dbContext = new CourseContextExtend();
        dbContext.EmailLogs.Add(new EmailLog
        {
            Content = body,
            Email = toEmail,
            CreatedAt = DateTime.Now
        });
        dbContext.SaveChanges();

        return status;
    }
}