using System.IO.Compression;
using System.Text;

namespace Course.Common.Zip;

public static class ZipHelper
{
    public static string Unzip(string? str)
    {
        if (string.IsNullOrWhiteSpace(str)) return "";

        try
        {
            var bytes = Convert.FromBase64String(str);
            using var msi = new MemoryStream(bytes);
            using var mso = new MemoryStream();
            using (var gs = new GZipStream(msi, CompressionMode.Decompress))
            {
                CopyTo(gs, mso);
            }

            return Encoding.UTF8.GetString(mso.ToArray());
        }
        catch (Exception)
        {
            return str;
        }
    }

    public static string Zip(string str)
    {
        var bytes = Encoding.UTF8.GetBytes(str);

        using var msi = new MemoryStream(bytes);
        using var mso = new MemoryStream();
        using (var gs = new GZipStream(mso, CompressionMode.Compress))
        {
            CopyTo(msi, gs);
        }

        var newStr = Convert.ToBase64String(mso.ToArray());
        return newStr;
    }

    private static void CopyTo(Stream src, Stream dest)
    {
        var bytes = new byte[4096];

        int cnt;

        while ((cnt = src.Read(bytes, 0, bytes.Length)) != 0) dest.Write(bytes, 0, cnt);
    }
}