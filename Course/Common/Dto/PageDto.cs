namespace Course.Common.Dto;

public class PageDto<T>
{
    public List<T> List { get; set; } = new();
    public int Total { get; set; }
}