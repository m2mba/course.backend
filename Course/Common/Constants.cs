namespace Course.Common;

public static class Constants
{
    // Common
    public const string ACTIVE = "ACTIVE";

    //Search Page Constant
    public const int PAGE_NUMBER_DEFAULT = 1;
    public const int PAGE_SIZE_DEFAULT = 10;

    // Roles
    public const string ROLE_ADMIN = "ADMIN";
    public const string ROLE_STUDENT = "STUDENT";
    
    // Classroom status
    public const string CLASSROOM_ACTIVE_STATUS = "ACTIVE";
    public const string CLASSROOM_LEARNING_STATUS = "LEARNING";
    public const string CLASSROOM_STOP_STATUS = "STOP";
    public const string CLASSROOM_FINISH_STATUS = "FINISH";
    
    // Register status
    public const string REGISTER_WAIT_PAYMENT_STATUS = "WAIT_PAYMENT";
    public const string REGISTER_SUCCESS_STATUS = "SUCCESS";
    public const string REGISTER_LEARNING_STATUS = "LEARNING";
    public const string REGISTER_CANCEL_STATUS = "CANCEL";
    public const string REGISTER_RESERVE_STATUS = "RESERVE";
    public const string REGISTER_FINISH_STATUS = "FINISH";
    
    // Student status
    public const string STUDENT_WAIT_JOIN_STATUS = "WAIT_JOIN";
    public const string STUDENT_LEARNING_STATUS = "LEARNING";
    public const string STUDENT_FINISH_STATUS = "FINISH";
    public const string STUDENT_EVALUATED_STATUS = "EVALUATED";
    
    // Survey status
    public const string SURVEY_NEW_STATUS = "NEW";
    public const string SURVEY_REJECT_STATUS = "REJECTED";
    public const string SURVEY_APPROVE_STATUS = "APPROVED";
    
    // Document method
    public const string DOC_METHOD_BEFORE = "BEFORE";
    public const string DOC_METHOD_OFFICIAL = "OFFICIAL";

    // Promotion apply type
    public const string PROMOTION_APPLY_TYPE_ALL = "ALL";
    public const string PROMOTION_APPLY_TYPE_OLD_STUDENT = "OLD_STUDENT";
    public const string PROMOTION_APPLY_TYPE_SURVEY = "SURVEY";
    public const string PROMOTION_APPLY_TYPE_SHARE = "SHARE";
    
    // System constants
    public const int MAX_RESERVE = 3;
}