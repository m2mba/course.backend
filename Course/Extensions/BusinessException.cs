using System.Net;

namespace Course.Extensions;

public class BusinessException : Exception
{
    public readonly object? Result;
    public readonly int StatusCode;

    public BusinessException(string message, int statusCode = (int)HttpStatusCode.BadRequest, object? result = null)
        : base(message)
    {
        StatusCode = statusCode;
        Result = result;
    }
}