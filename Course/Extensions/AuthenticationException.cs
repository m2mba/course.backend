using System.Net;

namespace Course.Extensions;

public class AuthorizationException : BusinessException
{
    public AuthorizationException(string message = "Unauthorized", object? result = null)
        : base(message, (int)HttpStatusCode.Unauthorized, result)
    {
    }
}